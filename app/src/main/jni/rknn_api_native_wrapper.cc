//
// Created by nagita on 2019/06/17.
//
#include <jni.h>
#include <unistd.h>
#include <sched.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <sys/syscall.h>

#include "rknn_api_native_wrapper.h"
#include "rknn_model.h"
#include "yolo_v3_model.h"
#include "mobilenet_model.h"

static rknn_model::YoloV3Model yolo_v3_tiny_model;
static rknn_model::MobileNetModel mobilenet_model[10];

static char * jstringToChar(JNIEnv * env, jstring jstr) {
    char * rtn = NULL;
    jclass clsstring = env->FindClass("java/lang/String");
    jstring strencode = env->NewStringUTF("utf-8");
    jmethodID mid = env->GetMethodID(clsstring, "getBytes", "(Ljava/lang/String;)[B");
    jbyteArray barr = (jbyteArray) env->CallObjectMethod(jstr, mid, strencode);
    jsize alen = env->GetArrayLength(barr);
    jbyte * ba = env->GetByteArrayElements(barr, JNI_FALSE);

    if (alen > 0) {
        rtn = new char[alen + 1];
        memcpy(rtn, ba, alen);
        rtn[alen] = 0;
    }
    env->ReleaseByteArrayElements(barr, ba, 0);
    return rtn;
}


extern "C" JNIEXPORT jint JNICALL
Java_com_yut3_cpos_ui_pos_InferenceWrapper_init(JNIEnv * env, jobject obj, jint modelNumber, jstring modelPath, jint originalImageWidth, jint originalImageHeight)
{
    char * model_path = jstringToChar(env, modelPath);
    int ret = 0;
    if (modelNumber == 0) {
        ret = yolo_v3_tiny_model.model_init(std::string(model_path));
        yolo_v3_tiny_model.set_original_image_size(originalImageWidth, originalImageHeight);
    }
    if (modelNumber != 0) ret = mobilenet_model[modelNumber - 1].model_init(std::string(model_path));
    return ret;
}

extern "C" JNIEXPORT jint JNICALL
Java_com_yut3_cpos_ui_pos_InferenceWrapper_queryIoNum(JNIEnv* env, jobject obj, jint modelNumber, jintArray io_num)
{
    jint buf[2];
    env->GetIntArrayRegion(io_num, 0, 2, buf);
    if (modelNumber == 0) {
        *buf = yolo_v3_tiny_model.get_input_count();
        *(buf + 1) = yolo_v3_tiny_model.get_output_count();
    } else {
        *buf = mobilenet_model[modelNumber - 1].get_input_count();
        *(buf + 1) = mobilenet_model[modelNumber - 1].get_output_count();
    }
    env->SetIntArrayRegion(io_num, 0, 2, buf);
    return 0;
}

extern "C" JNIEXPORT jint JNICALL
Java_com_yut3_cpos_ui_pos_InferenceWrapper_queryOutputAttribute
(JNIEnv* env, jobject obj, jint modelNumber, jint outputCount, jint outputNumber)
{
    // no need to do anything here
    return 0;
}

extern "C" JNIEXPORT jint JNICALL
Java_com_yut3_cpos_ui_pos_InferenceWrapper_inference
(JNIEnv* env, jobject obj, jint modelNumber, jint inputIndex, jbyteArray imageData, jint width, jint height, jint channels)
{
    jboolean inputCopy = JNI_FALSE;
    jbyte * const inData = env->GetByteArrayElements(imageData, &inputCopy);

    if (modelNumber == 0) {
        yolo_v3_tiny_model.set_input(inputIndex, (char *)inData, width * height * channels);
        yolo_v3_tiny_model.run_inference();
    }else {
        mobilenet_model[modelNumber - 1].set_input(inputIndex, (char *)inData, width * height * channels);
//        mobilenet_model.set_input(inputIndex, (char *)inData, width * height * channels, RKNN_TENSOR_FLOAT16, RKNN_TENSOR_NHWC);
        mobilenet_model[modelNumber - 1].run_inference();
    }

    return 0;
}

extern "C" JNIEXPORT jint JNICALL
Java_com_yut3_cpos_ui_pos_InferenceWrapper_getInferenceResult
(JNIEnv* env, jobject obj, jint modelNumber, jintArray objectsClassId, jfloatArray objectsConfidence, jintArray objectsLeft, jintArray objectsTop, jintArray objectsRight, jintArray objectsBottom)
{
    jint out;
//    LOG_I("%s(%d): in, modelNumber:[%d]\n", __FUNCTION__, __LINE__, modelNumber);
    if (modelNumber == 0) {
        int output_count = yolo_v3_tiny_model.get_output_count();
        yolo_v3_tiny_model.set_output(0, true, false, 0, 0);
        yolo_v3_tiny_model.set_output(1, true, false, 0, 0);
        std::vector<std::tuple<void*, int>> outputs = yolo_v3_tiny_model.get_outputs();

        //yolo_v3_tiny_model.get_infer_time();

        yolo_v3_tiny_model.create_post_process();
        yolo_v3_tiny_model.run_post_process();
        yolo_v3_tiny_model.release_post_process();
        yolo_v3_tiny_model.release_outputs();

        out = yolo_v3_tiny_model.getOutputSize();
        env->SetIntArrayRegion(objectsClassId, 0, yolo_v3_tiny_model.getOutputSize(), yolo_v3_tiny_model.getOutputClassId());
        env->SetIntArrayRegion(objectsLeft, 0, yolo_v3_tiny_model.getOutputSize(), yolo_v3_tiny_model.getOutputLeftPos());
        env->SetIntArrayRegion(objectsTop, 0, yolo_v3_tiny_model.getOutputSize(), yolo_v3_tiny_model.getOutputTopPos());
        env->SetIntArrayRegion(objectsRight, 0, yolo_v3_tiny_model.getOutputSize(), yolo_v3_tiny_model.getOutputRightPos());
        env->SetIntArrayRegion(objectsBottom, 0, yolo_v3_tiny_model.getOutputSize(), yolo_v3_tiny_model.getOutputBottomPos());
        env->SetFloatArrayRegion(objectsConfidence, 0, yolo_v3_tiny_model.getOutputSize(), yolo_v3_tiny_model.getOutputConfidence());
    } else {
//        LOG_I("%s(%d): modelNumber:[%d], set output start\n", __FUNCTION__, __LINE__, modelNumber);
        mobilenet_model[modelNumber - 1].set_output(0, true, false, 0, 0);
//        LOG_I("%s(%d): modelNumber:[%d], set output done\n", __FUNCTION__, __LINE__, modelNumber);
//        LOG_I("%s(%d): modelNumber:[%d], get outputs start\n", __FUNCTION__, __LINE__, modelNumber);
        std::vector<std::tuple<void*, int>> outputs = mobilenet_model[modelNumber - 1].get_outputs();

        //mobilenet_model[modelNumber - 1].get_infer_time();
//        LOG_I("%s(%d): modelNumber:[%d], get outputs done\n", __FUNCTION__, __LINE__, modelNumber);

//        LOG_I("%s(%d): modelNumber:[%d], create post process start\n", __FUNCTION__, __LINE__, modelNumber);
        mobilenet_model[modelNumber - 1].create_post_process();
//        LOG_I("%s(%d): modelNumber:[%d], create post process done\n", __FUNCTION__, __LINE__, modelNumber);

//        LOG_I("%s(%d): modelNumber:[%d], run post process start\n", __FUNCTION__, __LINE__, modelNumber);
        mobilenet_model[modelNumber - 1].run_post_process();
//        LOG_I("%s(%d): modelNumber:[%d], run post process done\n", __FUNCTION__, __LINE__, modelNumber);

//        LOG_I("%s(%d): modelNumber:[%d], release post process start\n", __FUNCTION__, __LINE__, modelNumber);
        mobilenet_model[modelNumber - 1].release_post_process();
//        LOG_I("%s(%d): modelNumber:[%d], release post process done\n", __FUNCTION__, __LINE__, modelNumber);

        mobilenet_model[modelNumber - 1].release_outputs();

        out = rknn_model::MOBILENET_OUTPUT::NUM_ITEMS;
        env->SetFloatArrayRegion(objectsConfidence, 0, mobilenet_model[modelNumber - 1].getOutputSize(), mobilenet_model[modelNumber - 1].getOutputConfidence());
    }

//    LOG_I("%s(%d): in\n", __FUNCTION__, __LINE__);
    return out;
}

extern "C" JNIEXPORT jint JNICALL
Java_com_yut3_cpos_ui_pos_InferenceWrapper_setYoloThreshold
(JNIEnv* env, jobject obj, jintArray nms_threshold, jintArray object_threshold, jintArray class_probability_threshold, jintArray object_score_threshold)
{
    jint nms_threshold_buf[1];
    jint object_threshold_buf[1];
    jint class_probability_threshold_buf[1];
    jint object_score_threshold_buf[1];
    env->GetIntArrayRegion(nms_threshold, 0, 1, nms_threshold_buf);
    env->GetIntArrayRegion(object_threshold, 0, 1, object_threshold_buf);
    env->GetIntArrayRegion(class_probability_threshold, 0, 1, class_probability_threshold_buf);
    env->GetIntArrayRegion(object_score_threshold, 0, 1, object_score_threshold_buf);

    return yolo_v3_tiny_model.set_thresholds((float)nms_threshold_buf[0] / 100.0f, (float)object_threshold_buf[0] / 100.0f, (float)class_probability_threshold_buf[0] / 100.0f, (float)object_score_threshold_buf[0] / 100.0f);
}

extern "C" JNIEXPORT jint JNICALL
Java_com_yut3_cpos_ui_pos_InferenceWrapper_setYoloOriginalImageSize
(JNIEnv* env, jobject obj, jint imageWidth, jint imageHeight)
{
    return yolo_v3_tiny_model.set_original_image_size(imageWidth, imageHeight);
}

extern "C" JNIEXPORT jint JNICALL
Java_com_yut3_cpos_ui_pos_InferenceWrapper_getModelInputSize
(JNIEnv* env, jobject obj, jintArray yolov3InputHeight, jintArray yolov3InputWidth, jintArray mobilenetv1InputHeight, jintArray mobilenetv1InputWidth)
{
    jint yolov3_input_height_buf[1];
    jint yolov3_input_width_buf[1];
    jint mobilenetv1_input_height_buf[1];
    jint mobilenetv1_input_width_buf[1];

    yolov3_input_height_buf[0] = yolo_v3_tiny_model.getInputHeight();
    yolov3_input_width_buf[0] = yolo_v3_tiny_model.getInputWidth();
    mobilenetv1_input_height_buf[0] = mobilenet_model[0].getInputHeight();
    mobilenetv1_input_width_buf[0] = mobilenet_model[0].getInputWidth();

    env->SetIntArrayRegion(yolov3InputHeight, 0, 1, yolov3_input_height_buf);
    env->SetIntArrayRegion(yolov3InputWidth, 0, 1, yolov3_input_width_buf);
    env->SetIntArrayRegion(mobilenetv1InputHeight, 0, 1, mobilenetv1_input_height_buf);
    env->SetIntArrayRegion(mobilenetv1InputWidth, 0, 1, mobilenetv1_input_width_buf);
    return 0;
}

