//
// Created by nagita on 2019/08/06.
//

#ifndef C4VIEWER_MOBILENET_MODEL_H
#define C4VIEWER_MOBILENET_MODEL_H

#include "rknn_model.h"
namespace rknn_model {
    namespace MOBILENET_OUTPUT {
        constexpr auto NUM_ITEMS = 105;
    }

    namespace MOBILENET_INPUT {
        constexpr int NET_HEIGHT = 224;
        constexpr int NET_WIDTH = 224;
    }

    class MobileNetModel : public RknnModel{
    public:
        MobileNetModel();
        MobileNetModel(std::string param_path) : RknnModel(param_path) { }
        ~MobileNetModel();
        int create_post_process();
        virtual int run_post_process() override;
        int release_post_process();
        auto getOutputConfidence() { return this->m_confidence.data(); }
        auto getOutputSize() { return MOBILENET_OUTPUT::NUM_ITEMS; }
        auto getInputHeight() { return MOBILENET_INPUT::NET_HEIGHT; }
        auto getInputWidth() { return MOBILENET_INPUT::NET_WIDTH; }

    private:
        std::vector<float> m_confidence = {};
    };

}


#endif //C4VIEWER_MOBILENET_MODEL_H
