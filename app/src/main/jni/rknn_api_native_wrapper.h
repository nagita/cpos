//
// Created by nagita on 2019/06/17.
//
#include <jni.h>

#ifndef C4VIEWER_RKNN_API_NATIVE_WRAPPER_H
#define C4VIEWER_RKNN_API_NATIVE_WRAPPER_H
#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT jint JNICALL Java_com_yut3_cpos_ui_pos_InferenceWrapper_init(JNIEnv*, jobject, jint, jstring, jint, jint);
JNIEXPORT jint JNICALL Java_com_yut3_cpos_ui_pos_InferenceWrapper_queryIoNum(JNIEnv*, jobject, jint, jintArray);
JNIEXPORT jint JNICALL Java_com_yut3_cpos_ui_pos_InferenceWrapper_queryOutputAttribute(JNIEnv*, jobject, jint, jint, jint);
JNIEXPORT jint JNICALL Java_com_yut3_cpos_ui_pos_InferenceWrapper_inference(JNIEnv*, jobject, jint, jint, jbyteArray, jint, jint, jint);

// なんか、JNIでオブジェクトのフィールド操作面倒すぎたから、それぞれ別のままにした
JNIEXPORT jint JNICALL Java_com_yut3_cpos_ui_pos_InferenceWrapper_getInferenceResult(JNIEnv*, jobject, jint, jintArray, jfloatArray, jintArray, jintArray, jintArray, jintArray);
JNIEXPORT jint JNICALL Java_com_yut3_cpos_ui_pos_InferenceWrapper_setYoloThreshold(JNIEnv*, jobject, jintArray, jintArray, jintArray, jintArray);
JNIEXPORT jint JNICALL Java_com_yut3_cpos_ui_pos_InferenceWrapper_setYoloOriginalImageSize(JNIEnv*, jobject, jint, jint);

JNIEXPORT jint JNICALL Java_com_yut3_cpos_ui_pos_InferenceWrapper_getModelInputSize(JNIEnv*, jobject, jintArray, jintArray, jintArray, jintArray);

#ifdef __cplusplus
}
#endif
#endif //C4VIEWER_RKNN_API_NATIVE_WRAPPER_H
