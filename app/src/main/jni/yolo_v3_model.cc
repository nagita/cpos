//
// Created by nagita on 2019/07/09.
//

#include "yolo_v3_model.h"
#include "yolo_output_process.h"

constexpr auto MAX_OBJECT_COUNT = 2048;

namespace rknn_model {

    YoloV3Model::YoloV3Model() = default;

    YoloV3Model::~YoloV3Model() = default;

    int YoloV3Model::create_post_process()
    {
//        this->set_thresholds(
//                YOLO_OUTPUT::NMS_THRESHOLD,
//                YOLO_OUTPUT::OBJ_THRESHOLD,
//                YOLO_OUTPUT::CLASS_PROB_THRESHOLD,
//                YOLO_OUTPUT::OBJECT_SCORE_THRESHOLD);

        for (int i = 0; i < rknn_model::YOLO_OUTPUT::DETECTION_SIZE; ++i) {
            this->m_detections[i].prob = new float[rknn_model::YOLO_OUTPUT::NUM_CLASSES];
            this->m_detections[i].reset();
        }
        this->m_class_id.reserve(MAX_OBJECT_COUNT);
        this->m_confidence.reserve(MAX_OBJECT_COUNT);
        this->m_left_pos.reserve(MAX_OBJECT_COUNT);
        this->m_top_pos.reserve(MAX_OBJECT_COUNT);
        this->m_right_pos.reserve(MAX_OBJECT_COUNT);
        this->m_bottom_pos.reserve(MAX_OBJECT_COUNT);

        return 0;
    }

    int YoloV3Model::run_post_process()
    {
        for (int i = 0; i < rknn_model::YOLO_OUTPUT::DETECTION_SIZE; ++i) {
            this->m_detections[i].reset();
        }

        //std::cout << "object: x: " << data[47] << ", y: " << data[216] << ", w: " << data[385] << ", h: " << data[554] << ", conf: " << data[723] << ", prob1: " << data[892] << ", prob2: " << data[1061] << std::endl;

//        LOG_I("object: x: %f, y: %f, w: %f, h: %f, conf: %f, prob1: %f, prob2: %f\n",
//                *((float *)this->m_outputs[0].buf + 47),
//              *((float *)this->m_outputs[0].buf + 216),
//              *((float *)this->m_outputs[0].buf + 385),
//              *((float *)this->m_outputs[0].buf + 554),
//              *((float *)this->m_outputs[0].buf + 723),
//              *((float *)this->m_outputs[0].buf + 892),
//              *((float *)this->m_outputs[0].buf + 1061));

        yolo_tiny_post_process(this->m_outputs, this->m_detections, const_cast<float *>(rknn_model::YOLO_OUTPUT::ANCHORS),
                rknn_model::YOLO_INPUT::NET_WIDTH, rknn_model::YOLO_INPUT::NET_HEIGHT,
                this->m_class_id, this->m_confidence, this->m_left_pos, this->m_top_pos, this->m_right_pos, this->m_bottom_pos);

        return 0;
    }

    int YoloV3Model::release_post_process()
    {
        for (int i = 0; i < rknn_model::YOLO_OUTPUT::DETECTION_SIZE; ++i) {
            delete [] this->m_detections[i].prob;
        }

        return 0;
    }

    int YoloV3Model::set_thresholds
    (float nms_threshold, float object_threshold, float class_probability_threshold, float object_score_threshold)
    {
        this->m_nms_threshold = (nms_threshold < 0) ? this->m_nms_threshold : nms_threshold;
        this->m_object_threshold = (object_threshold < 0) ? this->m_object_threshold : object_threshold;
        this->m_class_probability_threshold = (class_probability_threshold < 0) ? this->m_class_probability_threshold : class_probability_threshold;
        this->m_object_score_threshold = (object_score_threshold < 0) ? this->m_object_score_threshold : object_score_threshold;

        yolo_post_process_threshold_init(
                this->m_nms_threshold,
                this->m_object_threshold,
                this->m_class_probability_threshold,
                this->m_object_score_threshold);

        return 0;
    }

    int YoloV3Model::set_original_image_size(int width, int height) {
        yolo_set_original_image_size(width, height);
        return 0;
    }

}