//
// Created by nagita on 2019/06/21.
//

#ifndef C3VIEWER_YOLO_OUTPUT_PROCESS_H
#define C3VIEWER_YOLO_OUTPUT_PROCESS_H


#include "rknn_api.h"
#include "yolo_v3_model.h"

int yolo_post_process_threshold_init(float nms, float object, float class_probability, float object_score);

int yolo_tiny_post_process
        (const std::vector<rknn_output>& outputs, std::array<rknn_model::YOLO_OUTPUT::detection, rknn_model::YOLO_OUTPUT::DETECTION_SIZE>& detections, float* anchors, int net_width, int net_height,
         std::vector<int>& class_id, std::vector<float>& confidence, std::vector<int>& object_left, std::vector<int>& object_top, std::vector<int>& object_right, std::vector<int>& object_bottom);

int yolo_set_original_image_size(int width, int height);


#endif //C3VIEWER_YOLO_OUTPUT_PROCESS_H
