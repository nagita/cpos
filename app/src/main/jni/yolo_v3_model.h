//
// Created by nagita on 2019/07/09.
//

#ifndef C3VIEWER_YOLO_V3_MODEL_H
#define C3VIEWER_YOLO_V3_MODEL_H


#include "rknn_model.h"

namespace rknn_model {
    namespace YOLO_OUTPUT {
        typedef enum E_BOX_INDEX_OFFSET {
            X = 0,
            Y,
            W,
            H,
            CONF,
            CLASSES_PROB,
        } BOX_INDEX_OFFSET;

        typedef enum E_ANCHOR_GROUP {
            FIRST = 1,
            SECOND = 2,
            THIRD = 3,
            FOURTH = 3,
            FIFTH = 4,
            SIXTH = 5,
        } ANCHOR_GROUP;

        typedef struct {
            float x, y, w, h;
        }box;

        typedef struct detection {
            box bbox;
            int classes;
            float* prob;
            float objectness;
            int sort_class;
            void reset() { this->classes = -1; this->objectness = 0.0F; this->sort_class = -1; };
        } detection;

//        constexpr auto ORIGINAL_DISPLAY_CAMERA_WIDTH = (1536);
//        constexpr auto ORIGINAL_DISPLAY_CAMERA_HEIGHT = (1152 - 84);
        //constexpr auto ORIGINAL_DISPLAY_CAMERA_WIDTH = (640);
        //constexpr auto ORIGINAL_DISPLAY_CAMERA_HEIGHT = (480);
        constexpr auto ORIGINAL_DISPLAY_CAMERA_WIDTH = (1440);
        constexpr auto ORIGINAL_DISPLAY_CAMERA_HEIGHT = (1080);

        //constexpr auto GRID0 = 13;
        //constexpr auto GRID1 = 26;
        constexpr auto GRID0 = 19;
        constexpr auto GRID1 = 38;
        constexpr auto NUM_CLASSES = 1;
        constexpr auto NUM_YOLO_LAYER = 2;
        constexpr auto NUM_ANCHOR = 3; // n anchor per yolo layer

        constexpr auto TOTAL_ANCHOR_COUNT = NUM_YOLO_LAYER * NUM_ANCHOR;
        constexpr auto ANCHOR_ARRAY_SIZE = 2 * TOTAL_ANCHOR_COUNT; // 2 for x and y

        constexpr auto ANCHOR_X1 = 10.F;
        constexpr auto ANCHOR_Y1 = 14.F;

        constexpr auto ANCHOR_X2 = 23.F;
        constexpr auto ANCHOR_Y2 = 27.F;

        constexpr auto ANCHOR_X3 = 37.F;
        constexpr auto ANCHOR_Y3 = 58.F;

        constexpr auto ANCHOR_X4 = 81.F;
        constexpr auto ANCHOR_Y4 = 82.F;

        constexpr auto ANCHOR_X5 = 135.F;
        constexpr auto ANCHOR_Y5 = 169.F;

        constexpr auto ANCHOR_X6 = 344.F;
        constexpr auto ANCHOR_Y6 = 319.F;

        constexpr float ANCHORS[ANCHOR_ARRAY_SIZE] = { ANCHOR_X1, ANCHOR_Y1, ANCHOR_X2, ANCHOR_Y2, ANCHOR_X3, ANCHOR_Y3, ANCHOR_X4, ANCHOR_Y4, ANCHOR_X5, ANCHOR_Y5, ANCHOR_X6, ANCHOR_Y6 };

        constexpr int ANCHOR_MASK_0[NUM_ANCHOR] = { YOLO_OUTPUT::ANCHOR_GROUP::FOURTH, YOLO_OUTPUT::ANCHOR_GROUP::FIFTH, YOLO_OUTPUT::ANCHOR_GROUP::SIXTH };
        constexpr int ANCHOR_MASK_1[NUM_ANCHOR] = { YOLO_OUTPUT::ANCHOR_GROUP::FIRST, YOLO_OUTPUT::ANCHOR_GROUP::SECOND, YOLO_OUTPUT::ANCHOR_GROUP::THIRD };

        constexpr auto NBOXES_0 = GRID0 * GRID0 * NUM_ANCHOR;
        constexpr auto NBOXES_1 = GRID1 * GRID1 * NUM_ANCHOR;
        constexpr auto NBOXES_TOTAL = NBOXES_0 + NBOXES_1;

        constexpr float OBJ_THRESHOLD = 0.1F;
        constexpr float CLASS_PROB_THRESHOLD = OBJ_THRESHOLD * 1.0F;
        constexpr float NMS_THRESHOLD = 0.5F;  //darknet demo nms=0.4
        constexpr float OBJECT_SCORE_THRESHOLD = CLASS_PROB_THRESHOLD;
        constexpr int POST_PROCESS_ALLOW_MIN_OBJECT_WIDTH = 10;
        constexpr int POST_PROCESS_ALLOW_MIN_OBJECT_HEIGHT = 10;

        constexpr float EPSILON = 1e-31;
        constexpr float FLOAT_ZERO = 0.000'00F;
        constexpr auto MAX_STRING_LENGTH = 4096;
        constexpr double SIGMOID_GAIN = 1.F;

        const std::string LABELS[NUM_CLASSES] = { "0" };

        constexpr auto LAYER_0_SIZE = NUM_ANCHOR * GRID0 * GRID0 * (YOLO_OUTPUT::BOX_INDEX_OFFSET::CLASSES_PROB + NUM_CLASSES);
        constexpr auto LAYER_1_SIZE = NUM_ANCHOR * GRID1 * GRID1 * (YOLO_OUTPUT::BOX_INDEX_OFFSET::CLASSES_PROB + NUM_CLASSES);
        constexpr auto DETECTION_SIZE = (LAYER_0_SIZE + LAYER_1_SIZE) / (YOLO_OUTPUT::BOX_INDEX_OFFSET::CLASSES_PROB + NUM_CLASSES);

    }

    namespace YOLO_INPUT {
        constexpr int NET_WIDTH = 608;
        constexpr int NET_HEIGHT = 608;
    }

    class YoloV3Model : public RknnModel
    {
    public:
        YoloV3Model();
        YoloV3Model(std::string param_path) : RknnModel(param_path) { this->create_post_process(); }
        ~YoloV3Model();

        int create_post_process();
        virtual int run_post_process() override;
        int release_post_process();

        int set_thresholds(float nms_threshold, float object_threshold, float class_probability_threshold, float object_score_threshold);
        int set_original_image_size(int width, int height);

        auto getOutputSize() { return this->m_class_id.size(); }
        auto getOutputClassId()	{ return this->m_class_id.data(); }
        auto getOutputConfidence() { return this->m_confidence.data(); }
        auto getOutputLeftPos() { return this->m_left_pos.data(); }
        auto getOutputTopPos() { return this->m_top_pos.data(); }
        auto getOutputRightPos() { return this->m_right_pos.data(); }
        auto getOutputBottomPos() { return this->m_bottom_pos.data(); }
        auto getInputHeight() { return YOLO_INPUT::NET_HEIGHT; }
        auto getInputWidth() { return YOLO_INPUT::NET_WIDTH; }

    private:
        std::array<rknn_model::YOLO_OUTPUT::detection, rknn_model::YOLO_OUTPUT::DETECTION_SIZE> m_detections = {};
        std::vector<int> m_class_id = {};
        std::vector<float> m_confidence = {};
        std::vector<int> m_left_pos = {};
        std::vector<int> m_top_pos = {};
        std::vector<int> m_right_pos = {};
        std::vector<int> m_bottom_pos = {};
        float m_nms_threshold = YOLO_OUTPUT::NMS_THRESHOLD;
        float m_object_threshold = YOLO_OUTPUT::OBJ_THRESHOLD;
        float m_class_probability_threshold = YOLO_OUTPUT::CLASS_PROB_THRESHOLD;
        float m_object_score_threshold = YOLO_OUTPUT::OBJECT_SCORE_THRESHOLD;
    };

}




#endif //C3VIEWER_YOLO_V3_MODEL_H
