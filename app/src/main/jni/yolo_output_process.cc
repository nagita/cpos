//
// Created by nagita on 2019/06/21.
//

#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <unordered_set>
#include <vector>
#include <string.h>
#include <fcntl.h>
#include <array>
#include <algorithm>
#include <tuple>
#if _MSC_VER
#else
#include <getopt.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#endif
#include "rknn_api.h"
#include "rknn_model.h"
#include "yolo_output_process.h"
#include <omp.h>

#ifndef __ANDROID__
#define isnanf isnan
#endif

#include "arm_neon.h"

struct yolo_output_threshold {
    float nms_threshold = 0.25f;
    float object_threshold = 0.1f;
    float class_probability_threshold = 0.1f;
    float object_score_threshold = 0.1f;
};

static struct yolo_output_threshold output_threshold;

struct image_size {
    int width;
    int height;
};

struct image_size original_image_size;


/* ---------------------------------------------
  シグモイド関数を計算する
  引数1: x
  引数2: gain ゲイン: usually set 1.0
  戻り値 シグモイド関数
 ---------------------------------------------*/
constexpr bool USE_FAST_SIGMOID = false;
template <typename T>
inline T sigmoid(T x, T gain)
{
//    return 1.0 / (1.0 + vrecpxs_f32((float32_t)(-1.0F / gain * x)));
    if constexpr (USE_FAST_SIGMOID) {
        return 1.0 / (1.0 + abs(-gain * x));
    } else {
        return 1.0 / (1.0 + expf(-gain * x));
    }
}

inline bool is_prediction_result_out_of_range(float x)
{
    const float upper_bound = 100.F;
    const float lower_bound = -100.F;

    return ((x < lower_bound) || (x > upper_bound));
}

inline bool check_float_is_zero(float x)
{
    if ((x > 0 && x < rknn_model::YOLO_OUTPUT::EPSILON) || (x < 0 && x > -rknn_model::YOLO_OUTPUT::EPSILON))
        return true;
    else
        return x == 0.0F;
}

rknn_model::YOLO_OUTPUT::box get_yolo_box(float* x, int index, float const* biases, int anchor_mask_id, int col, int row, int grid_width, int grid_height, int net_width, int net_height)
{
    rknn_model::YOLO_OUTPUT::box b;
    int grid_size = grid_width * grid_height;
//    b.x = (col + x[index + (grid_size * 0)]) / grid_width;
//    b.y = (row + x[index + (grid_size * 1)]) / grid_height;
    b.x = ((float)col / grid_width) + ((float)x[index + (grid_size * 0)] / grid_width);
    b.y = ((float)row / grid_height) + ((float)x[index + (grid_size * 1)] / grid_height);
    b.w = expf(x[index + (grid_size * 2)]) / net_width * biases[2 * anchor_mask_id];
    b.h = expf(x[index + (grid_size * 3)]) / net_height * biases[2 * anchor_mask_id + 1];
//        LOG_I("yolo box: array idx[%d, %d, %d, %d]-->[%f, %f, %f, %f]@(x, y, w, h)-->(%f, %f, %f, %f)\n", index + (grid_size * 0), index + (grid_size * 1), index + (grid_size * 2), index + (grid_size * 3),
//              x[index + (grid_size * 0)], x[index + (grid_size * 1)], x[index + (grid_size * 2)], x[index + (grid_size * 3)],
//              b.x, b.y, b.w, b.h);
    return b;
}

void correct_yolo_boxes(rknn_model::YOLO_OUTPUT::detection* dets, int n, int w, int h, int netw, int neth, int relative)
{
    int i;
    int new_w = 0;
    int new_h = 0;
    if (((float)netw / w) < ((float)neth / h)) {
        new_w = netw;
        new_h = (h * netw) / w;
    }
    else {
        new_h = neth;
        new_w = (w * neth) / h;
    }
    for (i = 0; i < n; ++i) {
        rknn_model::YOLO_OUTPUT::box b = dets[i].bbox;
        b.x = (float)(b.x - ((float)netw - new_w) / 2. / netw) / ((float)new_w / netw);
        b.y = (float)(b.y - ((float)neth - new_h) / 2. / neth) / ((float)new_h / neth);
        b.w *= (float)netw / new_w;
        b.h *= (float)neth / new_h;
        if (!relative) {
            b.x *= w;
            b.w *= w;
            b.y *= h;
            b.h *= h;
        }
        dets[i].bbox = b;
    }
}

rknn_model::YOLO_OUTPUT::detection* get_network_boxes
(float* predictions, std::array<rknn_model::YOLO_OUTPUT::detection, rknn_model::YOLO_OUTPUT::DETECTION_SIZE> & detections,
        int netw, int neth, int grid_width, int grid_height, int* masks, float* anchors, int box_off, int* out_dets_count)
{
    const int grid2 = grid_width * grid_height;
    constexpr int list_size = (1 + 4 + rknn_model::YOLO_OUTPUT::NUM_CLASSES);

//#ifdef _OPENMP
//    std::cout << "OpenMP : Enabled (Max # of threads = " << omp_get_max_threads() << ")" << std::endl;
//    LOG_I("%s(%d): OpenMP: Enabled (Max # of threads = %d)\n", __FUNCTION__, __LINE__, omp_get_max_threads());
//#endif

    // シグモイドの処理
//#ifdef _OPENMP
//    omp_set_dynamic(0);
//#pragma omp parallel for num_threads(2)
//#endif
    for (int i_anchor = 0; i_anchor < rknn_model::YOLO_OUTPUT::NUM_ANCHOR; ++i_anchor) {
        for (int i_grid = 0; i_grid < grid2; ++i_grid) {
            int index_x = (i_anchor * list_size * grid2) + i_grid + (rknn_model::YOLO_OUTPUT::BOX_INDEX_OFFSET::X * grid2);
            int index_y = (i_anchor * list_size * grid2) + i_grid + (rknn_model::YOLO_OUTPUT::BOX_INDEX_OFFSET::Y * grid2);
//            int index_w = (i_anchor * list_size * grid2) + i_grid + (rknn_model::YOLO_OUTPUT::BOX_INDEX_OFFSET::W * grid2);
//            int index_h = (i_anchor * list_size * grid2) + i_grid + (rknn_model::YOLO_OUTPUT::BOX_INDEX_OFFSET::H * grid2);
            int index_conf = (i_anchor * list_size * grid2) + i_grid + (rknn_model::YOLO_OUTPUT::BOX_INDEX_OFFSET::CONF * grid2);
            int index_prob_n = (i_anchor * list_size * grid2) + i_grid + (rknn_model::YOLO_OUTPUT::BOX_INDEX_OFFSET::CLASSES_PROB * grid2);

//            if (index_x == 47) {
//                LOG_I("before: (x, y, w, h, conf, prob, prob)-->(%f, %f, %f, %f, %f, %f, %f)\n", predictions[47], predictions[216], predictions[385], predictions[554], predictions[723], predictions[892], predictions[1061]);
//            }

            // X 座標
            predictions[index_x] = (float)sigmoid((double)predictions[index_x], rknn_model::YOLO_OUTPUT::SIGMOID_GAIN);
            // Y 座標
            predictions[index_y] = (float)sigmoid((double)predictions[index_y], rknn_model::YOLO_OUTPUT::SIGMOID_GAIN);
            // 信頼度スコア
            predictions[index_conf] = (float)sigmoid((double)predictions[index_conf], rknn_model::YOLO_OUTPUT::SIGMOID_GAIN);
            // 確率 classes probability
            for (int i_class = 0; i_class < rknn_model::YOLO_OUTPUT::NUM_CLASSES; ++i_class) {
//                index_prob_n = (i_anchor * list_size * grid2) + i_grid + (rknn_model::YOLO_OUTPUT::BOX_INDEX_OFFSET::CLASSES_PROB * grid2);
                predictions[index_prob_n + (i_class * grid2)] = (float)sigmoid((double)predictions[index_prob_n + (i_class * grid2)], rknn_model::YOLO_OUTPUT::SIGMOID_GAIN);
            }
//            if (index_x == 47) {
//                LOG_I("after: (x, y, w, h, conf, prob, prob)-->(%f, %f, %f, %f, %f, %f, %f)\n", predictions[47], predictions[216], predictions[385], predictions[554], predictions[723], predictions[892], predictions[1061]);
//            }
//            LOG_I("(x, y, w, h, conf, prob, prob)-->(%f, %f, %f, %f, %f, %f, %f)\n", predictions[index_x], predictions[index_y], predictions[index_w], predictions[index_h], predictions[index_conf], predictions[index_prob_n + (0 * grid2)], predictions[index_prob_n + (1 * grid2)]);
        }
    }

    int count = box_off;
//#ifdef _OPENMP
//    omp_set_dynamic(0);
//#pragma omp parallel for num_threads(2)
//#endif
    for (int i = 0; i < grid2; i++) {
        int row = i / grid_width;
        int col = i % grid_width;
        for (int n = 0; n < rknn_model::YOLO_OUTPUT::NUM_ANCHOR; n++) {
            int box_index = n * grid2 * list_size + i;
            int obj_index = box_index + (rknn_model::YOLO_OUTPUT::BOX_INDEX_OFFSET::CONF * grid2);
            float objectness = predictions[obj_index];
            if (objectness < output_threshold.object_threshold) continue;
            int index = (n * list_size * grid2) + i;
            rknn_model::YOLO_OUTPUT::box b = get_yolo_box(predictions, index, anchors, masks[n], col, row, grid_width, grid_height, netw, neth);
            /*if (is_prediction_result_out_of_range(b.w) ||
                is_prediction_result_out_of_range(b.h) ||
                is_prediction_result_out_of_range(b.x) ||
                is_prediction_result_out_of_range(b.y) ||
                check_float_is_zero(b.w) ||
                check_float_is_zero(b.h) ||
                check_float_is_zero(b.x) ||
                check_float_is_zero(b.y) ||
                isnanf(b.x) ||
                isnanf(b.y) ||
                isnanf(b.w) ||
                isnanf(b.h)) {
//                LOG_E("prediction result[%d] OOR: (%f, %f, %f, %f) continue\n", index, b.x, b.y, b.w, b.h);
                continue;
            }*/
            detections[count].objectness = objectness;
            detections[count].classes = rknn_model::YOLO_OUTPUT::NUM_CLASSES;
            detections[count].bbox = b;
            for (int j = 0; j < rknn_model::YOLO_OUTPUT::NUM_CLASSES; ++j) {
                int class_index = box_index + (rknn_model::YOLO_OUTPUT::BOX_INDEX_OFFSET::CLASSES_PROB + j) * grid2;
                float prob = objectness * predictions[class_index];
                detections[count].prob[j] = (prob > output_threshold.class_probability_threshold) ? prob : rknn_model::YOLO_OUTPUT::FLOAT_ZERO;
            }
            ++count;
        }
    }
    *out_dets_count = count;
    return detections.data();
}

rknn_model::YOLO_OUTPUT::detection* outputs_transform
(rknn_output rknn_outputs[], std::array<rknn_model::YOLO_OUTPUT::detection, rknn_model::YOLO_OUTPUT::DETECTION_SIZE> & detections,
        float anchors[rknn_model::YOLO_OUTPUT::ANCHOR_ARRAY_SIZE], int net_width, int net_height, int* n_out)
{
    auto output_0 = (float*)rknn_outputs[0].buf;
    auto output_1 = (float*)rknn_outputs[1].buf;

    // output xywh range ∈ [0, 1]
    get_network_boxes(output_0, detections, net_width, net_height, rknn_model::YOLO_OUTPUT::GRID0, rknn_model::YOLO_OUTPUT::GRID0, (int*)rknn_model::YOLO_OUTPUT::ANCHOR_MASK_0, anchors, 0, n_out);
    //    LOG_I("%s(%d): detection out count: %d\n", __func__, __LINE__, *n_out);
    get_network_boxes(output_1, detections, net_width, net_height, rknn_model::YOLO_OUTPUT::GRID1, rknn_model::YOLO_OUTPUT::GRID1, (int*)rknn_model::YOLO_OUTPUT::ANCHOR_MASK_1, anchors, *n_out, n_out);
    //    LOG_I("%s(%d): detection out count: %d\n", __func__, __LINE__, *n_out);
//    correct_yolo_boxes(detections.data(), *n_out, rknn_model::YOLO_INPUT::ORIGINAL_IMAGE_WIDTH, rknn_model::YOLO_INPUT::ORIGINAL_IMAGE_HEIGHT, rknn_model::YOLO_INPUT::NET_WIDTH, rknn_model::YOLO_INPUT::NET_HEIGHT, 1);
    return detections.data();
}

float overlap(float x1, float w1, float x2, float w2)
{
    float l1 = x1 - w1 / 2;
    float l2 = x2 - w2 / 2;
    float left = l1 > l2 ? l1 : l2;
    float r1 = x1 + w1 / 2;
    float r2 = x2 + w2 / 2;
    float right = r1 < r2 ? r1 : r2;
    return right - left;
}

float box_intersection(rknn_model::YOLO_OUTPUT::box a, rknn_model::YOLO_OUTPUT::box b)
{
    float w = overlap(a.x, a.w, b.x, b.w);
    float h = overlap(a.y, a.h, b.y, b.h);
    if (w < 0 || h < 0) return 0;
    float area = w * h;
    return area;
}

float box_union(rknn_model::YOLO_OUTPUT::box a, rknn_model::YOLO_OUTPUT::box b)
{
    float i = box_intersection(a, b);
    float u = a.w * a.h + b.w * b.h - i;
    return u;
}

float box_iou(rknn_model::YOLO_OUTPUT::box a, rknn_model::YOLO_OUTPUT::box b)
{
    return box_intersection(a, b) / box_union(a, b);
}

int box_surrounded(rknn_model::YOLO_OUTPUT::box a, rknn_model::YOLO_OUTPUT::box b)
{
    float area_a = a.w * a.h;
    float area_b = b.w * b.h;
    float area_i = box_intersection(a, b);
    float ai = area_a/area_i;
    float bi = area_b/area_i;

    if(area_i == 0) {
        return 0;
    }
    if( (0.7 <= ai && 1.3 >= ai) || (0.7 <= bi && 1.3 >= bi) ) {
        if(area_a > area_b) {
            return 1;
        }
        else {
            return 2;
        }
    } else {
        return 0;
    }
}

int nms_comparator(const void* pa, const void* pb)
{
    rknn_model::YOLO_OUTPUT::detection a = *(rknn_model::YOLO_OUTPUT::detection*)pa;
    rknn_model::YOLO_OUTPUT::detection b = *(rknn_model::YOLO_OUTPUT::detection*)pb;
    float diff = 0;
    if (b.sort_class >= 0) {
        diff = a.prob[b.sort_class] - b.prob[b.sort_class];
    }
    else {
        diff = a.objectness - b.objectness;
    }
    if (diff < 0) return 1;
    else if (diff > 0) return -1;
    return 0;
}

[[deprecated("no use")]]
int do_nms_obj(rknn_model::YOLO_OUTPUT::detection *dets, int total, int classes, float thresh)
{
    int i, j, k;
    k = total-1;
//    cout<<"k: "<<k<<"\n";
    for(i = 0; i <= k; ++i){
        if(dets[i].objectness == 0){
            rknn_model::YOLO_OUTPUT::detection swap = dets[i];
            dets[i] = dets[k];
            dets[k] = swap;
            --k;
            --i;
        }
    }
    total = k+1;
//    cout<<"total after OBJ_THRESH: "<<total<<"\n";
    for(i = 0; i < total; ++i){
        dets[i].sort_class = -1;
    }

    qsort(dets, total, sizeof(rknn_model::YOLO_OUTPUT::detection), nms_comparator);
    for(i = 0; i < total; ++i){
        if(dets[i].objectness == 0) continue;
        rknn_model::YOLO_OUTPUT::box a = dets[i].bbox;
        for(j = i+1; j < total; ++j){
            if(dets[j].objectness == 0) continue;
            rknn_model::YOLO_OUTPUT::box b = dets[j].bbox;
            if (box_iou(a, b) > thresh){
                dets[j].objectness = 0;
                for(k = 0; k < classes; ++k){
                    dets[j].prob[k] = 0;
                }
            }
        }
    }
    return total;
}


int do_nms_sort(rknn_model::YOLO_OUTPUT::detection* dets, int total, int classes, float thresh)
{
    int i, j, k;
    k = total - 1;
    for (i = 0; i <= k; ++i) {
        if (check_float_is_zero(dets[i].objectness)) {
            rknn_model::YOLO_OUTPUT::detection swap = dets[i];
            dets[i] = dets[k];
            dets[k] = swap;
            --k;
            --i;
        }
    }
    total = k + 1;
    //    LOG_E("total after OBJ_THRES: %d", total);

    for (int nn = 0; nn < total; nn++) {
        rknn_model::YOLO_OUTPUT::detection tmp = dets[nn];
        if (tmp.classes > rknn_model::YOLO_OUTPUT::NUM_CLASSES) {
            LOG_E("ERROR: something going wrong..., idx:[%d]\n", nn);
        }
    }

    for (k = 0; k < classes; ++k) {
        for (i = 0; i < total; ++i) {
            dets[i].sort_class = k;
        }
        qsort(dets, (size_t)total, sizeof(rknn_model::YOLO_OUTPUT::detection), nms_comparator);
        for (i = 0; i < total; ++i) {
            if (check_float_is_zero(dets[i].prob[k])) continue;
            rknn_model::YOLO_OUTPUT::box a = dets[i].bbox;
            for (j = i + 1; j < total; ++j) {
                rknn_model::YOLO_OUTPUT::box b = dets[j].bbox;
                // IoU: 画像の重なりの割合を表す
                if ( box_iou(a, b) > thresh ) {
                    dets[j].prob[k] = rknn_model::YOLO_OUTPUT::FLOAT_ZERO;
                }
                /*
                else {// Filter the smaller one box which inside another box
                    int result = box_surrounded(a, b);
                    if ( ( result== 1) ) {
                        dets[j].prob[k] = rknn_model::YOLO_OUTPUT::FLOAT_ZERO;
                    }
                    else if ( (result == 2) ) {
                        dets[i].prob[k] = rknn_model::YOLO_OUTPUT::FLOAT_ZERO;
                    }
                }
                */
            }
        }
    }
    return total;
}

int convert_to_output
(std::array<rknn_model::YOLO_OUTPUT::detection, rknn_model::YOLO_OUTPUT::DETECTION_SIZE> & detections,
        int num_out, float threshold, std::vector<int>& class_id, std::vector<float>& confidence,
        std::vector<int>& object_left, std::vector<int>& object_top, std::vector<int>& object_right, std::vector<int>& object_bottom)
{
    int i_out = 0;
    class_id.clear();
    confidence.clear();
    object_left.clear();
    object_top.clear();
    object_right.clear();
    object_bottom.clear();
    for (int i = 0; i < num_out; ++i) {
        char labelstr[rknn_model::YOLO_OUTPUT::MAX_STRING_LENGTH] = { 0 };
        int class_ = -1;
        int topclass = -1;
        float topclass_score = 0;
        if (check_float_is_zero(detections[i].objectness)) continue;
        for (int j = 0; j < rknn_model::YOLO_OUTPUT::NUM_CLASSES; ++j) {
            if (detections[i].prob[j] > threshold) {
                if (topclass_score < detections[i].prob[j]) {
                    topclass_score = detections[i].prob[j];
                    topclass = j;
                }
                if (class_ < 0) {
                    strcat(labelstr, rknn_model::YOLO_OUTPUT::LABELS[j].data());
                    class_ = j;
                }
                else {
                    strcat(labelstr, ",");
                    strcat(labelstr, rknn_model::YOLO_OUTPUT::LABELS[j].data());
                }
//                LOG_E("%s: %.02f%%\n", rknn_model::YOLO_OUTPUT::LABELS[j].data(), detections[i].prob[j] * 100);
                //                printf("%s: %.02f%%\n",labels[j].data(),g_detections[i].prob[j]*100);
            }
        }

        if (class_ >= 0) {
            bool post_process_filter_flag = false;
            rknn_model::YOLO_OUTPUT::box b = detections[i].bbox;

            using namespace rknn_model::YOLO_INPUT;
            using namespace rknn_model::YOLO_OUTPUT;

            int x1 = (int)((b.x - b.w / 2.) * rknn_model::YOLO_INPUT::NET_WIDTH);
            int x2 = (int)((b.x + b.w / 2.) * rknn_model::YOLO_INPUT::NET_WIDTH);
            int y1 = (int)((b.y - b.h / 2.) * rknn_model::YOLO_INPUT::NET_HEIGHT);
            int y2 = (int)((b.y + b.h / 2.) * rknn_model::YOLO_INPUT::NET_HEIGHT);

            x1 = (int)(((float)x1 / rknn_model::YOLO_INPUT::NET_WIDTH) * ORIGINAL_DISPLAY_CAMERA_WIDTH);
            x2 = (int)(((float)x2 / rknn_model::YOLO_INPUT::NET_WIDTH) * ORIGINAL_DISPLAY_CAMERA_WIDTH);
            y1 = (int)(((float)y1 / rknn_model::YOLO_INPUT::NET_HEIGHT) * ORIGINAL_DISPLAY_CAMERA_HEIGHT);
            y2 = (int)(((float)y2 / rknn_model::YOLO_INPUT::NET_HEIGHT) * ORIGINAL_DISPLAY_CAMERA_HEIGHT);

            x1 = (int)((float)x1 / ORIGINAL_DISPLAY_CAMERA_WIDTH * original_image_size.width);
            x2 = (int)((float)x2 / ORIGINAL_DISPLAY_CAMERA_WIDTH * original_image_size.width);
            y1 = (int)((float)y1 / ORIGINAL_DISPLAY_CAMERA_HEIGHT * original_image_size.height);
            y2 = (int)((float)y2 / ORIGINAL_DISPLAY_CAMERA_HEIGHT * original_image_size.height);

//
//            x1 = x1 + (int)(abs(rknn_model::YOLO_OUTPUT::ORIGINAL_DISPLAY_CAMERA_WIDTH - original_image_size.width) * ((float)x1 / original_image_size.width));
//            x2 = x2 + (int)(abs(rknn_model::YOLO_OUTPUT::ORIGINAL_DISPLAY_CAMERA_WIDTH - original_image_size.width) * ((float)x2 / original_image_size.width));
//            y1 = y1 + (int)(abs(rknn_model::YOLO_OUTPUT::ORIGINAL_DISPLAY_CAMERA_HEIGHT - original_image_size.height) * ((float)y1 / original_image_size.height));
//            y2 = y2 + (int)(abs(rknn_model::YOLO_OUTPUT::ORIGINAL_DISPLAY_CAMERA_HEIGHT - original_image_size.height) * ((float)y2 / original_image_size.height));

//            x1 = (int)((float)x1 * ((float)rknn_model::YOLO_OUTPUT::ORIGINAL_DISPLAY_CAMERA_WIDTH / rknn_model::YOLO_INPUT::ORIGINAL_IMAGE_WIDTH));
//            x2 = (int)((float)x2 * ((float)rknn_model::YOLO_OUTPUT::ORIGINAL_DISPLAY_CAMERA_WIDTH / rknn_model::YOLO_INPUT::ORIGINAL_IMAGE_WIDTH));
//            y1 = (int)((float)y1 * ((float)rknn_model::YOLO_OUTPUT::ORIGINAL_DISPLAY_CAMERA_HEIGHT / rknn_model::YOLO_INPUT::ORIGINAL_IMAGE_HEIGHT));
//            y2 = (int)((float)y2 * ((float)rknn_model::YOLO_OUTPUT::ORIGINAL_DISPLAY_CAMERA_HEIGHT / rknn_model::YOLO_INPUT::ORIGINAL_IMAGE_HEIGHT));

//            x1 = (int)(((float)x1 / rknn_model::YOLO_INPUT::ORIGINAL_IMAGE_WIDTH) * rknn_model::YOLO_OUTPUT::ORIGINAL_DISPLAY_CAMERA_WIDTH);
//            x2 = (int)(((float)x2 / rknn_model::YOLO_INPUT::ORIGINAL_IMAGE_WIDTH) * rknn_model::YOLO_OUTPUT::ORIGINAL_DISPLAY_CAMERA_WIDTH);
//            y1 = (int)(((float)y1 / rknn_model::YOLO_INPUT::ORIGINAL_IMAGE_HEIGHT) * rknn_model::YOLO_OUTPUT::ORIGINAL_DISPLAY_CAMERA_HEIGHT);
//            y2 = (int)(((float)y2 / rknn_model::YOLO_INPUT::ORIGINAL_IMAGE_HEIGHT) * rknn_model::YOLO_OUTPUT::ORIGINAL_DISPLAY_CAMERA_HEIGHT);

            if (y1 == y2) {
                LOG_E("error: box height is zero: box(x, y, w, h)->(%f, %f, %f, %f)\n", b.x, b.y, b.w, b.h);
            }
/*
            if ((x1 < 0) || (y1 < 0) || (x2 > rknn_model::YOLO_OUTPUT::ORIGINAL_DISPLAY_CAMERA_WIDTH) || (y2 > rknn_model::YOLO_OUTPUT::ORIGINAL_DISPLAY_CAMERA_HEIGHT)) {
                continue;
            }
*/
            if (abs(x1 - x2) < rknn_model::YOLO_OUTPUT::POST_PROCESS_ALLOW_MIN_OBJECT_WIDTH) {
                LOG_E("post process: object width is too small:%d\n", abs(x1 - x2));
                post_process_filter_flag = true;
            }

            if (abs(y1 - y2) < rknn_model::YOLO_OUTPUT::POST_PROCESS_ALLOW_MIN_OBJECT_HEIGHT) {
                LOG_E("post process: object height is too small:%d\n", abs(y1 - y2));
                post_process_filter_flag = true;
            }

            if (post_process_filter_flag) {
                continue;
            }

            if (x1 < 0) x1 = 0;
            if (x2 > rknn_model::YOLO_OUTPUT::ORIGINAL_DISPLAY_CAMERA_WIDTH - 1) x2 = rknn_model::YOLO_OUTPUT::ORIGINAL_DISPLAY_CAMERA_WIDTH - 1;
            if (y1 < 0) y1 = 0;
            if (y2 > rknn_model::YOLO_OUTPUT::ORIGINAL_DISPLAY_CAMERA_HEIGHT - 1) y2 = rknn_model::YOLO_OUTPUT::ORIGINAL_DISPLAY_CAMERA_HEIGHT - 1;
//            LOG_E("tag[%s]: %02f%% @(%d, %d)->(%d, %d)\n", rknn_model::YOLO_OUTPUT::LABELS[topclass].c_str(), (float)detections[i].prob[topclass] * 100, x1, y1, x2, y2);
            class_id.push_back(std::stoi(rknn_model::YOLO_OUTPUT::LABELS[topclass].data()));
            confidence.push_back(detections[i].prob[topclass] * 100);
            object_left.push_back((x1));
            object_top.push_back((y1));
            object_right.push_back((x2));
            object_bottom.push_back((y2));

            i_out++;
        }
    }

    return i_out;
}

int process
(rknn_output outputs[rknn_model::YOLO_OUTPUT::NUM_YOLO_LAYER], std::array<rknn_model::YOLO_OUTPUT::detection,
        rknn_model::YOLO_OUTPUT::DETECTION_SIZE> & detections, float* p_anchors, int net_width, int net_height,
        std::vector<int>& class_id, std::vector<float>& confidence, std::vector<int>& object_left, std::vector<int>& object_top, std::vector<int>& object_right, std::vector<int>& object_bottom)
{
    int num_out;

    for (int i = 0; i < rknn_model::YOLO_OUTPUT::NBOXES_TOTAL; ++i) {
        detections[i].bbox.x = rknn_model::YOLO_OUTPUT::FLOAT_ZERO;
        detections[i].bbox.y = rknn_model::YOLO_OUTPUT::FLOAT_ZERO;
        detections[i].bbox.w = rknn_model::YOLO_OUTPUT::FLOAT_ZERO;
        detections[i].bbox.h = rknn_model::YOLO_OUTPUT::FLOAT_ZERO;
        detections[i].objectness = rknn_model::YOLO_OUTPUT::FLOAT_ZERO;
        detections[i].sort_class = -1;
        detections[i].classes = -1;
        for (int j = 0; j < rknn_model::YOLO_OUTPUT::NUM_CLASSES; ++j) {
            detections[i].prob[j] = rknn_model::YOLO_OUTPUT::FLOAT_ZERO;
        }
    }

    outputs_transform(outputs, detections, p_anchors, net_width, net_height, &num_out);
    do_nms_sort(detections.data(), num_out, rknn_model::YOLO_OUTPUT::NUM_CLASSES, output_threshold.nms_threshold);
//    nboxes_left = do_nms_obj(detections.data(), num_out, rknn_model::YOLO_OUTPUT::NUM_CLASSES, output_threshold.nms_threshold);
    num_out = convert_to_output(detections, num_out, output_threshold.object_score_threshold, class_id, confidence, object_left, object_top, object_right, object_bottom);

    return num_out;
}

int yolo_tiny_post_process
(const std::vector<rknn_output>& outputs, std::array<rknn_model::YOLO_OUTPUT::detection, rknn_model::YOLO_OUTPUT::DETECTION_SIZE> & detections,
        float* anchors, int net_width, int net_height,
        std::vector<int> & class_id, std::vector<float> & confidence, std::vector<int> & object_left, std::vector<int> & object_top, std::vector<int> & object_right, std::vector<int> & object_bottom)
{
    int num_out;
    if (outputs.data() == nullptr) {
        LOG_E("%s(%d): yolo out is nullptr\n", __func__, __LINE__);
        return -1;
    }

    if ((object_left.data() == nullptr) ||
        (object_top.data() == nullptr) ||
        (object_right.data() == nullptr) ||
        (object_bottom.data() == nullptr)) {
        LOG_E("%s(%d): objects position array is null\n", __func__, __LINE__);
        return -1;
    }

    num_out = process(const_cast<rknn_output *>(outputs.data()), detections, anchors, net_width, net_height, class_id, confidence, object_left, object_top, object_right, object_bottom);

    return num_out;
}

int yolo_post_process_threshold_init(float nms, float object, float class_probability, float object_score) {
    output_threshold.class_probability_threshold = class_probability;
    output_threshold.nms_threshold = nms;
    output_threshold.object_score_threshold = object_score;
    output_threshold.object_threshold = object;
    return 0;
}


int yolo_set_original_image_size(int width, int height)
{
    if (width > 0) {
        original_image_size.width = width;
    }

    if (height) {
        original_image_size.height = height;
    }
    return 0;
}