//
// Created by nagita on 2019/06/17.
//

#ifndef C3VIEWER_RKNN_MODEL_H
#define C3VIEWER_RKNN_MODEL_H

#include <android/log.h>


#define LOG_I(...) __android_log_print(ANDROID_LOG_INFO, "rknn_model", ##__VA_ARGS__);
#define LOG_E(...) __android_log_print(ANDROID_LOG_ERROR, "rknn_model", ##__VA_ARGS__);

#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <unordered_set>
#include <vector>
#include <string.h>
#include <fcntl.h>
#include <array>
#include <algorithm>
#include <tuple>
#if _MSC_VER
#else
#include <getopt.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#endif

#ifndef __ANDROID__
#define LOG_I printf
#define LOG_E printf
#endif

#include "rknn_api.h"

constexpr auto SUCCESS = 0;

namespace rknn_model {
    class RknnModel
    {
    public:
        RknnModel();
        RknnModel(std::string param_path);

        virtual int release_post_process() { return 0; }
        virtual int run_post_process() = 0;

        virtual int create_post_process() { return 0; }
        int release_outputs();
        std::vector<std::tuple<void*, int>> get_outputs();
        int set_output(int index, bool want_flaot, bool is_prealloc, int size, void* buffer);
        int run_inference();
        int set_input(int input_index, char* buffer, int size, rknn_tensor_type type = RKNN_TENSOR_UINT8, rknn_tensor_format fmt = RKNN_TENSOR_NHWC);
        int set_input(int index, void* buffer, int size, bool pass_through, rknn_tensor_type type, rknn_tensor_format fmt);
        int model_init(std::string param_path);
        int model_init();
        bool is_init_done() noexcept { return this->init_done; }
        int get_output_count() noexcept { return this->m_output_count; }
        int get_input_count() noexcept { return this->m_input_count; }
        int get_infer_time();
        ~RknnModel();

    protected:

        int create_model();
        int get_io_count();
        std::vector<rknn_tensor_attr> get_outputs_attribute();

        rknn_context ctx = {};
        std::string m_param_path = {};
        int m_input_count = {};
        int m_output_count = {};
        std::vector<rknn_input> m_inputs = {};
        std::vector<rknn_output> m_outputs = {};
        std::vector<rknn_tensor_attr> m_outputs_attribute = {};
        bool init_done = false;
    };




}


#endif //C3VIEWER_RKNN_MODEL_H
