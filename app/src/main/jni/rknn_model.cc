//
// Created by nagita on 2019/06/17.
//
#include "rknn_model.h"
#include <utility>
#include <time.h>

static double now_ms(void)
{
    struct timespec res;
    clock_gettime(CLOCK_REALTIME, &res);
    return 1000.0 * res.tv_sec + (double)res.tv_nsec / 1e6;
}

namespace rknn_model {
    RknnModel::RknnModel() = default;

    RknnModel::RknnModel(std::string param_path)
            : m_param_path(std::move(param_path))
    {
        this->model_init();
    }

    int RknnModel::release_outputs()
    {
        return rknn_outputs_release(this->ctx, static_cast<uint32_t>(this->m_output_count), this->m_outputs.data());
    }

    std::vector<std::tuple<void *, int>> RknnModel::get_outputs()
    {
//        LOG_I("%s(%d): in\n", __FUNCTION__, __LINE__);
        std::vector<std::tuple<void *, int>> outputs = {};
        outputs.resize(static_cast<unsigned long>(this->m_output_count));
//        LOG_I("%s(%d): rknn outputs get start\n", __FUNCTION__, __LINE__);
        double start = now_ms();
        rknn_outputs_get(this->ctx, static_cast<uint32_t>(this->m_output_count), this->m_outputs.data(), nullptr);
        double end = now_ms();
        LOG_I("%s(%d): rknn outputs get timing: %f ms\n", __FUNCTION__, __LINE__, end - start);
        auto itr = this->m_outputs.begin();
        std::for_each(outputs.begin(), outputs.end(), [&itr](auto& itb) { itb = std::make_tuple((*itr).buf, (*itr).size); *itr++; });
//        LOG_I("%s(%d): out\n", __FUNCTION__, __LINE__);
        return outputs;
    }

    int RknnModel::set_output(int index, bool want_flaot, bool is_prealloc, int size, void* buffer)
    {
        if (index < this->m_output_count) {
            this->m_outputs[index].index = static_cast<uint32_t>(index);
            this->m_outputs[index].want_float = static_cast<uint8_t>(want_flaot);
            this->m_outputs[index].is_prealloc = static_cast<uint8_t>(is_prealloc);
            if (this->m_outputs[index].is_prealloc)
            {
                this->m_outputs[index].size = static_cast<uint32_t>(size);
                this->m_outputs[index].buf = buffer;
            }
        } else {
            return -1;
        }
        return 0;
    }

    int RknnModel::run_inference()
    {
        int ret;

        try {
            ret = rknn_inputs_set(this->ctx, static_cast<uint32_t>(this->m_input_count), this->m_inputs.data());
            if (ret < 0) {
                LOG_E("rknn_input_set fail! ret=%d\n", ret);
                return -1;
            }
        } catch (...) {
            throw;
        }


        ret = rknn_run(this->ctx, nullptr);
        if (ret < 0) {
            LOG_E("rknn run fail! ret=%d\n", ret);
            return -1;
        }
        return 0;
    }

    int RknnModel::set_input(int input_index, char* buffer, int size, rknn_tensor_type type, rknn_tensor_format fmt)
    {
        return this->set_input(input_index, buffer, size, false, type, fmt);
    }

    int RknnModel::model_init(std::string param_path)
    {
        this->m_param_path = std::move(param_path);
        return this->model_init();
    }

    int RknnModel::model_init()
    {
        if (SUCCESS == this->create_model()) {
            this->get_io_count();
            if ((this->m_input_count > 0) && (this->m_output_count > 0)) {
                this->m_inputs.resize(static_cast<unsigned long>(this->m_input_count));
                this->m_outputs.resize(static_cast<unsigned long>(this->m_output_count));
                this->init_done = true;
            }
        }

        return 0;
    }

    int RknnModel::create_model()
    {
        std::ifstream ifs;
        std::ifstream::pos_type pos;
        ifs.open(this->m_param_path.c_str(), std::ios::in | std::ios::binary | std::ios::ate);
        if (!ifs.is_open()) {
            LOG_E("load model file failure !! \n");
            return -1;
        }
        pos = (int)ifs.tellg();
        void* model = new char[pos];
        ifs.seekg(0, std::ios::beg);
        ifs.read((char*)model, pos);
        if (ifs.fail() || ifs.bad()) {
            LOG_E("fread %s failure !!\n", m_param_path.c_str());
            delete[] static_cast<char *>(model);
            ifs.close();
            return -1;
        }
        ifs.close();
        LOG_I("load model success\n");

        LOG_I("run rknn_init\n");
        int ret = rknn_init(&this->ctx, model, static_cast<uint32_t>(pos), RKNN_FLAG_PRIOR_MEDIUM);
        delete[] static_cast<char *>(model);

        if (ret < 0) {
            LOG_E("rknn_init failed !! ret = %d\n", ret);
            return -1;
        }

        LOG_I("rknn_init success\n");

        return 0;
    }

    int RknnModel::set_input(int index, void* buffer, int size, bool pass_through, rknn_tensor_type type, rknn_tensor_format fmt)
    {
        if (index < this->m_input_count) {
            this->m_inputs[index].index = static_cast<uint32_t>(index);
            this->m_inputs[index].buf = buffer;
            this->m_inputs[index].size = static_cast<uint32_t>(size);
            this->m_inputs[index].pass_through = static_cast<uint8_t>(pass_through);
            this->m_inputs[index].type = type;
            this->m_inputs[index].fmt = fmt;
        } else {
            return -1;
        }
        return 0;
    }

    int RknnModel::get_io_count()
    {
        LOG_I("io_query...\n");
        int ret;
        rknn_input_output_num io_num;
        ret = rknn_query(this->ctx, RKNN_QUERY_IN_OUT_NUM, &io_num, sizeof(io_num));
        if (ret < 0) {
            LOG_E("rknn_query fail! ret=%d\n", ret);
            return -1;
        }

        this->m_input_count = io_num.n_input;
        this->m_output_count = io_num.n_output;

        if (this->m_input_count > 0) {
            this->m_inputs.clear();
            this->m_inputs.resize(static_cast<unsigned long>(this->m_input_count));
        }

        if (this->m_output_count > 0) {
            this->m_outputs.clear();
            this->m_outputs.resize(static_cast<unsigned long>(this->m_output_count));
        }

        LOG_I("io_query success\n");
        return 0;
    }

    int RknnModel::get_infer_time()
    {
        int ret;
        rknn_perf_run perf_run;
        ret = rknn_query(this->ctx, RKNN_QUERY_PERF_RUN, &perf_run, sizeof(rknn_perf_run));
        if (ret < 0) {
            LOG_E("rknn_query fail! ret=%d\n", ret);
            return -1;
        }
        LOG_I("Infer time = %ld\n", perf_run);
        return 0;
    }

    std::vector<rknn_tensor_attr> RknnModel::get_outputs_attribute()
    {
        LOG_I("io_query...\n");
        int ret;
        if (this->m_output_count > 0)
        {
            this->m_outputs_attribute.clear();
            this->m_outputs_attribute.resize(static_cast<unsigned long>(this->m_output_count));
            for (size_t i_out = 0; i_out < this->m_output_count; i_out++)
            {
                this->m_outputs_attribute[i_out].index = static_cast<uint32_t>(i_out);
                ret = rknn_query(this->ctx, RKNN_QUERY_OUTPUT_ATTR, &(this->m_outputs_attribute[i_out]), sizeof(this->m_outputs_attribute[i_out]));
                if (ret < 0) {
                    LOG_E("rknn_query fail! ret=%d\n", ret);
                    break;
                }
            }
        }

        LOG_I("io_query done\n");
        return this->m_outputs_attribute;
    }

    RknnModel::~RknnModel() = default;
}