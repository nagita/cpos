//
// Created by nagita on 2019/08/06.
//

#include "mobilenet_model.h"

namespace rknn_model {
    MobileNetModel::MobileNetModel() = default;
    MobileNetModel::~MobileNetModel() = default;
    int MobileNetModel::create_post_process()
    {
        return 0;
    }

    int MobileNetModel::run_post_process()
    {
        this->m_confidence.resize(MOBILENET_OUTPUT::NUM_ITEMS);
        auto * p_data = static_cast<float *>(this->m_outputs[0].buf);
        for (int i = 0; i < MOBILENET_OUTPUT::NUM_ITEMS; i ++) {
            this->m_confidence[i] = p_data[i];
        }

        return 0;
    }

    int MobileNetModel::release_post_process() {
        return RknnModel::release_post_process();
    }
}