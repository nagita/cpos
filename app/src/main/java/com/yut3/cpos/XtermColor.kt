package com.yut3.cpos


import android.graphics.Color


class XtermColor {
    fun getXtermRgbCode(num: Int): Int {
        when (num) {
            0  -> return Color.rgb(0,0,0)
            1  -> return Color.rgb(128,0,0)
            2  -> return Color.rgb(0,128,0)
            3  -> return Color.rgb(128,128,0)
            4  -> return Color.rgb(0,0,128)
            5  -> return Color.rgb(128,0,128)
            6  -> return Color.rgb(0,128,128)
            7  -> return Color.rgb(192,192,192)
            8  -> return Color.rgb(128,128,128)
            9  -> return Color.rgb(255,0,0)
            10 -> return Color.rgb(0,255,0)
            11 -> return Color.rgb(255,255,0)
            12 -> return Color.rgb(0,0,255)
            13 -> return Color.rgb(255,0,255)
            14 -> return Color.rgb(0,255,255)
            15 -> return Color.rgb(255,255,255)
            16 -> return Color.rgb(0,0,0)
            17 -> return Color.rgb(0,0,95)
            18 -> return Color.rgb(0,0,135)
            19 -> return Color.rgb(0,0,175)
            20 -> return Color.rgb(0,0,215)
            21 -> return Color.rgb(0,0,255)
            22 -> return Color.rgb(0,95,0)
            23 -> return Color.rgb(0,95,95)
            24 -> return Color.rgb(0,95,135)
            25 -> return Color.rgb(0,95,175)
            26 -> return Color.rgb(0,95,215)
            27 -> return Color.rgb(0,95,255)
            28 -> return Color.rgb(0,135,0)
            29 -> return Color.rgb(0,135,95)
            30 -> return Color.rgb(0,135,135)
            31 -> return Color.rgb(0,135,175)
            32 -> return Color.rgb(0,135,215)
            33 -> return Color.rgb(0,135,255)
            34 -> return Color.rgb(0,175,0)
            35 -> return Color.rgb(0,175,95)
            36 -> return Color.rgb(0,175,135)
            37 -> return Color.rgb(0,175,175)
            38 -> return Color.rgb(0,175,215)
            39 -> return Color.rgb(0,175,255)
            40 -> return Color.rgb(0,215,0)
            41 -> return Color.rgb(0,215,95)
            42 -> return Color.rgb(0,215,135)
            43 -> return Color.rgb(0,215,175)
            44 -> return Color.rgb(0,215,215)
            45 -> return Color.rgb(0,215,255)
            46 -> return Color.rgb(0,255,0)
            47 -> return Color.rgb(0,255,95)
            48 -> return Color.rgb(0,255,135)
            49 -> return Color.rgb(0,255,175)
            50 -> return Color.rgb(0,255,215)
            51 -> return Color.rgb(0,255,255)
            52 -> return Color.rgb(95,0,0)
            53 -> return Color.rgb(95,0,95)
            54 -> return Color.rgb(95,0,135)
            55 -> return Color.rgb(95,0,175)
            56 -> return Color.rgb(95,0,215)
            57 -> return Color.rgb(95,0,255)
            58 -> return Color.rgb(95,95,0)
            59 -> return Color.rgb(95,95,95)
            60 -> return Color.rgb(95,95,135)
            61 -> return Color.rgb(95,95,175)
            62 -> return Color.rgb(95,95,215)
            63 -> return Color.rgb(95,95,255)
            64 -> return Color.rgb(95,135,0)
            65 -> return Color.rgb(95,135,95)
            66 -> return Color.rgb(95,135,135)
            67 -> return Color.rgb(95,135,175)
            68 -> return Color.rgb(95,135,215)
            69 -> return Color.rgb(95,135,255)
            70 -> return Color.rgb(95,175,0)
            71 -> return Color.rgb(95,175,95)
            72 -> return Color.rgb(95,175,135)
            73 -> return Color.rgb(95,175,175)
            74 -> return Color.rgb(95,175,215)
            75 -> return Color.rgb(95,175,255)
            76 -> return Color.rgb(95,215,0)
            77 -> return Color.rgb(95,215,95)
            78 -> return Color.rgb(95,215,135)
            79 -> return Color.rgb(95,215,175)
            80 -> return Color.rgb(95,215,215)
            81 -> return Color.rgb(95,215,255)
            82 -> return Color.rgb(95,255,0)
            83 -> return Color.rgb(95,255,95)
            84 -> return Color.rgb(95,255,135)
            85 -> return Color.rgb(95,255,175)
            86 -> return Color.rgb(95,255,215)
            87 -> return Color.rgb(95,255,255)
            88 -> return Color.rgb(135,0,0)
            89 -> return Color.rgb(135,0,95)
            90 -> return Color.rgb(135,0,135)
            91 -> return Color.rgb(135,0,175)
            92 -> return Color.rgb(135,0,215)
            93 -> return Color.rgb(135,0,255)
            94 -> return Color.rgb(135,95,0)
            95 -> return Color.rgb(135,95,95)
            96 -> return Color.rgb(135,95,135)
            97 -> return Color.rgb(135,95,175)
            98 -> return Color.rgb(135,95,215)
            99 -> return Color.rgb(135,95,255)
            100-> return Color.rgb(135,135,0)
            101-> return Color.rgb(135,135,95)
            102-> return Color.rgb(135,135,135)
            103-> return Color.rgb(135,135,175)
            104-> return Color.rgb(135,135,215)
            105-> return Color.rgb(135,135,255)
            106-> return Color.rgb(135,175,0)
            107-> return Color.rgb(135,175,95)
            108-> return Color.rgb(135,175,135)
            109-> return Color.rgb(135,175,175)
            110-> return Color.rgb(135,175,215)
            111-> return Color.rgb(135,175,255)
            112-> return Color.rgb(135,215,0)
            113-> return Color.rgb(135,215,95)
            114-> return Color.rgb(135,215,135)
            115-> return Color.rgb(135,215,175)
            116-> return Color.rgb(135,215,215)
            117-> return Color.rgb(135,215,255)
            118-> return Color.rgb(135,255,0)
            119-> return Color.rgb(135,255,95)
            120-> return Color.rgb(135,255,135)
            121-> return Color.rgb(135,255,175)
            122-> return Color.rgb(135,255,215)
            123-> return Color.rgb(135,255,255)
            124-> return Color.rgb(175,0,0)
            125-> return Color.rgb(175,0,95)
            126-> return Color.rgb(175,0,135)
            127-> return Color.rgb(175,0,175)
            128-> return Color.rgb(175,0,215)
            129-> return Color.rgb(175,0,255)
            130-> return Color.rgb(175,95,0)
            131-> return Color.rgb(175,95,95)
            132-> return Color.rgb(175,95,135)
            133-> return Color.rgb(175,95,175)
            134-> return Color.rgb(175,95,215)
            135-> return Color.rgb(175,95,255)
            136-> return Color.rgb(175,135,0)
            137-> return Color.rgb(175,135,95)
            138-> return Color.rgb(175,135,135)
            139-> return Color.rgb(175,135,175)
            140-> return Color.rgb(175,135,215)
            141-> return Color.rgb(175,135,255)
            142-> return Color.rgb(175,175,0)
            143-> return Color.rgb(175,175,95)
            144-> return Color.rgb(175,175,135)
            145-> return Color.rgb(175,175,175)
            146-> return Color.rgb(175,175,215)
            147-> return Color.rgb(175,175,255)
            148-> return Color.rgb(175,215,0)
            149-> return Color.rgb(175,215,95)
            150-> return Color.rgb(175,215,135)
            151-> return Color.rgb(175,215,175)
            152-> return Color.rgb(175,215,215)
            153-> return Color.rgb(175,215,255)
            154-> return Color.rgb(175,255,0)
            155-> return Color.rgb(175,255,95)
            156-> return Color.rgb(175,255,135)
            157-> return Color.rgb(175,255,175)
            158-> return Color.rgb(175,255,215)
            159-> return Color.rgb(175,255,255)
            160-> return Color.rgb(215,0,0)
            161-> return Color.rgb(215,0,95)
            162-> return Color.rgb(215,0,135)
            163-> return Color.rgb(215,0,175)
            164-> return Color.rgb(215,0,215)
            165-> return Color.rgb(215,0,255)
            166-> return Color.rgb(215,95,0)
            167-> return Color.rgb(215,95,95)
            168-> return Color.rgb(215,95,135)
            169-> return Color.rgb(215,95,175)
            170-> return Color.rgb(215,95,215)
            171-> return Color.rgb(215,95,255)
            172-> return Color.rgb(215,135,0)
            173-> return Color.rgb(215,135,95)
            174-> return Color.rgb(215,135,135)
            175-> return Color.rgb(215,135,175)
            176-> return Color.rgb(215,135,215)
            177-> return Color.rgb(215,135,255)
            178-> return Color.rgb(215,175,0)
            179-> return Color.rgb(215,175,95)
            180-> return Color.rgb(215,175,135)
            181-> return Color.rgb(215,175,175)
            182-> return Color.rgb(215,175,215)
            183-> return Color.rgb(215,175,255)
            184-> return Color.rgb(215,215,0)
            185-> return Color.rgb(215,215,95)
            186-> return Color.rgb(215,215,135)
            187-> return Color.rgb(215,215,175)
            188-> return Color.rgb(215,215,215)
            189-> return Color.rgb(215,215,255)
            190-> return Color.rgb(215,255,0)
            191-> return Color.rgb(215,255,95)
            192-> return Color.rgb(215,255,135)
            193-> return Color.rgb(215,255,175)
            194-> return Color.rgb(215,255,215)
            195-> return Color.rgb(215,255,255)
            196-> return Color.rgb(255,0,0)
            197-> return Color.rgb(255,0,95)
            198-> return Color.rgb(255,0,135)
            199-> return Color.rgb(255,0,175)
            200-> return Color.rgb(255,0,215)
            201-> return Color.rgb(255,0,255)
            202-> return Color.rgb(255,95,0)
            203-> return Color.rgb(255,95,95)
            204-> return Color.rgb(255,95,135)
            205-> return Color.rgb(255,95,175)
            206-> return Color.rgb(255,95,215)
            207-> return Color.rgb(255,95,255)
            208-> return Color.rgb(255,135,0)
            209-> return Color.rgb(255,135,95)
            210-> return Color.rgb(255,135,135)
            211-> return Color.rgb(255,135,175)
            212-> return Color.rgb(255,135,215)
            213-> return Color.rgb(255,135,255)
            214-> return Color.rgb(255,175,0)
            215-> return Color.rgb(255,175,95)
            216-> return Color.rgb(255,175,135)
            217-> return Color.rgb(255,175,175)
            218-> return Color.rgb(255,175,215)
            219-> return Color.rgb(255,175,255)
            220-> return Color.rgb(255,215,0)
            221-> return Color.rgb(255,215,95)
            222-> return Color.rgb(255,215,135)
            223-> return Color.rgb(255,215,175)
            224-> return Color.rgb(255,215,215)
            225-> return Color.rgb(255,215,255)
            226-> return Color.rgb(255,255,0)
            227-> return Color.rgb(255,255,95)
            228-> return Color.rgb(255,255,135)
            229-> return Color.rgb(255,255,175)
            230-> return Color.rgb(255,255,215)
            231-> return Color.rgb(255,255,255)
            232-> return Color.rgb(8,8,8)
            233-> return Color.rgb(18,18,18)
            234-> return Color.rgb(28,28,28)
            235-> return Color.rgb(38,38,38)
            236-> return Color.rgb(48,48,48)
            237-> return Color.rgb(58,58,58)
            238-> return Color.rgb(68,68,68)
            239-> return Color.rgb(78,78,78)
            240-> return Color.rgb(88,88,88)
            241-> return Color.rgb(98,98,98)
            242-> return Color.rgb(108,108,108)
            243-> return Color.rgb(118,118,118)
            244-> return Color.rgb(128,128,128)
            245-> return Color.rgb(138,138,138)
            246-> return Color.rgb(148,148,148)
            247-> return Color.rgb(158,158,158)
            248-> return Color.rgb(168,168,168)
            249-> return Color.rgb(178,178,178)
            250-> return Color.rgb(188,188,188)
            251-> return Color.rgb(198,198,198)
            252-> return Color.rgb(208,208,208)
            253-> return Color.rgb(218,218,218)
            254-> return Color.rgb(228,228,228)
            255-> return Color.rgb(238,238,238)
            else -> return -1
        }
    }
}