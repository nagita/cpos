package com.yut3.cpos.data.model

data class OrderItem(
    var id: Int,
    var name: String,
    var barcode: String,
    var count: Int,
    var price: Double,
    var specialOffer: Double
) {
}