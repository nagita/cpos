package com.yut3.cpos.data

import android.graphics.Bitmap

class InferenceResult {
    //    var outputObjects: MutableList<Quadruple<Int, Int, Float, Rect>> = arrayListOf() // idx, classId, confidence, boundingBox
    var outputObjects: MutableList<RObject> = arrayListOf()
    var inferenceImage: Bitmap? = null
    var objectCount: Int = 0
    var dataArray: MutableList<Float> = arrayListOf()
}