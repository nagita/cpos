package com.yut3.cpos.data

import android.graphics.Bitmap
import android.graphics.Rect

data class IObject(var coordinate: Rect, var tag: String, var confidence: Float, var bitmap: Bitmap?, var tagId: Int?, var ean13: String?, var candidateTagName: MutableList<String>?)