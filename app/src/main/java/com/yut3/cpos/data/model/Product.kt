package com.yut3.cpos.data.model

import java.util.*

data class Product(
    var uuid: UUID,
    var uid: String,
    var id: Int,
    var categoryLevel: CategoryLevel,
    var barcode: Barcode,
    var price: Double,
    var specialOffer: Double
) {
}