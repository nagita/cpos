package com.yut3.cpos.data.model

enum class RuntimeOperatingMode {
    TYPE_END_USER,
    TYPE_ENGINERRING,
    TYPE_DEVELOPER
}