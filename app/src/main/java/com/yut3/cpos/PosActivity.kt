package com.yut3.cpos


import android.Manifest
import android.annotation.TargetApi
import android.app.Dialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Point
import android.hardware.Camera
import android.hardware.camera2.CameraAccessException
import android.os.*
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.os.ConfigurationCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.listItems
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.DrawableImageViewTarget
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.dialog.MaterialDialogs
import com.google.android.material.shape.CutCornerTreatment
import com.google.android.material.shape.TriangleEdgeTreatment
import com.google.android.material.snackbar.Snackbar
import com.google.zxing.BarcodeFormat
import com.google.zxing.ResultPoint
import com.google.zxing.client.android.BeepManager
import com.gurutouchlabs.kenneth.elegantdialog.ElegantActionListeners
import com.gurutouchlabs.kenneth.elegantdialog.ElegantDialog
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.DecoratedBarcodeView
import com.journeyapps.barcodescanner.DefaultDecoderFactory
import com.journeyapps.barcodescanner.camera.CameraSettings
import com.yut3.cpos.data.model.OrderItem
import com.yut3.cpos.data.model.Product
import com.yut3.cpos.ui.pos.CameraDetectViewModel
import com.yut3.cpos.ui.pos.CameraFragment
import com.yut3.cpos.ui.pos.ProductFragment
import com.yut3.cpos.ui.pos.dummy.ForSaleProduct
import kotlinx.android.synthetic.main.pos_activity.*
import org.opencv.android.OpenCVLoader
import java.lang.ref.WeakReference
import java.util.*

class PosActivity : AppCompatActivity(),
    OrderFragment.OnListFragmentInteractionListener,
    ProductFragment.OnListFragmentInteractionListener,
    OrderFragment.OnUpdateOrderListListener {
    //OrderFragment.OnLongClickOrderListListener{


    private lateinit var mBeepManager: BeepManager
    private lateinit var mBarcodeView: DecoratedBarcodeView
    private var mBarcodeScanLastTime: Long = 0

    private lateinit var mCPosSharedViewModel: CPosSharedViewModel
    private lateinit var mCameraDetectViewModel: CameraDetectViewModel

    private var mBackgroundUpdateUiHandler: Handler? = null

    private var mIsRk3399Pro: Boolean = false
    //private var mStopCameraDetect: Boolean = false

    private lateinit var mOrderFragment: OrderFragment
    private lateinit var mProductFragment: ProductFragment
    private lateinit var mCameraFragment: CameraFragment
    private var mProgressDialog: ProgressDialog? = null

    private var advertisePlayIndex: Int = 0

    override fun onUpdateOrderList() {
        mBackgroundUpdateUiHandler!!.sendMessage(mBackgroundUpdateUiHandler!!.obtainMessage(
            UPDATE_UI_DATE_MESSAGE))
    }

    //override fun onLongClickOrderListListener(item: Product?) {

    //}


    override fun onListFragmentInteraction(item: Product?) {
        when {
            item!!.id == -1 -> { // previous button
                val productFragment = supportFragmentManager.findFragmentById(R.id.product_list_container) as ProductFragment
                val productList = ForSaleProduct.createDisplayList(item.categoryLevel.getPreviousLevel())
                productFragment.setProductList(productList)
            }
            item.id == 0 -> { // title button
                val productFragment = supportFragmentManager.findFragmentById(R.id.product_list_container) as ProductFragment
                val productList = ForSaleProduct.createDisplayList(item.categoryLevel)
                productFragment.setProductList(productList)
            }
            else -> { // product item
//                val inflater = this.layoutInflater
                val dialogView = View.inflate(applicationContext, R.layout.dialog_item_counter,null)

                val systemUiFlag: Int = (View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_FULLSCREEN
                        or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION)


                title = item.uid

                val dialog2 = ElegantDialog(this)
                    .setTitleIcon(resources.getDrawable(R.drawable.ic_local_offer_black_120dp))
                    .setBackgroundTopColor(Color.parseColor("#64FC2C"))
                    .setCustomView(R.layout.dialog_item_counter)
                    .setCornerRadius(50.0f)
                    .setCanceledOnTouchOutside(true)
                    .setTitleHidden(false)
                    .setElegantActionClickListener(object: ElegantActionListeners {
                        override fun onCancelListener(dialog: DialogInterface) {
                            dialog.dismiss()
                            setTitle(R.string.app_name)
                        }

                        override fun onGotItListener(dialog: ElegantDialog) {
                            dialog.dismiss()
                            setTitle(R.string.app_name)
                        }

                        override fun onNegativeListener(dialog: ElegantDialog) {
                            dialog.dismiss()
                            setTitle(R.string.app_name)
                        }

                        override fun onPositiveListener(dialog: ElegantDialog) {
                            // add item to order list
                            val itemCountTextView = dialog.getCustomView()!!.findViewById<TextView>(R.id.tvItemCount)
                            val itemCount: Int = itemCountTextView.text.toString().toInt()
                            for (i in 0 until itemCount) {
                                mCPosSharedViewModel.addOrderProduct(item)
                            }
                            mCPosSharedViewModel.setBeepSound()
                            val orderFragment = supportFragmentManager.findFragmentById(R.id.order_list_container) as OrderFragment
                            orderFragment.updateOrderList()

                            mBackgroundUpdateUiHandler!!.sendMessage(mBackgroundUpdateUiHandler!!.obtainMessage(
                                UPDATE_UI_DATE_MESSAGE))
                            dialog.dismiss()
                            setTitle(R.string.app_name)
                        }
                    })
                    .show()

                if (dialog2.getTitleIconView() != null) {
                    dialog2.getTitleTextView()!!.systemUiVisibility = systemUiFlag
                    dialog2.getCustomView()!!.systemUiVisibility = systemUiFlag
                    dialog2.getContentTextView()!!.systemUiVisibility = systemUiFlag
                    dialog2.getGotItButtonIconView()!!.systemUiVisibility = systemUiFlag
                    dialog2.getNegativeButtonIconView()!!.systemUiVisibility = systemUiFlag
                    dialog2.getPositiveButtonIconView()!!.systemUiVisibility = systemUiFlag
                    dialog2.getGotItButtonTextView()!!.systemUiVisibility = systemUiFlag
                    dialog2.getNegativeButtonTextView()!!.systemUiVisibility = systemUiFlag
                    dialog2.getPositiveButtonTextView()!!.systemUiVisibility = systemUiFlag

//                    dialog2.getTitleTextView()!!.visibility = View.GONE
                    dialog2.getTitleTextView()!!.text = item.uid
                    dialog2.getTitleTextView()!!.textSize = 44.0f
                    dialog2.getTitleTextView()!!.setTextColor(Color.parseColor("#404040"))

                    dialog2.getPositiveButtonIconView()!!.visibility = View.GONE
                    dialog2.getPositiveButtonTextView()!!.text = "OK" //Set positive button text
                    dialog2.getPositiveButtonTextView()!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 50.0f)

                    dialog2.getCustomView()!!
                    val buttonPlus = dialog2.getCustomView()!!.findViewById<Button>(R.id.btnPlus)

                    buttonPlus!!.setOnClickListener {
                        val itemCountTextView = dialog2.getCustomView()!!.findViewById<TextView>(R.id.tvItemCount)
                        var counter = itemCountTextView.text.toString().toInt()
                        counter += 1
                        itemCountTextView.text = counter.toString()
                    }

                    dialog2.getCustomView()!!.findViewById<Button>(R.id.btnMinus)!!.setOnClickListener {
                        val itemCountTextView = dialog2.getCustomView()!!.findViewById<TextView>(R.id.tvItemCount)
                        var counter = itemCountTextView.text.toString().toInt()
                        if (counter > 0) counter -= 1
                        itemCountTextView.text = counter.toString()
                    }

                    val hideGotItButton = true
                    val hidePositiveButton = false
                    val hideNegativeButton = true
                    dialog2.getPositiveButton()!!.visibility = if (hidePositiveButton) View.GONE else View.VISIBLE //Hide positive button
                    dialog2.getNegativeButton()!!.visibility = if (hideNegativeButton) View.GONE else View.VISIBLE //Hide negative button
                    dialog2.getGotItButton()!!.visibility = if (hideGotItButton) View.GONE else View.VISIBLE  //Hide got it button
                }


//                val dialogView = View.inflate(applicationContext, R.layout.dialog_item_counter,null)
//                val dialog = AlertDialog.Builder(this)
//                    .setView(dialogView)
//                    .setPositiveButton("OK") { _, _ ->
//                        // add item to order list
//                        val itemCountTextView = dialogView.findViewById<TextView>(R.id.tvItemCount)
//                        val itemCount: Int = itemCountTextView.text.toString().toInt()
//                        for (i in 0 until itemCount) {
//                            mCPosSharedViewModel.addOrderProduct(item)
//                        }
//                        val orderFragment = supportFragmentManager.findFragmentById(R.id.order_list_container) as OrderFragment
//                        orderFragment.updateOrderList()
//
//                        mBackgroundUpdateUiHandler!!.sendMessage(mBackgroundUpdateUiHandler!!.obtainMessage(
//                            UPDATE_UI_DATE_MESSAGE))
//                    }
//                    .setNeutralButton("Cancel") { _, _ ->
//
//                    }
//                    .create()
////                dialog.setOnShowListener {
////                    val btnPositive = dialog.getButton(Dialog.BUTTON_POSITIVE)
////                    btnPositive.textSize = 40.0f
////                }
//                val buttonPlus = dialogView.findViewById<Button>(R.id.btnPlus)
//
//                buttonPlus!!.setOnClickListener {
//                    val itemCountTextView = dialogView.findViewById<TextView>(R.id.tvItemCount)
//                    var counter = itemCountTextView.text.toString().toInt()
//                    counter += 1
//                    itemCountTextView.text = counter.toString()
//                }
//
//                dialogView.findViewById<Button>(R.id.btnMinus)!!.setOnClickListener {
//                    val itemCountTextView = dialogView.findViewById<TextView>(R.id.tvItemCount)
//                    var counter = itemCountTextView.text.toString().toInt()
//                    if (counter > 0) counter -= 1
//                    itemCountTextView.text = counter.toString()
//                }
//                dialog.show()
//
//                dialog.getButton(Dialog.BUTTON_POSITIVE)
//                    .setTextSize(TypedValue.COMPLEX_UNIT_SP, 50.0f)
//
//                dialog.getButton(Dialog.BUTTON_NEUTRAL)
//                    .setTextSize(TypedValue.COMPLEX_UNIT_SP, 50.0f)


            }
        }
    }

    override fun onListFragmentInteraction(item: OrderItem?) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE
                // Set the content to appear under the system bars so that the
                // content doesn't resize when the system bars hide and show.
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                // Hide the nav bar and status bar
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN)
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) hideSystemUI()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.pos_activity)

        OpenCVLoader.initDebug()

        if (savedInstanceState == null) {
            mOrderFragment = OrderFragment.newInstance(1)
            mProductFragment = ProductFragment.newInstance(4)
            mCameraFragment = CameraFragment.newInstance(this)

            supportFragmentManager.beginTransaction()
                .add(R.id.order_list_container, mOrderFragment)
                .add(R.id.product_list_container, mProductFragment)
                .add(R.id.camera_detect_container, mCameraFragment)
//                .replace(R.id.container, PosFragment.newInstance())
//                .show(mOrderFragment)
//                .show(mProductFragment)
//                .hide(mCameraFragment)
                .commit()
        }

        try {
            createBackgroundUpdateUiHandler()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        iv_pos_left.setImageResource(R.mipmap.ic_cpos_launcher)
        iv_pos_right.setImageResource(R.drawable.ic_demo_user)

        button30.setTextColor(Color.WHITE)
        button31.setTextColor(Color.WHITE)

        initCameraDetectRunButtonStyle()

        // do something for rknn init
        if (Build.PRODUCT == PRODUCT_NAME_RK3399PRO) {
            mIsRk3399Pro = true
            try {
                activateRk3399ProFunction()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        loadProduct()
        openCamera()
        createBarcodeView()
        buttonOnClickFunctionInit()

        mCPosSharedViewModel = ViewModelProviders.of(this, ViewModelProvider.AndroidViewModelFactory(application)).get(CPosSharedViewModel::class.java)
        mCPosSharedViewModel.orderProductList.value = arrayListOf()
        mCPosSharedViewModel.orderItemList.value = arrayListOf()
        mCPosSharedViewModel.tempOrderProductList.value = arrayListOf()
        mCPosSharedViewModel.tempOrderItemList.value = arrayListOf()

        advertiserStart()

        displayProductButtonFragment()

        initDisplayTextView()
    }

    private fun createBackgroundUpdateUiHandler() {
        @JvmStatic
        mBackgroundUpdateUiHandler = object : Handler() {
            override fun handleMessage(msg: Message?) {
                super.handleMessage(msg)
                when (msg?.what) {
                    UPDATE_UI_DATE_MESSAGE -> {
                        // update UI component here

                        var itemCount = 0
                        var totalPrice = 0.0
                        mCPosSharedViewModel.getOrderItemList()!!.forEach {
                            itemCount += it.count
                            totalPrice += (it.count * it.specialOffer)
                        }

                        if (ConfigurationCompat.getLocales(resources.configuration)[0].language == "ja") {
                            tv_info_3.text = "点数：${itemCount} 点"
                            tv_info_5.text = "${totalPrice} 円(税込)"
                        } else if (ConfigurationCompat.getLocales(resources.configuration)[0].language == "zh") {
                            tv_info_3.text = "數量：${itemCount} 件"
                            tv_info_5.text = "${totalPrice} 元(含稅)"
                        } else if (ConfigurationCompat.getLocales(resources.configuration)[0].language == "en") {
                            tv_info_3.text = "Count：${itemCount}"
                            tv_info_5.text = "$ ${totalPrice}　"
                        } else {

                        }
                    }
                    BACKGROUND_THREAD_INITIALIZE_RUNNING_MESSAGE -> {
                        @Suppress("DEPRECATION")
                        mProgressDialog = ProgressDialog(this@PosActivity)
                        mProgressDialog!!.setTitle("Loading...")
                        mProgressDialog!!.setMessage("Wait RKNN library initial done")
                        mProgressDialog!!.show()
                    }
                    CAMERA_DETECTED_PRODUCT_MESSAGE -> {
                        //if (!mStopCameraDetect) {
                            var listString = ""
                            val detectResult = mCPosSharedViewModel.getRObjectList()

                            mCPosSharedViewModel.clearTempOrderProduct()

                            for(i in 0 until detectResult.value!!.size) {
                                mCPosSharedViewModel.addTempOrderProduct(ForSaleProduct.getProductById(detectResult.value!![i].iObject.tagId!!))
                            }

                            if(mCPosSharedViewModel.getTempOrderItemList().isNullOrEmpty()) {
                                val alertDialog = AlertDialog.Builder(this@PosActivity)
                                    .setTitle("None of items detected.")
                                    .setMessage(listString)
                                    .setPositiveButton(android.R.string.ok) { _, _ ->
                                    }
                                    .show()
                            }
                            else {
                                mCPosSharedViewModel.getTempOrderItemList()!!.forEach {
                                    listString += "ID: ${it.name} → x${it.count}\r\n"
                                }

                                val alertDialog = AlertDialog.Builder(this@PosActivity)
                                    .setTitle("Add following items to oder list?")
                                    .setMessage(listString)
                                    .setPositiveButton(android.R.string.ok) { _, _ ->
                                        cameraDetectionAddItem()
                                    }
                                    .setNegativeButton(android.R.string.cancel) { _, _ ->
                                    }
                                    .show()
                            }
                        //}
                        camera_detect_run.isEnabled = true
                    }
                    SET_END_USER_INFO_MESSAGE -> {

                        tv_pos_user_info_1.text = intent.getStringExtra("logged_in_end_user_name")
//                        tv_pos_user_info_2.text = intent.getStringExtra("logged_in_end_user_uid")

                        if (ConfigurationCompat.getLocales(resources.configuration)[0].language == "ja") {
                            tv_pos_user_info_2.text = "ちはっすヾ('-'*)ﾉ"
                        } else if (ConfigurationCompat.getLocales(resources.configuration)[0].language == "zh") {
                            tv_pos_user_info_2.text = "歡迎光臨ヾ('-'*)ﾉ"
                        } else if (ConfigurationCompat.getLocales(resources.configuration)[0].language == "en") {
                            tv_pos_user_info_2.text = "Welcomヾ('-'*)ﾉ"
                        } else {

                        }

                    }

                    ADVERTISEMENT_CHANGE_MESSAGE -> {
                        advertisementChange()
                    }
                }
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun requestCameraPermission() {
        if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
            android.app.AlertDialog.Builder(applicationContext)
                .setMessage("Permission Here")
                .setPositiveButton(android.R.string.ok) { _, _ ->
                    requestPermissions(arrayOf(Manifest.permission.CAMERA), 1)
                }
                .setNegativeButton(android.R.string.cancel) { _, _ ->
                    // TODO: can not get camera permission, need to do something
                }
                .create()
        } else {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), 1)
        }
    }

    private fun loadProduct() {
        val productNameMappingList = resources.getStringArray(R.array.productName).toMutableList()
        val productBarCodeMappingList = resources.getStringArray(R.array.productBarcode).toMutableList()
        val productClassMappingList = resources.getStringArray(R.array.productClass).toMutableList()
        ForSaleProduct.createMenuItem()
        ForSaleProduct.setProductPictureList()
        for(i in 0 until productNameMappingList.size) {
            ForSaleProduct.createProductItem(i+1, productNameMappingList[i], productBarCodeMappingList[i], productClassMappingList[i])
        }
    }

    private fun openCamera() {
        try {
            val permission = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.CAMERA)
            if (permission != PackageManager.PERMISSION_GRANTED) {
                requestCameraPermission()
            } else {
                Log.i("POS", "no need to open camera here")
                // open camera here
                // 今回はただのカメラ権限を要求するから
                // カメラの起動は必要がない
            }
        } catch (e: CameraAccessException) {
            Log.e("POS", e.toString())
            e.printStackTrace()
        } catch (e: InterruptedException) {
            Log.e("POS", e.toString())
            e.printStackTrace()
        } catch (e: Exception) {
            Log.e("POS", e.toString())
            e.printStackTrace()
        }
    }

    private fun createBarcodeView() {
        mBarcodeView = findViewById<DecoratedBarcodeView>(R.id.barcode_scanner)
        mBarcodeView.initializeFromIntent(intent)
        val formats: Collection<BarcodeFormat> = Arrays.asList(BarcodeFormat.EAN_13)
        val cameraSettings = CameraSettings()
        cameraSettings.requestedCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT
        mBarcodeView.barcodeView.cameraSettings = cameraSettings
        mBarcodeView.barcodeView.decoderFactory = DefaultDecoderFactory(formats)
        mBarcodeView.decodeContinuous(object : BarcodeCallback {
            override fun barcodeResult(result: BarcodeResult?) {
                if (result!!.text == null) {
                    return
                }
                val currentTime = System.currentTimeMillis()
                if (currentTime - mBarcodeScanLastTime > BARCODE_TIME_INTERVAL) {
                    mBarcodeView.setStatusText(result.text)
                    mBeepManager.playBeepSoundAndVibrate()

//                    val imageView = findViewById<ImageView>(R.id.iv_pos_right)
//                    imageView.setImageBitmap(result.getBitmapWithResultPoints(Color.YELLOW))
                    Log.i("POS", "scanned barcode: ${result.text}")

                    barcodeAddItem(result.text)
                    mBarcodeScanLastTime = currentTime
                } else {
                    Log.i("POS", "ignore result: barcode scan time interval < $BARCODE_TIME_INTERVAL ms")
                }
            }

            override fun possibleResultPoints(resultPoints: MutableList<ResultPoint>?) {
            }
        })
        mBeepManager = BeepManager(this)
    }

    private fun barcodeAddItem(barcode: String) {
        if (barcode.isNotEmpty()) {
//            TODO("need to use real bar code")
            //val testBarcode = "1234567890123"
            val productIndex = ForSaleProduct.getProductIndexByBarcode(barcode)
            //mCPosSharedViewModel.addOrderProduct(ForSaleProduct.getProductByBarcode(barcode))
            if(productIndex < 0 ) {
                var listString = ""
                val alertDialog = AlertDialog.Builder(this@PosActivity)
                    .setTitle("Unknown item, please try again.")
                    .setMessage(listString)
                    .setPositiveButton(android.R.string.ok) { _, _ ->
                    }
                    .show()
            }
            else {
                mCPosSharedViewModel.addOrderProduct(ForSaleProduct.getProductByIndex(productIndex))
                mCPosSharedViewModel.setBeepSound()
            }

            val orderFragment = supportFragmentManager.findFragmentById(R.id.order_list_container) as OrderFragment
            orderFragment.updateOrderList()

            mBackgroundUpdateUiHandler!!.sendMessage(mBackgroundUpdateUiHandler!!.obtainMessage(
                UPDATE_UI_DATE_MESSAGE))
        }
    }

    private fun cameraDetectionAddItem() {
//            TODO("need to use real bar code")
        mCPosSharedViewModel.addTempToOrderProduct()
        mCPosSharedViewModel.setBeepSound()

        val orderFragment = supportFragmentManager.findFragmentById(R.id.order_list_container) as OrderFragment
        orderFragment.updateOrderList()

        mBackgroundUpdateUiHandler!!.sendMessage(mBackgroundUpdateUiHandler!!.obtainMessage(
            UPDATE_UI_DATE_MESSAGE))

        val alertDialog = AlertDialog.Builder(this@PosActivity)
            .setTitle("Notices:")
            .setMessage("To prevent double counting, please remove all items on the detection area before do next run.")
            .setPositiveButton(android.R.string.ok) { _, _ ->
            }
            .show()
    }

    private fun showCheckoutList() {
        var listString = ""
        mCPosSharedViewModel.getOrderItemList()!!.forEach {
            listString += "${it.name} → x${it.count}\r\n"
        }

        val order = listString.split("\r\n") as MutableList<String>
        if (listString.isNotEmpty()) {
            order.removeIf { it.isEmpty() }
        }

        val dialog = MaterialDialog(this)
        dialog.positiveButton {
//            TODO("calculate total price at some where not here !")
            var totalPrice = 0.0
            mCPosSharedViewModel.getOrderItemList()!!.forEach {
                totalPrice += (it.count * it.specialOffer)
            }
            val intent = Intent(this, CheckoutActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            intent.putExtra("amount", totalPrice)
            startActivity(intent)
        }
        dialog.negativeButton(text = "Cancel")
        dialog.show {
            title(null, "統計結果")
            listItems(items = order)
        }

//        val show = AlertDialog.Builder(this)
//            .setTitle("統計結果")
//            .setMessage(listString)
//            .setPositiveButton("OK") { _, _ ->
//                val intent = Intent(this, CheckoutActivity::class.java)
//                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
//                startActivity(intent)
//            }
//            .setNegativeButton("Cancel", null)
//            .show()
    }

    override fun onResume() {
        super.onResume()
        mBarcodeView.resume()

        val display = windowManager.defaultDisplay
        val point = Point()
        display.getSize(point)
        Log.i("PA", "display width: ${point.x} px, height: ${point.y} px")

        val metrics = resources.displayMetrics
        Log.i("PA", "display width: ${point.x / metrics.density} dp, height: ${point.y / metrics.density} dp")
    }

    override fun onPause() {
        super.onPause()
        mBarcodeView.pause()
    }

    private fun activateRk3399ProFunction() {
        mCameraDetectViewModel = ViewModelProviders.of(this, ViewModelProvider.AndroidViewModelFactory(application)).get(CameraDetectViewModel::class.java)

        mBackgroundUpdateUiHandler!!.sendMessage(mBackgroundUpdateUiHandler!!.obtainMessage(
            BACKGROUND_THREAD_INITIALIZE_RUNNING_MESSAGE))

        // wait for something running
        // activity must be created or it will crash
        Thread.sleep(200)
        Rk3399ProInitAsyncTask(this).execute()
    }
    inner class Rk3399ProInitAsyncTask internal constructor(context: PosActivity)  : AsyncTask<Void, Int, Void>() {
        private val weakActivity: WeakReference<PosActivity> = WeakReference(context)
        private val mContext = context
        override fun onPreExecute() {
            super.onPreExecute()
            val activity = weakActivity.get()
            if (activity == null || activity.isFinishing) return
        }

        override fun doInBackground(vararg param: Void?): Void? {
            mCameraDetectViewModel.rknnModelInit()
            if (mProgressDialog!!.isShowing) {
                mProgressDialog?.dismiss()
            }
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            val activity = weakActivity.get()
            if (activity == null || activity.isFinishing) return
        }
    }

    inner class CameraDetectRunAsyncTask(rootView: View, activity: PosActivity) :
        AsyncTask<Void, Int, Void>() {

        private val mRootView: View? = rootView
        private  val weakActivity: WeakReference<PosActivity> = WeakReference(activity)

        override fun onPreExecute() {
            super.onPreExecute()
            val activity = weakActivity.get()
            if (activity == null || activity.isFinishing) return
        }

        override fun doInBackground(vararg params: Void?): Void? {
            if (mCameraDetectViewModel.isRknnInitDone()) {
                //while (!mStopCameraDetect) {
                    run()
                //}
            } else {
                Snackbar.make(mRootView!!, "wait for RKNN init done\ntry again later\n", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
            }
            Snackbar.make(mRootView!!, "Camera Detecttion Thread STOP !!", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
            return null
        }

        private fun run () {
//            TODO("implement inference function here")
            Log.i("PA", "Camera detection started..")
            while(mCameraFragment.getBitmap() == null) {
                Thread.sleep(3000)
                Log.i("PA", "Bitmap not ready, wait for 3s...")
            }
            val detectResult = mCameraDetectViewModel.cameraDetectRun(mCameraFragment)
            val nnResultMappingList = resources.getStringArray(R.array.nnResultMapping).toMutableList()
            var mapId: Int

            for(index in 0 until detectResult!!.objectCount) {
                mapId = nnResultMappingList[detectResult!!.outputObjects[index].iObject.tagId!!].toInt()
                detectResult!!.outputObjects[index].iObject.tagId = mapId
            }

            mCPosSharedViewModel.setRObjectList(detectResult!!.outputObjects)

            mBackgroundUpdateUiHandler!!.sendMessage(mBackgroundUpdateUiHandler!!.obtainMessage(
                CAMERA_DETECTED_PRODUCT_MESSAGE))
        }
/*
        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)

            mBackgroundUpdateUiHandler!!.sendMessage(mBackgroundUpdateUiHandler!!.obtainMessage(
                CAMERA_DETECT_DONE_MESSAGE))

            val activity = weakActivity.get()
            if (activity == null || activity.isFinishing) return
        }
*/
    }

    private fun advertiserStart() {
        mCPosSharedViewModel.setAdvertisePictureList()

        AdvertiserAsyncTask(this@PosActivity)
            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
    }

    private fun advertisementChange() {
        if(advertisePlayIndex == mCPosSharedViewModel.advertisePictureList.value!!.size) {
            advertisePlayIndex = 0
        }

        Glide.with(this)
            .load(mCPosSharedViewModel.getAdvertisePicture(advertisePlayIndex))
            .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE))
            .into(DrawableImageViewTarget(advertisement_image))

//        advertisement_image.setImageResource(mCPosSharedViewModel.getAdvertisePicture(advertisePlayIndex))
        advertisePlayIndex++
    }

    inner class AdvertiserAsyncTask(activity: PosActivity) :
        AsyncTask<Void, Int, Void>() {
        private  val weakActivity: WeakReference<PosActivity> = WeakReference(activity)

        override fun onPreExecute() {
            super.onPreExecute()
            val activity = weakActivity.get()
            if (activity == null || activity.isFinishing) return
        }

        override fun doInBackground(vararg param: Void?): Void? {
            while (true) {
                run()
            }
            return null
        }

        private fun run() {
            mBackgroundUpdateUiHandler!!.sendMessage(mBackgroundUpdateUiHandler!!.obtainMessage(
                ADVERTISEMENT_CHANGE_MESSAGE))
            Thread.sleep(5500)
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            val activity = weakActivity.get()
            if (activity == null || activity.isFinishing) return
        }

    }

    private fun buttonOnClickFunctionInit() {
        button10.setOnClickListener(button10OnClickListener)
        button11.setOnClickListener(button11OnClickListener)
        button12.setOnClickListener(button12OnClickListener)
        button13.setOnClickListener(button13OnClickListener)
        button16.setOnClickListener(button16OnClickListener)

        button31.setOnClickListener(button31OnClickListener)

        camera_detect_run.setOnClickListener(buttonCameraDetectRunOnClickListener)
    }

    private val button10OnClickListener = View.OnClickListener {v ->
        displayProductButtonFragment()
    }

    private val button11OnClickListener = View.OnClickListener {v ->
        displayCameraDetectionFragment()
    }

    private val button16OnClickListener = View.OnClickListener {v ->
        clearOrderProduct()
    }

    private val button12OnClickListener = View.OnClickListener {v ->

    }

    private val button13OnClickListener = View.OnClickListener {v ->

    }

    private val button31OnClickListener = View.OnClickListener { v ->
        showCheckoutList()
    }

    private val buttonCameraDetectRunOnClickListener = View.OnClickListener { v ->
        camera_detect_run.isEnabled = false
        CameraDetectRunAsyncTask(window.decorView.findViewById(android.R.id.content), this@PosActivity)
            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
    }

    private fun displayProductButtonFragment() {
        camera_detect_container.visibility = View.GONE
        product_list_container.visibility = View.VISIBLE
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction
            .show(mProductFragment)
            .hide(mCameraFragment)
            .commit()

        camera_detect_run.visibility = View.GONE
    }

    private fun displayCameraDetectionFragment() {
        product_list_container.visibility = View.GONE
        camera_detect_container.visibility = View.VISIBLE
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction
            .show(mCameraFragment)
            .hide(mProductFragment)
            .commit()

        camera_detect_run.visibility = View.VISIBLE
    }

    private fun clearOrderProduct() {
        val AlertDialog = AlertDialog.Builder(this@PosActivity)
            .setTitle("Clean oder list?")
            .setPositiveButton(android.R.string.ok) { _, _ ->
                mCPosSharedViewModel.clearOrderProduct()
                val orderFragment = supportFragmentManager.findFragmentById(R.id.order_list_container) as OrderFragment
                orderFragment.updateOrderList()
                mBackgroundUpdateUiHandler!!.sendMessage(mBackgroundUpdateUiHandler!!.obtainMessage(
                    UPDATE_UI_DATE_MESSAGE))
            }
            .setNegativeButton(android.R.string.cancel) { _, _ ->
            }
            .show()

    }
/*
    inner class startCameraDetectAsyncTask internal constructor(context: PosActivity)  : AsyncTask<Void, Int, Void>() {
        private val weakActivity: WeakReference<PosActivity> = WeakReference(context)
        private val mContext = context
        override fun onPreExecute() {
            super.onPreExecute()
            val activity = weakActivity.get()
            if (activity == null || activity.isFinishing) return
        }

        override fun doInBackground(vararg param: Void?): Void? {
            while (true) {
                if (mCameraFragment.isVisible && mCameraFragment.isAdded && mCameraFragment.isResumed) {
                    CameraDetectRunAsyncTask(window.decorView.findViewById(android.R.id.content), this@PosActivity)
                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
                    //mStopCameraDetect = false
                    break
                }
                else {
                    Thread.sleep(100)
                }
            }
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            val activity = weakActivity.get()
            if (activity == null || activity.isFinishing) return
        }
    }
*/
    private fun initLoggedInUser() {
//        TODO("set user infomation here")

        Thread.sleep(10)

        mBackgroundUpdateUiHandler!!.sendMessage(mBackgroundUpdateUiHandler!!.obtainMessage(
            SET_END_USER_INFO_MESSAGE))
    }


    private fun initCameraDetectRunButtonStyle() {
        val cutCornerTreatment = CutCornerTreatment(42.0f)
        val triangleEdgeTreatment = TriangleEdgeTreatment(42.0f, true)

        camera_detect_run.shapeAppearanceModel = camera_detect_run.shapeAppearanceModel
            .toBuilder()
            .setLeftEdge(triangleEdgeTreatment)
            .setTopRightCorner(cutCornerTreatment)
            .setBottomRightCorner(cutCornerTreatment)
            .build()
    }

    private fun initDisplayTextView() {
        initLoggedInUser()

        mBackgroundUpdateUiHandler!!.sendMessage(mBackgroundUpdateUiHandler!!.obtainMessage(
            UPDATE_UI_DATE_MESSAGE))
    }

    companion object {
        private const val PRODUCT_NAME_RK3399PRO = "rk3399pro"
        private const val BARCODE_TIME_INTERVAL = 1000
        private const val UPDATE_UI_DATE_MESSAGE = 0
        private const val BACKGROUND_THREAD_INITIALIZE_RUNNING_MESSAGE = 1
        private const val CAMERA_DETECTED_PRODUCT_MESSAGE = 2
        private const val SET_END_USER_INFO_MESSAGE = 3
        private const val CAMERA_DETECT_DONE_MESSAGE = 4
        private const val ADVERTISEMENT_CHANGE_MESSAGE = 5

        init {
            System.loadLibrary("opencv_java4")
        }
    }
}
