package com.yut3.cpos.ui.pos.dummy

import androidx.lifecycle.MutableLiveData
import com.yut3.cpos.R
import com.yut3.cpos.data.model.Barcode
import com.yut3.cpos.data.model.CategoryLevel
import com.yut3.cpos.data.model.Product
import java.security.MessageDigest
import java.util.*

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 *
 * TODO: Replace all uses of this class before publishing your app.
 */
object ForSaleProduct {

    /**
     * An array of sample (dummy) items.
     */
    val ITEMS: MutableList<Product> = ArrayList()

    /**
     * A map of sample (dummy) items, by ID.
     */
    val ITEM_MAP: MutableMap<String, Product> = HashMap()

    val productPictureList = MutableLiveData<MutableList<Int>>()

    private val COUNT = 105

    fun createMenuItem() {
        // Add some sample items.
//        for (level in 0 until 6) {
//            for (i in 0..4) {
//                addItem(createDummyTitleItem(level))
//            }
//        }

        addItem(Product(UUID.randomUUID(), "飲品", 0, CategoryLevel(1.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "零食", 0, CategoryLevel(2.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "泡麵", 0, CategoryLevel(3.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
/*
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(1.toString(), 1.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(1.toString(), 2.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(1.toString(), 3.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(2.toString(), 1.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(2.toString(), 2.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(2.toString(), 3.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(3.toString(), 1.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(3.toString(), 2.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))

        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(1.toString(), 1.toString(), 1.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(1.toString(), 1.toString(), 2.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))

        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(1.toString(), 2.toString(), 1.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(1.toString(), 2.toString(), 2.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))

        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(1.toString(), 3.toString(), 1.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(1.toString(), 3.toString(), 2.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))

        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(2.toString(), 1.toString(), 1.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(2.toString(), 1.toString(), 2.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))

        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(2.toString(), 2.toString(), 1.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(2.toString(), 2.toString(), 2.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))

        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(2.toString(), 3.toString(), 1.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(2.toString(), 3.toString(), 2.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))

        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(3.toString(), 1.toString(), 1.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(3.toString(), 1.toString(), 2.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))

        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(3.toString(), 2.toString(), 1.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(3.toString(), 2.toString(), 2.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
*/
    }

    fun createProductItem(id: Int, name: String, barcode: String, productClass: String) {
        addItem(createItem(id, name, barcode, productClass))
    }

    private fun createItem(id: Int, name: String, barcode: String, productClass: String): Product {
        val uuid = UUID.randomUUID()
        val categoryLevel0 = productClass
        val categoryLevel1 = id
        val categoryLevel2 = 0
        val categoryLevel3 = 0
        //val categoryLevel0 = (1..3).random()
        //val categoryLevel1 = (1..3).random()
        //val categoryLevel2 = (1..2).random()
        //val categoryLevel3 = (1..7).random()
        val categoryLevel4 = 0
        val categoryLevel5 = 0
        val categoryLevel6 = 0
        val categoryLevel = CategoryLevel(
            categoryLevel0.toString(),
            categoryLevel1.toString(),
            categoryLevel2.toString(),
            categoryLevel3.toString(),
            categoryLevel4.toString(),
            categoryLevel5.toString(),
            categoryLevel6.toString())
        val barcode = Barcode(barcode, "EAN13")

        //return Product(uuid, MessageDigest.getInstance("MD5").digest(uuid.toString().toByteArray()).toString(), position, categoryLevel, barcode)
        return Product(uuid, name, id, categoryLevel, barcode, (100..200).random().toDouble(), (50..100).random().toDouble())
    }

    private fun addItem(item: Product) {
        ITEMS.add(item)
        ITEM_MAP.put(item.id.toString(), item)
    }

    private fun createTitleItem(level: Int): Product {
        val uuid = UUID.randomUUID()
        val categoryLevel0 = if (level >= 0) (0..10).random() else 0
        val categoryLevel1 = if (level >= 1)  (0..9).random() else 0
        val categoryLevel2 = if (level >= 2) (0..8).random() else 0
        val categoryLevel3 = if (level >= 3) (0..7).random() else 0
        val categoryLevel4 = if (level >= 4) (0..6).random() else 0
        val categoryLevel5 = if (level >= 5) (0..5).random() else 0
        val categoryLevel6 = if (level >= 6) (0..4).random() else 0
        val categoryLevel = CategoryLevel(
            categoryLevel0.toString(),
            categoryLevel1.toString(),
            categoryLevel2.toString(),
            categoryLevel3.toString(),
            categoryLevel4.toString(),
            categoryLevel5.toString(),
            categoryLevel6.toString())
        return Product(uuid,
            MessageDigest.getInstance("MD5").digest(uuid.toString().toByteArray()).toString(),
            0,
            categoryLevel,
            Barcode("", "")
            , (100..200).random().toDouble(), (50..100).random().toDouble())
    }

    fun createDisplayList(displayCategoryLevel: CategoryLevel): MutableList<Product> {
        val productList: MutableList<Product> = arrayListOf()
        val displayLastLevel = displayCategoryLevel.lastLevel() + 1

        if (displayLastLevel > 0) {
            val previous = Product(UUID.randomUUID(), "", -1, displayCategoryLevel, Barcode("", ""), (100..200).random().toDouble(), (50..100).random().toDouble())
            productList.add(previous)
        }

        var tmp = ForSaleProduct.ITEMS.filter { it.categoryLevel.lastLevel() == displayLastLevel }

        if (displayLastLevel == 6) {
//            tmp = tmp.filter { it.categoryLevel.level6 == displayCategoryLevel.level6 }
            tmp = tmp.filter { it.categoryLevel.level5 == displayCategoryLevel.level5 }
            tmp = tmp.filter { it.categoryLevel.level4 == displayCategoryLevel.level4 }
            tmp = tmp.filter { it.categoryLevel.level3 == displayCategoryLevel.level3 }
            tmp = tmp.filter { it.categoryLevel.level2 == displayCategoryLevel.level2 }
            tmp = tmp.filter { it.categoryLevel.level1 == displayCategoryLevel.level1 }
            tmp = tmp.filter { it.categoryLevel.level0 == displayCategoryLevel.level0 }
        }

        if (displayLastLevel == 5) {
//            tmp = tmp.filter { it.categoryLevel.level6 == displayCategoryLevel.level6 }
//            tmp = tmp.filter { it.categoryLevel.level5 == displayCategoryLevel.level5 }
            tmp = tmp.filter { it.categoryLevel.level4 == displayCategoryLevel.level4 }
            tmp = tmp.filter { it.categoryLevel.level3 == displayCategoryLevel.level3 }
            tmp = tmp.filter { it.categoryLevel.level2 == displayCategoryLevel.level2 }
            tmp = tmp.filter { it.categoryLevel.level1 == displayCategoryLevel.level1 }
            tmp = tmp.filter { it.categoryLevel.level0 == displayCategoryLevel.level0 }
        }

        if (displayLastLevel == 4) {
            tmp = tmp.filter { it.categoryLevel.level6 == "" || it.categoryLevel.level6 == "0" }
//            tmp = tmp.filter { it.categoryLevel.level5 == displayCategoryLevel.level5 }
//            tmp = tmp.filter { it.categoryLevel.level4 == displayCategoryLevel.level4 }
            tmp = tmp.filter { it.categoryLevel.level3 == displayCategoryLevel.level3 }
            tmp = tmp.filter { it.categoryLevel.level2 == displayCategoryLevel.level2 }
            tmp = tmp.filter { it.categoryLevel.level1 == displayCategoryLevel.level1 }
            tmp = tmp.filter { it.categoryLevel.level0 == displayCategoryLevel.level0 }
        }

        if (displayLastLevel == 3) {
            tmp = tmp.filter { it.categoryLevel.level6 == "" || it.categoryLevel.level6 == "0" }
            tmp = tmp.filter { it.categoryLevel.level5 == "" || it.categoryLevel.level5 == "0" }
//            tmp = tmp.filter { it.categoryLevel.level4 == displayCategoryLevel.level4 }
//            tmp = tmp.filter { it.categoryLevel.level3 == displayCategoryLevel.level3 }
            tmp = tmp.filter { it.categoryLevel.level2 == displayCategoryLevel.level2 }
            tmp = tmp.filter { it.categoryLevel.level1 == displayCategoryLevel.level1 }
            tmp = tmp.filter { it.categoryLevel.level0 == displayCategoryLevel.level0 }
        }

        if (displayLastLevel == 2) {
            tmp = tmp.filter { it.categoryLevel.level6 == "" || it.categoryLevel.level6 == "0" }
            tmp = tmp.filter { it.categoryLevel.level5 == "" || it.categoryLevel.level5 == "0" }
            tmp = tmp.filter { it.categoryLevel.level4 == "" || it.categoryLevel.level4 == "0" }
//            tmp = tmp.filter { it.categoryLevel.level3 == displayCategoryLevel.level3 }
//            tmp = tmp.filter { it.categoryLevel.level2 == displayCategoryLevel.level2 }
            tmp = tmp.filter { it.categoryLevel.level1 == displayCategoryLevel.level1 }
            tmp = tmp.filter { it.categoryLevel.level0 == displayCategoryLevel.level0 }
        }

        if (displayLastLevel == 1) {
            tmp = tmp.filter { it.categoryLevel.level6 == "" || it.categoryLevel.level6 == "0" }
            tmp = tmp.filter { it.categoryLevel.level5 == "" || it.categoryLevel.level5 == "0" }
            tmp = tmp.filter { it.categoryLevel.level4 == "" || it.categoryLevel.level4 == "0" }
            tmp = tmp.filter { it.categoryLevel.level3 == "" || it.categoryLevel.level3 == "0" }
//            tmp = tmp.filter { it.categoryLevel.level2 == displayCategoryLevel.level2 }
//            tmp = tmp.filter { it.categoryLevel.level1 == displayCategoryLevel.level1 }
            tmp = tmp.filter { it.categoryLevel.level0 == displayCategoryLevel.level0 }
        }

        if (displayLastLevel == 0) {
            tmp = tmp.filter { it.categoryLevel.level6 == "" || it.categoryLevel.level6 == "0" }
            tmp = tmp.filter { it.categoryLevel.level5 == "" || it.categoryLevel.level5 == "0" }
            tmp = tmp.filter { it.categoryLevel.level4 == "" || it.categoryLevel.level4 == "0" }
            tmp = tmp.filter { it.categoryLevel.level3 == "" || it.categoryLevel.level3 == "0" }
            tmp = tmp.filter { it.categoryLevel.level2 == "" || it.categoryLevel.level2 == "0" }
//            tmp = tmp.filter { it.categoryLevel.level1 == displayCategoryLevel.level1 }
//            tmp = tmp.filter { it.categoryLevel.level0 == displayCategoryLevel.level0 }
        }

        tmp.sortedBy { it.id }
        productList.addAll(tmp)

        return productList
    }

    fun getProductByBarcode(barcode: String) : Product {
        val index = ForSaleProduct.ITEMS.indexOfFirst { it.barcode.code == barcode }
        if(index < 0) {
            return ForSaleProduct.ITEMS[1]
        }
        return ForSaleProduct.ITEMS[index]
    }

    fun getProductIndexByBarcode(barcode: String) : Int {
        val index = ForSaleProduct.ITEMS.indexOfFirst { it.barcode.code == barcode }
        return index
    }

    fun getProductByIndex(index: Int) : Product {
        return ForSaleProduct.ITEMS[index]
    }

    fun getProductById(id: Int) : Product {
        return ForSaleProduct.ITEMS!!.first {it.id == id}
    }

    private fun makeDetails(position: Int): String {
        val builder = StringBuilder()
        builder.append("Details about Item: ").append(position)
        for (i in 0..position - 1) {
            builder.append("\nMore details information here.")
        }
        return builder.toString()
    }

    fun setProductPictureList() {
        productPictureList.value = arrayListOf()
        for(i in 0 until 106) {
            productPictureList.value!!.add(R.mipmap.ic_launcher)
        }
        productPictureList.value!![1] = R.raw.b1
        productPictureList.value!![100] = R.raw.b100
        productPictureList.value!![101] = R.raw.b101
        productPictureList.value!![104] = R.raw.b104
        productPictureList.value!![11] = R.raw.b11
        productPictureList.value!![20] = R.raw.b20
        productPictureList.value!![21] = R.raw.b21
        productPictureList.value!![22] = R.raw.b22
        productPictureList.value!![24] = R.raw.b24
        productPictureList.value!![27] = R.raw.b27
        productPictureList.value!![28] = R.raw.b28
        productPictureList.value!![56] = R.raw.b56
        productPictureList.value!![64] = R.raw.b64
        productPictureList.value!![75] = R.raw.b75
        productPictureList.value!![79] = R.raw.b79
        productPictureList.value!![83] = R.raw.b83
        productPictureList.value!![69] = R.raw.b69
    }

    fun getProductPicture(index: Int) :Int {
        return productPictureList.value!![index]
    }


}
