package com.yut3.cpos.ui.login

import android.app.Activity
import android.app.PendingIntent
import android.app.ProgressDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.res.Configuration
import android.graphics.drawable.AnimationDrawable
import android.hardware.usb.UsbAccessory
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.Handler
import android.os.Message
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.os.ConfigurationCompat
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import com.acs.smartcard.Reader
import com.acs.smartcard.ReaderException
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.yut3.cpos.App
import com.yut3.cpos.CPosSharedViewModel
import com.yut3.cpos.PosActivity

import com.yut3.cpos.R
import com.yut3.cpos.data.model.LocaleManager
import com.yut3.cpos.data.model.LoggedInUser
import com.yut3.cpos.data.model.RuntimeOperatingMode
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.pos_activity.*
import java.util.*

class LoginActivity : AppCompatActivity() {

    private lateinit var loginViewModel: LoginViewModel
    private lateinit var mCPosSharedViewModel: CPosSharedViewModel

    private lateinit var mUsbManager: UsbManager
    private lateinit var mReader: Reader
    private lateinit var mUsbNfcReaderDevice: UsbDevice
    private lateinit var mAnimationDrawable: AnimationDrawable

    private var mBackgroundUpdateUiHandler: Handler? = null

    private lateinit var mLocale: Locale
    private var currentLanguage = "en"
    private var currentLang: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)

        try {
            val currentLocale = ConfigurationCompat.getLocales(resources.configuration)[0]
            currentLanguage = currentLocale.language
//            currentLanguage = intent.getStringExtra(currentLang)
            Log.i("LA", "Language has been change to ${currentLanguage}")
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        }


        Log.i("test", "DIP scale: ${resources.displayMetrics.density}")
        Log.i("test", "DPI: ${resources.displayMetrics.densityDpi}")

        val username = findViewById<EditText>(R.id.username)
        val password = findViewById<EditText>(R.id.password)
        val login = findViewById<Button>(R.id.login)
        val loading = findViewById<ProgressBar>(R.id.loading)

        mAnimationDrawable = resources.getDrawable(R.drawable.animation, null) as AnimationDrawable
        iv_nfc_signal.background = mAnimationDrawable
        iv_nfc_signal.visibility = View.INVISIBLE

        loginViewModel = ViewModelProviders.of(this, LoginViewModelFactory())
            .get(LoginViewModel::class.java)

        mCPosSharedViewModel = ViewModelProviders.of(this, ViewModelProvider.AndroidViewModelFactory(application)).get(
            CPosSharedViewModel::class.java)


        loginViewModel.loginFormState.observe(this@LoginActivity, Observer {
            val loginState = it ?: return@Observer

            // disable login button unless both username / password is valid
            login.isEnabled = loginState.isDataValid

            if (loginState.usernameError != null) {
                username.error = getString(loginState.usernameError)
            }
            if (loginState.passwordError != null) {
                password.error = getString(loginState.passwordError)
            }
        })

        loginViewModel.loginResult.observe(this@LoginActivity, Observer {
            val loginResult = it ?: return@Observer

            loading.visibility = View.GONE
            if (loginResult.error != null) {
                showLoginFailed(loginResult.error)
            }
            if (loginResult.success != null) {
                updateUiWithUser(loginResult.success)

//                TODO("set runtime operating mode from user data")
                mCPosSharedViewModel.setRuntimeOperatingMode(RuntimeOperatingMode.TYPE_END_USER)

                when (mCPosSharedViewModel.getRuntimeOperatingMode()) {
                    RuntimeOperatingMode.TYPE_END_USER -> {
                        // Logged in success
                        val intent = Intent(this, PosActivity::class.java)
                        intent.putExtra("logged_in_end_user_uid", loginResult.success.userId)
                        intent.putExtra("logged_in_end_user_name", loginResult.success.displayName)
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                    }
                    RuntimeOperatingMode.TYPE_ENGINERRING  -> {}
                    RuntimeOperatingMode.TYPE_DEVELOPER -> {}
                }

            }
            setResult(Activity.RESULT_OK)

            //Complete and destroy login activity once successful
            finish()
        })

        username.afterTextChanged {
            loginViewModel.loginDataChanged(
                username.text.toString(),
                password.text.toString()
            )
        }

        password.apply {
            afterTextChanged {
                loginViewModel.loginDataChanged(
                    username.text.toString(),
                    password.text.toString()
                )
            }

            setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_DONE ->
                        loginViewModel.login(
                            username.text.toString(),
                            password.text.toString()
                        )
                }
                false
            }

            login.setOnClickListener {
                loading.visibility = View.VISIBLE
                loginViewModel.login(username.text.toString(), password.text.toString())
            }
        }
        createBackgroundUpdateUiHandler()
        acsNfcReaderInit()

        createLanguageList()

    }

    private fun createLanguageList() {
        val languageList = ArrayList<String>()
        val languageListTitle = when (ConfigurationCompat.getLocales(resources.configuration)[0].language) {
            "ja" -> {
                "言語設定"
            }
            "en" -> {
                "Select Language"
            }
            "zh" -> {
                "語言設定"
            }
            else -> {
                "Select Language"
            }
        }

        languageList.add(languageListTitle)
        languageList.add("English")
        languageList.add("日本語")
        languageList.add("中文(繁體)")

        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, languageList)

        languageSpinner.adapter = adapter
        languageSpinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                when(position) {
                    1 -> {
                        // EN
                        setNewLocale(LocaleManager.LANGUAGE_ENGLISH)
//                        setLocale("en")
                    }
                    2 -> {
                        // JP
                        setNewLocale(LocaleManager.LANGUAGE_JAPANESE)
//                        setLocale("ja")
                    }
                    3 -> {
                        // ZH-TW
                        setNewLocale(LocaleManager.LANGUAGE_CHINESE)
//                        setLocale("zh")
                    }
                    else -> {

                    }
                }
            }
        }
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(base!!))
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        LocaleManager.setLocale(this)
    }

    private fun setNewLocale(language: String) {

        if (currentLanguage != language) {
            LocaleManager.setNewLocale(this@LoginActivity, language)
            intent.putExtra(currentLang, language)
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
        } else {
            Log.i("LA", "Language already selected !")
        }
    }

    private fun updateUiWithUser(model: LoggedInUserView) {
        val welcome = getString(R.string.welcome)
        val displayName = model.displayName
        // TODO : initiate successful logged in experience
        Toasty.info(applicationContext,
            "$welcome $displayName",
            Toast.LENGTH_SHORT,
            true
        ).show();
//        Toast.makeText(
//            applicationContext,
//            "$welcome $displayName",
//            Toast.LENGTH_LONG
//        ).show()
    }

    private fun showLoginFailed(@StringRes errorString: Int) {
        Toast.makeText(applicationContext, errorString, Toast.LENGTH_SHORT).show()
    }

    private fun nfcLogin() {
        loginViewModel.login("user", "password")
        try {
            unregisterReceiver(usbReceiver)
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
            Log.e("LA", "no need to unregister receiver")
        }

    }

    private fun acsNfcReaderInit() {
        val filter = IntentFilter(ACTION_USB_PERMISSION)
        registerReceiver(usbReceiver, filter)

        mUsbManager = getSystemService(Context.USB_SERVICE) as UsbManager
        mReader = Reader(mUsbManager)

        mReader.setOnStateChangeListener(Reader.OnStateChangeListener { slotNum, prevState, currState ->
            try {
                mBackgroundUpdateUiHandler!!.sendMessage(mBackgroundUpdateUiHandler!!.obtainMessage(
                    NFC_CARD_READER_READING_START_MESSAGE))
                val atr = mReader.power(slotNum, Reader.CARD_WARM_RESET)
                val cardName: ByteArray = byteArrayOf(atr[13], atr[14])
                when (cardName.toHexString()) {
                    "00 01 " -> {
                        Log.i("LA", "Card Name: Mifare 1K")
                    }
                    "00 02 " -> {
                        Log.i("LA", "Card Name: Mifare 4K")
                    }
                    "00 03 " -> {
                        Log.i("LA", "Card Name: Mifare Ultralight")
                    }
                    "00 26 " -> {
                        Log.i("LA", "Card Name: Mifare Mini")
                    }
                    "f0 04 " -> {
                        Log.i("LA", "Card Name: Topaz & Jewel")
                    }
                    "f0 11 " -> {
                        Log.i("LA", "Card Name: FeliCa 212K")
                    }
                    "f0 12 " -> {
                        Log.i("LA", "Card Name: FeliCa 424K")
                    }
                    else -> {
                        Log.i("LA", "Card Name: Undefined")
                    }
                }
                Log.i("LA", "ATR: ${atr.toHexString()}")
            } catch (e: ReaderException) {
                e.printStackTrace()
            }
            try {
                mReader.setProtocol(slotNum, Reader.PROTOCOL_T0 or Reader.PROTOCOL_T1)
            } catch (e: ReaderException) {
                e.printStackTrace()
            }
            val cmd : ByteArray = byteArrayOf(0xFF.toByte(), 0xCA.toByte(), 0x00.toByte(), 0x00.toByte(), 0x00.toByte())
            var response = ByteArray(300)
            var responseLength: Int = 0
            try {
                responseLength = mReader.transmit(slotNum, cmd, cmd.size, response, response.size)
                Log.i("LA", "ACS response size: ${response.size}, msg: ${response.toHexString()}")
                mBackgroundUpdateUiHandler!!.sendMessage(mBackgroundUpdateUiHandler!!.obtainMessage(
                    NFC_LOGIN_MESSAGE))
            } catch (e: ReaderException) {
                e.printStackTrace()
            }

            mBackgroundUpdateUiHandler!!.sendMessage(mBackgroundUpdateUiHandler!!.obtainMessage(
                NFC_CARD_READER_READING_STOP_MESSAGE))
        })

        val deviceList = mUsbManager.deviceList
        var deviceName: String = ""
        deviceList.values.forEach {device ->
            if (device.productName == "ACR122U PICC Interface") {
                deviceName = device.deviceName
            }
        }
        mUsbNfcReaderDevice = deviceList.get(deviceName)!!
        // open device
        try {
            mReader.open(mUsbNfcReaderDevice)
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()

            val permissionIntent = PendingIntent.getBroadcast(this, 0, Intent(ACTION_USB_PERMISSION), 0)
            mUsbManager.requestPermission(mUsbNfcReaderDevice, permissionIntent)
        }
    }

    fun ByteArray.toHexString() = joinToString("") { "%02x ".format(it) }

    private val usbReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (ACTION_USB_PERMISSION == intent!!.action) {
                val accessory: UsbAccessory? = intent.getParcelableExtra(UsbManager.EXTRA_ACCESSORY)
                if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                    try {
                        mReader.open(mUsbNfcReaderDevice)
                    } catch (e: IllegalArgumentException) {
                        e.printStackTrace()
                    }
                } else {
                    Toast.makeText(context, "Permission denied for usb nfc device", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun createBackgroundUpdateUiHandler() {
        @JvmStatic
        mBackgroundUpdateUiHandler = object : Handler() {
            override fun handleMessage(msg: Message?) {
                super.handleMessage(msg)
                when (msg?.what) {
                    NFC_LOGIN_MESSAGE -> {
                        nfcLogin()
                    }
                    NFC_CARD_READER_READING_START_MESSAGE -> {
                        loading.visibility = View.VISIBLE
                        iv_nfc_signal.visibility = View.VISIBLE
                        mAnimationDrawable.start()
                    }
                    NFC_CARD_READER_READING_STOP_MESSAGE -> {
                        mAnimationDrawable.stop()
                        iv_nfc_signal.visibility = View.INVISIBLE
                    }
                }
            }
        }
    }

    companion object {
        val ACTION_USB_PERMISSION = "com.yut3.cpos.USB_PERMISSION"
        private const val NFC_LOGIN_MESSAGE = 0
        private const val NFC_CARD_READER_READING_START_MESSAGE = 1
        private const val NFC_CARD_READER_READING_STOP_MESSAGE = 2
    }
}

/**
 * Extension function to simplify setting an afterTextChanged action to EditText components.
 */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}
