package com.yut3.cpos.ui.pos.dummy

import com.yut3.cpos.data.model.Barcode
import com.yut3.cpos.data.model.CategoryLevel
import com.yut3.cpos.data.model.Product
import java.security.MessageDigest
import java.util.*

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 *
 * TODO: Replace all uses of this class before publishing your app.
 */
object DummyProduct {

    /**
     * An array of sample (dummy) items.
     */
    val ITEMS: MutableList<Product> = ArrayList()

    /**
     * A map of sample (dummy) items, by ID.
     */
    val ITEM_MAP: MutableMap<String, Product> = HashMap()

    private val COUNT = 300

    init {
        // Add some sample items.
//        for (level in 0 until 6) {
//            for (i in 0..4) {
//                addItem(createDummyTitleItem(level))
//            }
//        }

        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(1.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(2.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(3.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))

        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(1.toString(), 1.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(1.toString(), 2.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(1.toString(), 3.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(2.toString(), 1.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(2.toString(), 2.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(2.toString(), 3.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(3.toString(), 1.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(3.toString(), 2.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))

        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(1.toString(), 1.toString(), 1.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(1.toString(), 1.toString(), 2.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))

        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(1.toString(), 2.toString(), 1.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(1.toString(), 2.toString(), 2.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))

        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(1.toString(), 3.toString(), 1.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(1.toString(), 3.toString(), 2.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))

        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(2.toString(), 1.toString(), 1.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(2.toString(), 1.toString(), 2.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))

        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(2.toString(), 2.toString(), 1.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(2.toString(), 2.toString(), 2.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))

        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(2.toString(), 3.toString(), 1.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(2.toString(), 3.toString(), 2.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))

        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(3.toString(), 1.toString(), 1.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(3.toString(), 1.toString(), 2.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))

        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(3.toString(), 2.toString(), 1.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))
        addItem(Product(UUID.randomUUID(), "", 0, CategoryLevel(3.toString(), 2.toString(), 2.toString(), 0.toString(), 0.toString(), 0.toString(), 0.toString()), Barcode("", ""), 0.toDouble(), 0.toDouble()))

        for (i in 1..COUNT) {
            addItem(createDummyItem(i))
        }
    }

    private fun addItem(item: Product) {
        ITEMS.add(item)
        ITEM_MAP.put(item.id.toString(), item)
    }

    private fun createDummyItem(position: Int): Product {
        val uuid = UUID.randomUUID()
        val categoryLevel0 = (1..3).random()
        val categoryLevel1 = (1..3).random()
        val categoryLevel2 = (1..2).random()
        val categoryLevel3 = (1..7).random()
        val categoryLevel4 = 0
        val categoryLevel5 = 0
        val categoryLevel6 = 0
        val categoryLevel = CategoryLevel(
            categoryLevel0.toString(),
            categoryLevel1.toString(),
            categoryLevel2.toString(),
            categoryLevel3.toString(),
            categoryLevel4.toString(),
            categoryLevel5.toString(),
            categoryLevel6.toString())
        val barcode = Barcode("1234567890123", "EAN13")

        return Product(uuid, MessageDigest.getInstance("MD5").digest(uuid.toString().toByteArray()).toString(), position, categoryLevel, barcode, 0.toDouble(), 0.toDouble())
    }

    private fun createDummyTitleItem(level: Int): Product {
        val uuid = UUID.randomUUID()
        val categoryLevel0 = if (level >= 0) (0..10).random() else 0
        val categoryLevel1 = if (level >= 1)  (0..9).random() else 0
        val categoryLevel2 = if (level >= 2) (0..8).random() else 0
        val categoryLevel3 = if (level >= 3) (0..7).random() else 0
        val categoryLevel4 = if (level >= 4) (0..6).random() else 0
        val categoryLevel5 = if (level >= 5) (0..5).random() else 0
        val categoryLevel6 = if (level >= 6) (0..4).random() else 0
        val categoryLevel = CategoryLevel(
            categoryLevel0.toString(),
            categoryLevel1.toString(),
            categoryLevel2.toString(),
            categoryLevel3.toString(),
            categoryLevel4.toString(),
            categoryLevel5.toString(),
            categoryLevel6.toString())
        return Product(uuid,
            MessageDigest.getInstance("MD5").digest(uuid.toString().toByteArray()).toString(),
            0,
            categoryLevel,
            Barcode("", ""), 0.toDouble(), 0.toDouble())
    }

    fun createDisplayList(displayCategoryLevel: CategoryLevel): MutableList<Product> {
        val productList: MutableList<Product> = arrayListOf()
        val displayLastLevel = displayCategoryLevel.lastLevel() + 1

        if (displayLastLevel > 0) {
            val previous = Product(UUID.randomUUID(), "", -1, displayCategoryLevel, Barcode("", ""), 0.toDouble(), 0.toDouble())
            productList.add(previous)
        }

        var tmp = DummyProduct.ITEMS.filter { it.categoryLevel.lastLevel() == displayLastLevel }

        if (displayLastLevel == 6) {
//            tmp = tmp.filter { it.categoryLevel.level6 == displayCategoryLevel.level6 }
            tmp = tmp.filter { it.categoryLevel.level5 == displayCategoryLevel.level5 }
            tmp = tmp.filter { it.categoryLevel.level4 == displayCategoryLevel.level4 }
            tmp = tmp.filter { it.categoryLevel.level3 == displayCategoryLevel.level3 }
            tmp = tmp.filter { it.categoryLevel.level2 == displayCategoryLevel.level2 }
            tmp = tmp.filter { it.categoryLevel.level1 == displayCategoryLevel.level1 }
            tmp = tmp.filter { it.categoryLevel.level0 == displayCategoryLevel.level0 }
        }

        if (displayLastLevel == 5) {
//            tmp = tmp.filter { it.categoryLevel.level6 == displayCategoryLevel.level6 }
//            tmp = tmp.filter { it.categoryLevel.level5 == displayCategoryLevel.level5 }
            tmp = tmp.filter { it.categoryLevel.level4 == displayCategoryLevel.level4 }
            tmp = tmp.filter { it.categoryLevel.level3 == displayCategoryLevel.level3 }
            tmp = tmp.filter { it.categoryLevel.level2 == displayCategoryLevel.level2 }
            tmp = tmp.filter { it.categoryLevel.level1 == displayCategoryLevel.level1 }
            tmp = tmp.filter { it.categoryLevel.level0 == displayCategoryLevel.level0 }
        }

        if (displayLastLevel == 4) {
            tmp = tmp.filter { it.categoryLevel.level6 == "" || it.categoryLevel.level6 == "0" }
//            tmp = tmp.filter { it.categoryLevel.level5 == displayCategoryLevel.level5 }
//            tmp = tmp.filter { it.categoryLevel.level4 == displayCategoryLevel.level4 }
            tmp = tmp.filter { it.categoryLevel.level3 == displayCategoryLevel.level3 }
            tmp = tmp.filter { it.categoryLevel.level2 == displayCategoryLevel.level2 }
            tmp = tmp.filter { it.categoryLevel.level1 == displayCategoryLevel.level1 }
            tmp = tmp.filter { it.categoryLevel.level0 == displayCategoryLevel.level0 }
        }

        if (displayLastLevel == 3) {
            tmp = tmp.filter { it.categoryLevel.level6 == "" || it.categoryLevel.level6 == "0" }
            tmp = tmp.filter { it.categoryLevel.level5 == "" || it.categoryLevel.level5 == "0" }
//            tmp = tmp.filter { it.categoryLevel.level4 == displayCategoryLevel.level4 }
//            tmp = tmp.filter { it.categoryLevel.level3 == displayCategoryLevel.level3 }
            tmp = tmp.filter { it.categoryLevel.level2 == displayCategoryLevel.level2 }
            tmp = tmp.filter { it.categoryLevel.level1 == displayCategoryLevel.level1 }
            tmp = tmp.filter { it.categoryLevel.level0 == displayCategoryLevel.level0 }
        }

        if (displayLastLevel == 2) {
            tmp = tmp.filter { it.categoryLevel.level6 == "" || it.categoryLevel.level6 == "0" }
            tmp = tmp.filter { it.categoryLevel.level5 == "" || it.categoryLevel.level5 == "0" }
            tmp = tmp.filter { it.categoryLevel.level4 == "" || it.categoryLevel.level4 == "0" }
//            tmp = tmp.filter { it.categoryLevel.level3 == displayCategoryLevel.level3 }
//            tmp = tmp.filter { it.categoryLevel.level2 == displayCategoryLevel.level2 }
            tmp = tmp.filter { it.categoryLevel.level1 == displayCategoryLevel.level1 }
            tmp = tmp.filter { it.categoryLevel.level0 == displayCategoryLevel.level0 }
        }

        if (displayLastLevel == 1) {
            tmp = tmp.filter { it.categoryLevel.level6 == "" || it.categoryLevel.level6 == "0" }
            tmp = tmp.filter { it.categoryLevel.level5 == "" || it.categoryLevel.level5 == "0" }
            tmp = tmp.filter { it.categoryLevel.level4 == "" || it.categoryLevel.level4 == "0" }
            tmp = tmp.filter { it.categoryLevel.level3 == "" || it.categoryLevel.level3 == "0" }
//            tmp = tmp.filter { it.categoryLevel.level2 == displayCategoryLevel.level2 }
//            tmp = tmp.filter { it.categoryLevel.level1 == displayCategoryLevel.level1 }
            tmp = tmp.filter { it.categoryLevel.level0 == displayCategoryLevel.level0 }
        }

        if (displayLastLevel == 0) {
            tmp = tmp.filter { it.categoryLevel.level6 == "" || it.categoryLevel.level6 == "0" }
            tmp = tmp.filter { it.categoryLevel.level5 == "" || it.categoryLevel.level5 == "0" }
            tmp = tmp.filter { it.categoryLevel.level4 == "" || it.categoryLevel.level4 == "0" }
            tmp = tmp.filter { it.categoryLevel.level3 == "" || it.categoryLevel.level3 == "0" }
            tmp = tmp.filter { it.categoryLevel.level2 == "" || it.categoryLevel.level2 == "0" }
//            tmp = tmp.filter { it.categoryLevel.level1 == displayCategoryLevel.level1 }
//            tmp = tmp.filter { it.categoryLevel.level0 == displayCategoryLevel.level0 }
        }

        tmp.sortedBy { it.id }
        productList.addAll(tmp)

        return productList
    }

    fun getProductByBarcode(barcode: String) : Product {
        val index = DummyProduct.ITEMS.indexOfFirst { it.barcode.code == barcode }
        return DummyProduct.ITEMS[index]
    }

    fun getProductById(id: Int) : Product {
        return DummyProduct.ITEMS!!.first {it.id == id}
    }

    private fun makeDetails(position: Int): String {
        val builder = StringBuilder()
        builder.append("Details about Item: ").append(position)
        for (i in 0..position - 1) {
            builder.append("\nMore details information here.")
        }
        return builder.toString()
    }


}
