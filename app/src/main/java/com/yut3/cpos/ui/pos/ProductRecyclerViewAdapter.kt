package com.yut3.cpos.ui.pos

import android.graphics.Color
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.yut3.cpos.R
import com.yut3.cpos.data.model.Product


import com.yut3.cpos.ui.pos.ProductFragment.OnListFragmentInteractionListener
import com.yut3.cpos.ui.pos.dummy.DummyContent.DummyItem
import com.yut3.cpos.ui.pos.dummy.ForSaleProduct

import kotlinx.android.synthetic.main.fragment_product.view.*
import org.w3c.dom.Text

/**
 * [RecyclerView.Adapter] that can display a [DummyItem] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 * TODO: Replace the implementation with code for your data type.
 */
class ProductRecyclerViewAdapter(
    private val mValues: List<Product>,
    private val mListener: OnListFragmentInteractionListener?
) : RecyclerView.Adapter<ProductRecyclerViewAdapter.ViewHolder>() {
    private var mProductList: MutableList<Product> = mValues.toMutableList()

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Product
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            mListener?.onListFragmentInteraction(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_product, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mProductList[position]
        if (item.id == -1) { // previous button
            holder.mProductImageView.setImageResource(R.drawable.ic_navigate_before_120dp)
        //} else if (item.id == 0) { // next category button
        //    holder.mProductImageView.setImageResource(R.drawable.ic_toc_120dp)

        } else if (item.uid == "飲品") { // next category button
            holder.mProductImageView.setImageResource(R.raw.class1.toInt())
        } else if (item.uid == "零食") { // next category button
            holder.mProductImageView.setImageResource(R.raw.class2.toInt())
        } else if (item.uid == "泡麵") { // next category button
            holder.mProductImageView.setImageResource(R.raw.class3.toInt())
        } else { // product image button
            //holder.mProductImageView.setImageResource(R.mipmap.ic_launcher)
            holder.mProductImageView.setImageResource(ForSaleProduct.getProductPicture(item.id))
        }
        holder.mProductName.text = item.uid
        holder.mProductColorBar.setBackgroundColor(Color.GREEN)
        holder.mProductHighlightColorBar.setBackgroundColor(Color.YELLOW)

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mProductList.size

    fun setPageList(mValues: List<Product>) {
        mProductList = mValues.toMutableList()
        notifyDataSetChanged()

    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mProductName: TextView = mView.productName
        val mProductImageView: ImageView = mView.productImageView
        val mProductColorBar: TextView = mView.productColorBarTextView
        val mProductHighlightColorBar: TextView = mView.productHighlightColorBarTextView


        override fun toString(): String {
            return super.toString() + " '" + mProductName.text + "'"
        }
    }
}
