package com.yut3.cpos.ui.pos

import android.app.Activity
import android.app.Application
import android.app.ProgressDialog
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Rect
import android.os.AsyncTask
import android.util.Log
import android.util.Size
import androidx.lifecycle.AndroidViewModel
import com.yut3.cpos.CPosSharedViewModel
import com.yut3.cpos.data.InferenceResult
import org.opencv.android.Utils
import org.opencv.core.*
import org.opencv.imgproc.Imgproc
import java.lang.IllegalStateException
import kotlin.math.abs

class CameraDetectViewModel(application: Application) : AndroidViewModel(application){
    private val mContext: Context = getApplication<Application>().applicationContext

    private lateinit var cPosSharedViewModel: CPosSharedViewModel

    private lateinit var mInferenceWrapper: InferenceWrapper
    //private lateinit var mCameraFragment: CameraFragment
    private lateinit var mInferenceResult: InferenceResult
    private lateinit var mSurfacePreviewSize: Size

    private var mRknnInitDone = false

    private var roiBitmap: Bitmap? = null
    private var mCroppedInferenceRangeFromCameraFragment: Rect = Rect(240, 0, 1680, 1080)
    private var mSelectedInferenceRangeFromCameraFragment: Rect = Rect(240, 0, 1680, 1080)
    //private var mCroppedInferenceRangeFromCameraFragment: Rect = Rect(160, 0, 1120, 720)
    //private var mSelectedInferenceRangeFromCameraFragment: Rect = Rect(160, 0, 1120, 720)
    private var mSelectedInferenceRangeMode: Int = 0  //0: crop, 1: padding, 2: crop + padding


    private val DEBUG_ENABLE = 0

    fun isRknnInitDone() : Boolean {
        return mRknnInitDone
    }

    fun rknnModelInit() {
        mInferenceWrapper = InferenceWrapper(mContext)
        // Uri.parse("android.resource://" + packageName + "/ssd.rknn").toString()

        // first model
        mInferenceWrapper!!.create(0, mContext.applicationContext.cacheDir.absolutePath, "yolo3tiny_stage1.rknn")
        val io: IntArray = intArrayOf(3, 4)
        mInferenceWrapper!!.queryIoCount(0, io)
        Log.e("IW", "modified value: (${io[0]}, ${io[1]})")
        mInferenceWrapper!!.queryOutputsAttribute(0, 2, 0)

        for (i in 1..MOBILENET_MODEL_PARALLEL_THREAD_COUNT) {
            // second model
            mInferenceWrapper!!.create(i, mContext.applicationContext.cacheDir.absolutePath, "mobilenet_stage2.rknn")
            val io2: IntArray = intArrayOf(5, 6)
            mInferenceWrapper!!.queryIoCount(i, io2)
            Log.e("IW", "model 2 counter: [${i}], modified value: (${io2[0]}, ${io2[1]})")
            mInferenceWrapper!!.queryOutputsAttribute(i, 1, 0)
        }


        mRknnInitDone = true

//        InferenceWrapperInitAsyncTask().execute()

    }

    fun cameraDetectRun(cameraFragment: CameraFragment) : InferenceResult {
        var bitmap: Bitmap? = cameraFragment.getBitmap()
        var croppedBitmap: Bitmap? = null
        var bytes: ByteArray? = null

        croppedBitmap = cropBitmapAndResize(bitmap!!, mCroppedInferenceRangeFromCameraFragment, mCroppedInferenceRangeFromCameraFragment.width(), mCroppedInferenceRangeFromCameraFragment.height())
        bytes = convertRGBA8888BitmapToRGB888ByteArray(cameraFragment, Bitmap.createScaledBitmap(croppedBitmap!!, mInferenceWrapper.getModelInputWidth(0), mInferenceWrapper.getModelInputHeight(0), false))
        mInferenceWrapper!!.setOriginalImageSize(mCroppedInferenceRangeFromCameraFragment.width(), mCroppedInferenceRangeFromCameraFragment.height())

        mInferenceWrapper!!.predict(0, 0, bytes, mInferenceWrapper.getModelInputWidth(0), mInferenceWrapper.getModelInputHeight(0), 3)

        mInferenceResult = mInferenceWrapper!!.result(0)
        Log.i("ODA", "object count: ${mInferenceResult!!.objectCount}")

        val inferenceThreads: Array<Thread?> = Array(1 + MOBILENET_MODEL_PARALLEL_THREAD_COUNT) {null}

        for(index in 0 until mInferenceResult!!.objectCount) {
            var threadTimeout = 0
            if (index >= MOBILENET_MODEL_PARALLEL_THREAD_COUNT) {
                while (inferenceThreads[index % MOBILENET_MODEL_PARALLEL_THREAD_COUNT]!!.isAlive) {
                    Log.d("ODA", "wait for thread done: $index")
                    Thread.sleep(INFERENCE_THREAD_WAIT_TIME.toLong())
                    threadTimeout += INFERENCE_THREAD_WAIT_TIME
                    if (threadTimeout >= INFERENCE_THREAD_TIMEOUT) {
                        Log.e("ODA", "inference thread:[$index] timeout")
                        break
                    }
                }
            }
            if (threadTimeout >= INFERENCE_THREAD_TIMEOUT) {
                Log.e("ODA", "escape thread[$index] because it is dying and can not stop it safely")
                continue
            }

            val cropImageBytes = cropBitmapToByteArrayAndResize(croppedBitmap!!, mInferenceResult!!.outputObjects[index].iObject.coordinate, mInferenceWrapper.getModelInputWidth(1), mInferenceWrapper.getModelInputHeight(1))

            val runnable = Runnable {
                var workTimeout = 0
                while (!mInferenceWrapper!!.isPredictDone((index % MOBILENET_MODEL_PARALLEL_THREAD_COUNT) + 1)) {
                    Log.d("ODA", "wait for work done: $index")
                    Thread.sleep(INFERENCE_WORK_WAIT_TIME.toLong())
                    workTimeout += INFERENCE_WORK_WAIT_TIME
                    if (workTimeout >= INFERENCE_WORK_TIMEOUT) {
                        Log.e("ODA", "inference work:[$index] timeout\n")
                        break
                    }
                }
                if (workTimeout < INFERENCE_WORK_TIMEOUT) {
                    val startTime = System.currentTimeMillis()
                    mInferenceWrapper!!.predict((index % MOBILENET_MODEL_PARALLEL_THREAD_COUNT) + 1, 0, cropImageBytes!!, mInferenceWrapper.getModelInputWidth(1), mInferenceWrapper.getModelInputHeight(1),3)
                    val mClassInferenceResult = mInferenceWrapper!!.result((index % MOBILENET_MODEL_PARALLEL_THREAD_COUNT) + 1)
                    val elapsedTime = System.currentTimeMillis() - startTime
                    Log.i("ODA", "inference and get outputs time: $elapsedTime ms")

                    val maxId = mClassInferenceResult.dataArray.indexOf(mClassInferenceResult.dataArray.max())
/*
                    val mappingList = resources.getStringArray(R.array.mappingList).toMutableList()
                    val mappingId = mappingList[maxId]
                    val itemList = resources.getStringArray(R.array.itemList).toMutableList()
                    mInferenceResult!!.outputObjects[index].iObject.tag = itemList[mappingId.toInt() - 1]
                    mInferenceResult!!.outputObjects[index].sortedIndices = mClassInferenceResult.dataArray.withIndex()
                        .sortedByDescending { it.value }
                        .map { it.index }
                        .toMutableList()
                    mInferenceResult!!.outputObjects[index].iObject.candidateTagName = arrayListOf()

                    // Top 2
                    if (mClassInferenceResult.dataArray[mInferenceResult!!.outputObjects[index].sortedIndices?.get(1)!!] > 0.001) {
                        mInferenceResult!!.outputObjects[index].iObject.candidateTagName!!.add(itemList[mappingList[mInferenceResult!!.outputObjects[index].sortedIndices?.get(1)!!].toInt() - 1])
                    }
                    // Top 3
                    if (mClassInferenceResult.dataArray[mInferenceResult!!.outputObjects[index].sortedIndices?.get(2)!!] > 0.001) {
                        mInferenceResult!!.outputObjects[index].iObject.candidateTagName!!.add(itemList[mappingList[mInferenceResult!!.outputObjects[index].sortedIndices?.get(2)!!].toInt() - 1])
                    }
                    // Top 4
                    if (mClassInferenceResult.dataArray[mInferenceResult!!.outputObjects[index].sortedIndices?.get(3)!!] > 0.001) {
                        mInferenceResult!!.outputObjects[index].iObject.candidateTagName!!.add(itemList[mappingList[mInferenceResult!!.outputObjects[index].sortedIndices?.get(3)!!].toInt() - 1])
                    }
*/

                    //try {
                        mInferenceResult!!.outputObjects[index].iObject.bitmap = cropBitmapAndResize(croppedBitmap!!, mInferenceResult!!.outputObjects[index].iObject.coordinate, mInferenceWrapper.getModelInputWidth(1), mInferenceWrapper.getModelInputHeight(1))
                        mInferenceResult!!.outputObjects[index].iObject.coordinate = Rect(
                            (((mInferenceResult!!.outputObjects[index].iObject.coordinate.left).toFloat() + mCroppedInferenceRangeFromCameraFragment.left) / bitmap.width * cameraFragment.getSurfacePreviewSize().width).toInt(),
                            (((mInferenceResult!!.outputObjects[index].iObject.coordinate.top).toFloat() + mCroppedInferenceRangeFromCameraFragment.top) / bitmap.height * cameraFragment.getSurfacePreviewSize().height).toInt(),
                            (((mInferenceResult!!.outputObjects[index].iObject.coordinate.right).toFloat() + mCroppedInferenceRangeFromCameraFragment.left) / bitmap.width * cameraFragment.getSurfacePreviewSize().width).toInt(),
                            (((mInferenceResult!!.outputObjects[index].iObject.coordinate.bottom).toFloat() + mCroppedInferenceRangeFromCameraFragment.top) / bitmap.height * cameraFragment.getSurfacePreviewSize().height).toInt())

                        //mInferenceResult!!.outputObjects[index].iObject.tagId = mappingId.toInt()
                        mInferenceResult!!.outputObjects[index].iObject.tagId = maxId
                        //try {
                            //mInferenceResult!!.outputObjects[index].iObject.ean13 = c4SharedViewModel.item.getEanCodeById(mInferenceResult!!.outputObjects[index].iObject.tagId!!)
                        //} catch (e: IllegalStateException) {
//                                Log.e("ODA", "inference tagId:[${mInferenceResult!!.outputObjects[index].iObject.tagId}]")
                          //  e.printStackTrace()
                        //}
                    //} catch (e: Exception) {
                    //    e.printStackTrace()
                    //}
                }
                Thread.sleep(2)
            }
            inferenceThreads[index % MOBILENET_MODEL_PARALLEL_THREAD_COUNT] = Thread(runnable)
            inferenceThreads[index % MOBILENET_MODEL_PARALLEL_THREAD_COUNT]?.priority = Thread.MAX_PRIORITY
            inferenceThreads[index % MOBILENET_MODEL_PARALLEL_THREAD_COUNT]?.start()
        }
        try {
            inferenceThreads.forEach {
                it?.join()
            }
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

        //mInferenceResult!!.outputObjects = mInferenceResult!!.outputObjects.sortedWith(compareBy {it.iObject.tagId}).toMutableList()

        //c4SharedViewModel.setRObjectList(mInferenceResult!!.outputObjects)
        //cPosSharedViewModel.setRObjectList(mInferenceResult!!.outputObjects)

        //backgroundHandler!!.sendMessage(backgroundHandler!!.obtainMessage(UPDATE_UI_DATA_MESSAGE))

        Thread.sleep(5)

        return mInferenceResult
    }


    private fun cropBitmapAndResize(bitmap8888: Bitmap, rect: Rect, width: Int, height: Int) : Bitmap {
        val cvmatRgba = Mat()
        val resizeRgba = Mat(height, width, CvType.CV_8UC4)

        try {
            Utils.bitmapToMat(bitmap8888, cvmatRgba)
        } catch (e: CvException) {
            e.printStackTrace()
            Log.e("ODA", "ROI resize failed")
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("ODA", "ROI resize failed")
        }

        lateinit var roi: Mat
        try {
            if (rect.left < 0) {
                rect.left = 0
            }

            if (rect.top < 0) {
                rect.top = 0
            }

            if (rect.right > bitmap8888.width) {
                rect.right = bitmap8888.width
            }

            if (rect.bottom > bitmap8888.height) {
                rect.bottom = bitmap8888.height
            }

            roi = Mat(cvmatRgba, org.opencv.core.Rect(rect.left, rect.top, abs(rect.left - rect.right), abs(rect.top - rect.bottom)))
            Imgproc.resize(roi, resizeRgba, Size(width.toDouble(), height.toDouble()))
        } catch (e: CvException) {
            e.printStackTrace()
            Log.e("ODA", "ROI resize failed")
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("ODA", "ROI resize failed")
        }


        try {
            val bmp = Bitmap.createBitmap(roi.cols(), roi.rows(), Bitmap.Config.ARGB_8888)
            Utils.matToBitmap(roi, bmp)
            return bmp
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }

        return bitmap8888
    }

    private fun convertRGBA8888BitmapToRGB888ByteArray(cameraFragment: CameraFragment, bitmap: Bitmap): ByteArray {
        val cvmatRgba = Mat()
        val cvmatRgb = Mat()
        Utils.bitmapToMat(bitmap, cvmatRgba)
        Imgproc.cvtColor(cvmatRgba, cvmatRgb, Imgproc.COLOR_RGBA2RGB)

        var selectedRange: Rect? = null
        val bytes = ByteArray(cvmatRgb.channels() * cvmatRgb.rows() * cvmatRgb.cols())


        if (mSelectedInferenceRangeMode == 0) { // cropping, range from EditText
//                selectedRange = getSelectedInferenceImageRangeFromEditText(YOLO_INPUT_WIDTH.toFloat(), YOLO_INPUT_HEIGHT.toFloat())
            cvmatRgb.get(0, 0, bytes)

            if (DEBUG_ENABLE == 1) {
                roiBitmap = Bitmap.createBitmap(cvmatRgb.cols(), cvmatRgb.rows(), Bitmap.Config.ARGB_8888)
                Utils.matToBitmap(cvmatRgb, roiBitmap)
            }
        }
        else if(mSelectedInferenceRangeMode == 1) { // padding, range from CameraFragment
            selectedRange = getSelectedInferenceImageRangeFromCameraFragment(cameraFragment, mInferenceWrapper.getModelInputHeight(0).toFloat(), mInferenceWrapper.getModelInputWidth(0).toFloat())
            if (selectedRange.width() <= 0 || selectedRange.height() <= 0) {
                selectedRange = getSelectedInferenceImageRangeFromEditText(cameraFragment, mInferenceWrapper.getModelInputHeight(0).toFloat(), mInferenceWrapper.getModelInputWidth(0).toFloat())
                if (selectedRange.width() <= 0 || selectedRange.height() <= 0) {
                    selectedRange = Rect(0,0, bitmap.width, bitmap.height)
                }
            }

            val mask = Mat(cvmatRgb.rows(), cvmatRgb.cols(), CvType.CV_8U, Scalar.all(0.0))
            val p1 = org.opencv.core.Point(selectedRange.left.toDouble(), selectedRange.top.toDouble())
            val p2 = org.opencv.core.Point(selectedRange.right.toDouble(), selectedRange.bottom.toDouble())
            Imgproc.rectangle(mask, p1, p2, Scalar(255.0), -1)

            val cvmatSelectedImage = Mat()
            cvmatRgb.copyTo(cvmatSelectedImage, mask)
            cvmatSelectedImage.get(0, 0, bytes)

            if (DEBUG_ENABLE == 1) {
                roiBitmap = Bitmap.createBitmap(cvmatSelectedImage.cols(), cvmatSelectedImage.rows(), Bitmap.Config.ARGB_8888)
                Utils.matToBitmap(cvmatSelectedImage, roiBitmap)
            }
        }
        else if (mSelectedInferenceRangeMode == 2){ // cropping + padding, range from EditText ; CameraFragment
            var selectedRangeFromEditText = mSelectedInferenceRangeFromCameraFragment
            var selectedRangeFromCameraFragment = cameraFragment.getSelectedRange()
            if (selectedRangeFromCameraFragment.width() <= 0 || selectedRangeFromCameraFragment.height() <= 0) {
                if (selectedRangeFromEditText.width() > 0 && selectedRangeFromEditText.height() > 0) {
                    selectedRangeFromCameraFragment = selectedRangeFromEditText
                } else {
                    selectedRangeFromCameraFragment = Rect(0, 0, cameraFragment.getCameraViewSize().x, cameraFragment.getCameraViewSize().y)
                    selectedRangeFromEditText = selectedRangeFromCameraFragment
                }
            }


            selectedRange = Rect(
                ((selectedRangeFromCameraFragment.left - selectedRangeFromEditText.left).toFloat() / selectedRangeFromEditText.width() * mInferenceWrapper.getModelInputWidth(0)).toInt(),
                ((selectedRangeFromCameraFragment.top - selectedRangeFromEditText.top).toFloat() / selectedRangeFromEditText.height() * mInferenceWrapper.getModelInputHeight(0)).toInt(),
                ((selectedRangeFromCameraFragment.right - selectedRangeFromEditText.left).toFloat() / selectedRangeFromEditText.width() * mInferenceWrapper.getModelInputWidth(0)).toInt(),
                ((selectedRangeFromCameraFragment.bottom - selectedRangeFromEditText.top).toFloat() / selectedRangeFromEditText.height() * mInferenceWrapper.getModelInputHeight(0)).toInt())

            val mask = Mat(cvmatRgb.rows(), cvmatRgb.cols(), CvType.CV_8U, Scalar.all(0.0))
            val p1 = org.opencv.core.Point(selectedRange.left.toDouble(), selectedRange.top.toDouble())
            val p2 = org.opencv.core.Point(selectedRange.right.toDouble(), selectedRange.bottom.toDouble())
            Imgproc.rectangle(mask, p1, p2, Scalar(255.0), -1)

            val cvmatSelectedImage = Mat()
            cvmatRgb.copyTo(cvmatSelectedImage, mask)
            cvmatSelectedImage.get(0, 0, bytes)

            if (DEBUG_ENABLE == 1) {
                roiBitmap = Bitmap.createBitmap(cvmatSelectedImage.cols(), cvmatSelectedImage.rows(), Bitmap.Config.ARGB_8888)
                Utils.matToBitmap(cvmatSelectedImage, roiBitmap)
            }

        }

        return bytes
    }

    private fun getSelectedInferenceImageRangeFromEditText(cameraFragment: CameraFragment, targetWidth: Float, targetHeight: Float): android.graphics.Rect {
        val selectedRange = mCroppedInferenceRangeFromCameraFragment
        val cameraSize = cameraFragment.getCameraViewSize()

        return if ((mCroppedInferenceRangeFromCameraFragment.left < 0) &&
            (mCroppedInferenceRangeFromCameraFragment.top < 0) &&
            (mCroppedInferenceRangeFromCameraFragment.right < 0) &&
            (mCroppedInferenceRangeFromCameraFragment.bottom < 0)) {
            Rect(-1, -1, -1, -1)
        } else {
            Rect((selectedRange.left.toFloat() / cameraSize.x.toFloat() * targetWidth).toInt(),
                (selectedRange.top.toFloat() / cameraSize.y.toFloat() * targetHeight).toInt(),
                (selectedRange.right.toFloat() / cameraSize.x.toFloat() * targetWidth).toInt(),
                (selectedRange.bottom.toFloat() / cameraSize.y.toFloat() * targetHeight).toInt())
        }
    }

    private fun getSelectedInferenceImageRangeFromCameraFragment(cameraFragment: CameraFragment, targetWidth: Float, targetHeight: Float): android.graphics.Rect {
        var selectedRange: Rect = cameraFragment.getSelectedRange()
        val cameraMaxPreviewSize = cameraFragment.getSupportedPreviewSize(-1)
        val cameraSize = cameraFragment.getCameraViewSize()
        selectedRange = Rect(
            ((selectedRange.left.toFloat() / cameraMaxPreviewSize!!.width.toFloat()) * cameraSize.x.toFloat()).toInt(),
            ((selectedRange.top.toFloat() / cameraMaxPreviewSize!!.height.toFloat()) * cameraSize.y.toFloat()).toInt(),
            ((selectedRange.right.toFloat() / cameraMaxPreviewSize!!.width.toFloat()) * cameraSize.x.toFloat()).toInt(),
            ((selectedRange.bottom.toFloat() / cameraMaxPreviewSize!!.height.toFloat()) * cameraSize.y.toFloat()).toInt()
        )
        mSelectedInferenceRangeFromCameraFragment = selectedRange

        return if ((selectedRange.left < 0) ||
            (selectedRange.top < 0) ||
            (selectedRange.right < 0) ||
            (selectedRange.bottom < 0)) {
            Rect(-1, -1, -1, -1)
        } else {
            Rect((selectedRange.left.toFloat() / cameraSize.x.toFloat() * targetWidth).toInt(),
                (selectedRange.top.toFloat() / cameraSize.y.toFloat() * targetHeight).toInt(),
                (selectedRange.right.toFloat() / cameraSize.x.toFloat() * targetWidth).toInt(),
                (selectedRange.bottom.toFloat() / cameraSize.y.toFloat() * targetHeight).toInt())
        }
    }

    private fun cropBitmapToByteArrayAndResize(bitmap8888: Bitmap, rect: Rect, width: Int, height: Int) : ByteArray {
        val cvmatRgba = Mat()
        val cvmatRgb = Mat()
        val resizeRgb = Mat(height, width, CvType.CV_8UC3)

        try {
            Utils.bitmapToMat(bitmap8888, cvmatRgba)
            Imgproc.cvtColor(cvmatRgba, cvmatRgb, Imgproc.COLOR_RGBA2RGB)
        } catch (e: CvException) {
            e.printStackTrace()
            Log.e("ODA", "ROI resize failed")
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("ODA", "ROI resize failed")
        }

        lateinit var roi: Mat
        try {
            if (rect.left < 0) {
                rect.left = 0
            }

            if (rect.top < 0) {
                rect.top = 0
            }

            if (rect.right > bitmap8888.width) {
                rect.right = bitmap8888.width
            }

            if (rect.bottom > bitmap8888.height) {
                rect.bottom = bitmap8888.height
            }
            roi = Mat(cvmatRgb, org.opencv.core.Rect(rect.left, rect.top, abs(rect.left - rect.right), abs(rect.top - rect.bottom)))
            Imgproc.resize(roi, resizeRgb, Size(width.toDouble(), height.toDouble()))
        } catch (e: CvException) {
            e.printStackTrace()
            Log.e("ODA", "ROI resize failed")
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("ODA", "ROI resize failed")
        }
        val bytes = ByteArray(resizeRgb.channels() * resizeRgb.rows() * resizeRgb.cols())
        resizeRgb.get(0, 0, bytes)
        return bytes

    }

    inner class InferenceWrapperInitAsyncTask : AsyncTask<Void, Int, Void>() {


        override fun onPreExecute() {
            super.onPreExecute()
        }

        override fun doInBackground(vararg param: Void?): Void? {

            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
        }
    }

    companion object {
        private const val MOBILENET_MODEL_PARALLEL_THREAD_COUNT = 2

        private const val INFERENCE_THREAD_TIMEOUT = 3000
        private const val INFERENCE_WORK_TIMEOUT = 2000
        private const val INFERENCE_THREAD_WAIT_TIME = 10
        private const val INFERENCE_WORK_WAIT_TIME = 10
    }

}