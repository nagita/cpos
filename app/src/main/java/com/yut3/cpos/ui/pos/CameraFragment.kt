package com.yut3.cpos.ui.pos


import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.app.AlertDialog
import android.content.ClipData
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.*
import android.hardware.camera2.*
import android.hardware.camera2.params.StreamConfigurationMap
import android.icu.text.SimpleDateFormat
import android.media.ImageReader
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.text.InputType
import android.util.Log
import android.util.Size
import android.view.*
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.Constraints
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.google.zxing.integration.android.IntentIntegrator
import com.yut3.cpos.CPosSharedViewModel
import com.yut3.cpos.R
import com.yut3.cpos.XtermColor
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.lang.Exception
import java.util.*
import java.util.concurrent.Semaphore
import java.util.concurrent.ThreadLocalRandom
import java.util.concurrent.TimeUnit
import kotlin.math.abs


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//private const val ARG_PARAM1 = "param1"
//private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [CameraFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [CameraFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class CameraFragment : Fragment(), View.OnClickListener {
    // for fragment variables
    private lateinit var mTextureView: TextureView
    private lateinit var mSurfaceView: SurfaceView
    private var mContext: Context? = null

    private lateinit var cPosSharedViewModel: CPosSharedViewModel

    // for camera device
    private var mImageReader: ImageReader? = null
    private var mCameraDevice: CameraDevice? = null
    private var mCaptureSession: CameraCaptureSession? = null
    private lateinit var mSurfacePreviewSize: Size
    private lateinit var mPreviewRequestBuilder: CaptureRequest.Builder
    private lateinit var mPreviewRequest: CaptureRequest
    private var mCameraSupportedPreviewSize: MutableList<Size> = arrayListOf()

    // for task
    private var mBackgroundHandlerThread: HandlerThread? = null
    private var mBackgroundHandler: Handler? = null
    private var mSemaphore: Semaphore = Semaphore(SEMAPHORE_PERMITS_COUNT)
    private var mMJPEGServerHandler: Handler? = null
    private var mMJPEGServerHandlerThread: HandlerThread? = null

    // for application
    private var mBoundingBoxes: MutableList<Rect> = arrayListOf()
    private var mTouchDownPositionX: Float = -1.0F
    private var mTouchDownPositionY: Float = -1.0F
    private var mNeedToCheckObjectPositionX: Float = -1.0F
    private var mNeedToCheckObjectPositionY: Float = -1.0F
    private var mDragStartPosition: Point = android.graphics.Point(-1, -1)
    private var mDragEndPosition: Point = android.graphics.Point(-1, -1)
    private var mDragStartPosition_resize: Point = android.graphics.Point(-1, -1)
    private var mDragEndPosition_resize: Point = android.graphics.Point(-1, -1)
    private lateinit var mPath: String
    private var bitmap: Bitmap? = null

//    private lateinit var c4SharedViewModel: C4SharedViewModel
    private var onSharedRObjectsChangedListener: CameraOnSharedRObjectsChangedListener? = null

    private var mCaptureEventListener: CaptureEventListener? = null
    private lateinit var mIntentIntegrator: IntentIntegrator
    private val mCameraOpenCloseLock = Semaphore(1)

    private var mLastPressTime: Long = 0
    private var mDoubleClickMode: Boolean = false
    private var mDoubleClickChecker: Int = 0

    private var mSaveFileSemaphore: Semaphore = Semaphore(SEMAPHORE_PERMITS_COUNT)

    // TODO: Rename and change types of parameters
//    private var param1: String? = null
//    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    fun getSupportedPreviewSize(index: Int) : Size? {
        if (mCameraSupportedPreviewSize.size <= 0) {
            return null
        }
        return if (index < 0) {
            mCameraSupportedPreviewSize[mCameraSupportedPreviewSize.size - 1]
        } else {
            mCameraSupportedPreviewSize[if (index < mCameraSupportedPreviewSize.size) index else (mCameraSupportedPreviewSize.size - 1)]
        }
    }

    fun getSurfacePreviewSize() : Size {
        return mSurfacePreviewSize
    }

    fun getSelectedRange() : Rect {
        return Rect(mDragStartPosition_resize.x, mDragStartPosition_resize.y, mDragEndPosition_resize.x, mDragEndPosition_resize.y)
    }

    fun getCameraViewSize(): Point {
        val cameraSurfaceView: SurfaceView = view!!.findViewById(R.id.CameraSurfaceView)
        return Point(cameraSurfaceView.layoutParams.width, cameraSurfaceView.layoutParams.height)
    }

    fun getDoubleClickModeStatus() : Boolean {
        return mDoubleClickMode
    }

    fun getTouchedPositon() : Pair<Float,Float> {
        return Pair(mTouchDownPositionX, mTouchDownPositionY)
    }

    fun getBitmap(): Bitmap? {
        try {
            mCaptureSession?.stopRepeating()
            if (mTextureView.isAvailable)  {
                bitmap = Bitmap.createScaledBitmap(mTextureView.bitmap, mSurfacePreviewSize.width, mSurfacePreviewSize.height, false)
                Log.i("CF", "bitmap size (${bitmap?.width}, ${bitmap?.height})")
            }
            mCaptureSession?.setRepeatingRequest(mPreviewRequest, null, Handler(mBackgroundHandlerThread?.looper))
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }
        return bitmap
    }

    fun saveBitmap(bitmap: Bitmap) {
        if ( PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(context!!, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            // 設定済
            saveImageToStorage(bitmap)
        } else {
            Toast.makeText(mContext, "No Permission to write external storage", Toast.LENGTH_SHORT).show()
            requestExternalStoragePermission()
        }
    }


    fun stopUpdateCameraView() {
        try {
            mCaptureSession?.stopRepeating() // プレビューの更新を止める
            if (mTextureView.isAvailable) {
//                // save picture to storage
//                bitmap = mTextureView.bitmap // bitmap from texture view
//                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, null)
            }
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }

    }

    fun startUpdateCameraView() {
        try {
            mCaptureSession?.setRepeatingRequest(mPreviewRequest, null, Handler(mBackgroundHandlerThread?.looper))
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }
    }

    fun getSharedRObjects() {
        mSemaphore.acquire()
        val rObjects = cPosSharedViewModel.getRObjectList().value
        var boundingBoxes: MutableList<Rect> = arrayListOf()
        rObjects!!.forEach {
            boundingBoxes.add(it.iObject.coordinate)
        }
        mBoundingBoxes = boundingBoxes
        mSemaphore.release()
    }

    fun setBoundingBoxes(boundingBoxes: MutableList<Rect>) {
        mSemaphore.acquire()
        mBoundingBoxes = boundingBoxes.toMutableList()
        mSemaphore.release()
    }

    override fun onClick(v: View?) {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // this function will call after onAttach()
        arguments?.let {
            //            param1 = it.getString(ARG_PARAM1)
//            param2 = it.getString(ARG_PARAM2)
        }

        mMJPEGServerHandlerThread = HandlerThread("mjpeg_server_update_thread")
        mMJPEGServerHandlerThread!!.start()

//        @JvmStatic
        mMJPEGServerHandler = object: Handler(mMJPEGServerHandlerThread!!.looper) {
            override fun handleMessage(msg: Message?) {
                super.handleMessage(msg)
                when (msg?.what) {
                    UPDATE_MJPEG_SERVER_MESSAGE -> {
                        if (mCaptureEventListener != null) {
                            val baos = ByteArrayOutputStream()
                            bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                            mCaptureEventListener!!.onFrame(baos.toByteArray())
                        }
                    }
//                    UPDATE_UI_DATA_MESSAGE -> {
//                        val itemFragment = supportFragmentManager.findFragmentById(R.id.object_list_container) as ItemFragment
//                        val cameraFragment = supportFragmentManager.findFragmentById(R.id.camera_container) as CameraFragment
//                        cameraFragment.getSharedRObjects()
//                        itemFragment.getSharedRObject()
//                        test_image_view.setImageBitmap(diffBitmap)
//                    }
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // this function will call after onCreate() and before onViewCreated()
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_camera, container, false)
        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {

        } else {
            val cameraSurfaceView: SurfaceView = view!!.findViewById(R.id.CameraSurfaceView)
            cameraSurfaceView.layoutParams.width = 1280
            cameraSurfaceView.layoutParams.height = 720
            cameraSurfaceView.requestLayout()

            val cameraTextureView: TextureView = view!!.findViewById(R.id.CameraTextureView)
            cameraTextureView.layoutParams.width = 1280
            cameraTextureView.layoutParams.height = 720
            cameraTextureView.requestLayout()
        }

        mTextureView = view!!.findViewById(R.id.CameraTextureView)
        mTextureView.surfaceTextureListener = surfaceTextureListener
        mTextureView.setOnClickListener(textureViewOnClickListener)
        mTextureView.setOnTouchListener(textureViewOnTouchListener)
        mTextureView.setOnLongClickListener(textureViewOnLongClickListener)
        mTextureView.setOnDragListener(textureViewOnDragListener)
        view.setOnClickListener(this)
        mIntentIntegrator = IntentIntegrator.forSupportFragment(this)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // this function will call after onCreateView() and before onStart()
        super.onViewCreated(view, savedInstanceState)
        mSurfaceView = view.findViewById(R.id.CameraSurfaceView)
        mSurfaceView.setZOrderOnTop(true)
        val mHolder = mSurfaceView.holder
        mHolder.setFormat(PixelFormat.TRANSPARENT)
        mHolder.addCallback(surfaceHolderCallback)
    }

    override fun onPause() {
        // this function will call after fragment exit running status and before onStop()
        closeCamera()
        stopBackgroundThread()
        super.onPause()
    }

    override fun onResume() {
        // this function will call after onStart() and before fragment into running status
        super.onResume()
        startBackgroundThread()
        if (mTextureView.isAvailable) {
            if (mImageReader == null) {
                mImageReader = ImageReader.newInstance(
                    mSurfacePreviewSize.width, mSurfacePreviewSize.height, ImageFormat.JPEG, 2)
            }
            openCamera()
        } else {
            if (mTextureView.surfaceTextureListener == null) {
                mTextureView.surfaceTextureListener = surfaceTextureListener
            }
        }
    }

    override fun onAttach(context: Context) {
        // this is the first startup function in fragment's lifecycle
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            // TODO: Remove this while camera fragment crash at resume
//            listener = context
        } else {
            // TODO: Remove this while camera fragment crash at resume
//            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
//        cPosSharedViewModel = ViewModelProviders.of(activity!!).get(cPosSharedViewModel::class.java)
//        this.onSharedRObjectsChangedListener = activity as CameraOnSharedRObjectsChangedListener
    }

    override fun onDetach() {
        // this is the last function in fragment's lifecycle
        super.onDetach()
        // TODO: Remove this while camera fragment crash at resume
        listener = null
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults.size != 1 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Log.e("erfs", "camera permission NOT granted!!")
            }
        } else if (requestCode == REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION) {
            if (grantResults.size != 1 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Log.e("erfs", "write permission NOT granted!!")
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (intentResult != null) {
            if (intentResult.contents == null) {
                Snackbar.make(view!!, "Bar code の操作をキャンセルしました！", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
            } else {
                val editText = EditText(view!!.context).apply {
                    inputType = InputType.TYPE_CLASS_NUMBER
                    layoutParams = FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.MATCH_PARENT,
                        FrameLayout.LayoutParams.WRAP_CONTENT
                    ).apply {
                        val margin = (16 * resources.displayMetrics.density).toInt()
                        marginStart = margin
                        marginEnd = margin
                    }
                }

                val frameLayout = FrameLayout(view!!.context).apply {
                    addView(editText)
                }


                val dialog = AlertDialog.Builder(view!!.context)
                    .setTitle("コード名：${intentResult.formatName}")
                    .setMessage(intentResult.contents)
                    .setView(frameLayout)
                    .setPositiveButton(
                        android.R.string.ok,
                        object: DialogInterface.OnClickListener {
                            override fun onClick(dialog: DialogInterface?, which: Int) {
                                var itemCode : String = ""
                                if (editText.text.toString().trim().isEmpty()) {
                                    itemCode = intentResult.contents
                                } else {
                                    itemCode = editText.text.toString()
                                }

//                                val itemId = c4SharedViewModel.item.getIdByEanCode(itemCode)
//                                if (itemId < 0) {
//                                    AlertDialog.Builder(view!!.context)
//                                        .setTitle("エラーだよ！！エラーっ！！(´・ω・｀)")
//                                        .setMessage("DBにこのコードが見つからないから何もしないよ！！")
//                                        .setPositiveButton(android.R.string.ok, null)
//                                        .show()
//                                } else {
//                                    modifyItem(mNeedToCheckObjectPositionX, mNeedToCheckObjectPositionY, itemCode)
//                                }
                            }
                        }
                    )
                    .show()
                val tv = dialog.findViewById(android.R.id.message) as TextView
                tv.setTextSize(40.toFloat())

                Snackbar.make(view!!, "Bar code value: ${intentResult.contents}", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()


            }
//            startBackgroundThread()
//            openCamera()
        } else {
            super.onActivityResult(requestCode, resultCode, data)
            if (requestCode == CAMERA_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
//            val contentValues = ContentValues().apply {
//                put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
//                put("_data", path)
//            }
//            contentResolver.insert(
//                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)

                Snackbar.make(view!!, "On Activity Result", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
            }
        }
    }

    private val surfaceTextureListener = object: TextureView.SurfaceTextureListener {
        override fun onSurfaceTextureSizeChanged(surface: SurfaceTexture?, width: Int, height: Int) {

        }

        override fun onSurfaceTextureUpdated(surface: SurfaceTexture?) {
            mSurfaceView = view!!.findViewById(R.id.CameraSurfaceView)
            mSurfaceView.setZOrderOnTop(true)
            val mHolder = mSurfaceView.holder
            mHolder.setFormat(PixelFormat.TRANSPARENT)

            val canvas = mHolder!!.lockCanvas()
            if (canvas == null) {
                Log.e(Constraints.TAG, "Cannot draw onto the canvas as it's null")
            } else {
//                var width = canvas.width
//                var height = canvas.height
//                Log.i("CF", "canvas size($width, $height)")
                canvas.drawColor(0, PorterDuff.Mode.CLEAR)
                mSemaphore.acquire()

                mBoundingBoxes.forEach {
                    val myPaint = Paint()
                    myPaint.color = XtermColor().getXtermRgbCode(ThreadLocalRandom.current().nextInt(0, 255))
                    myPaint.strokeWidth = 5.0f
                    myPaint.style = Paint.Style.STROKE
                    val randomInteger = ThreadLocalRandom.current().nextInt(1, 10)
                    canvas.drawRect(
                        it.left.toFloat() + randomInteger.toFloat(),
                        it.top.toFloat() + randomInteger.toFloat(),
                        it.right.toFloat() + randomInteger.toFloat(),
                        it.bottom.toFloat() + randomInteger.toFloat(),
                        myPaint)
                }

//                val obj = c4SharedViewModel.getRObjectList().value
//                obj!!.forEach {
//                    val textPaint = Paint()
//                    textPaint.color = XtermColor().getXtermRgbCode(ThreadLocalRandom.current().nextInt(0, 255))
//                    textPaint.strokeWidth = 3.0f
//                    textPaint.textSize = 35.0f
//                    textPaint.style = Paint.Style.STROKE
//                    canvas.drawText(
//                        it.iObject.confidence.toString(),
//                        it.iObject.coordinate.left.toFloat(),
//                        it.iObject.coordinate.top.toFloat(),
//                        textPaint
//                    )
//                }


                if (mDoubleClickMode) {
                    val myPaint = Paint()
                    myPaint.color = Color.RED
                    myPaint.strokeWidth = 5.0f
                    myPaint.style = Paint.Style.STROKE
                    if (mDragStartPosition.x >= 0 && mDragStartPosition.y >= 0) {
                        canvas.drawRect(
                            mDragStartPosition.x.toFloat(),
                            mDragStartPosition.y.toFloat(),
                            mDragEndPosition.x.toFloat(),
                            mDragEndPosition.y.toFloat(),
                            myPaint)
                    }
                }

//                if( (c4SharedViewModel.selectedDrawItemID >= 0) && (c4SharedViewModel.selectedDrawItemID < mBoundingBoxes.size)) {
//                    val myPaint = Paint()
//                    myPaint.setARGB(180, 30, 114, 255)
//                    myPaint.strokeWidth = 5.0f
//                    myPaint.style = Paint.Style.FILL_AND_STROKE
//                    canvas.drawRect(
//                        mBoundingBoxes[c4SharedViewModel.selectedDrawItemID].left.toFloat(),
//                        mBoundingBoxes[c4SharedViewModel.selectedDrawItemID].top.toFloat(),
//                        mBoundingBoxes[c4SharedViewModel.selectedDrawItemID].right.toFloat(),
//                        mBoundingBoxes[c4SharedViewModel.selectedDrawItemID].bottom.toFloat(),
//                        myPaint)
//                }

                mSemaphore.release()
                mHolder.unlockCanvasAndPost(canvas)

                mMJPEGServerHandler!!.sendMessage(mMJPEGServerHandler!!.obtainMessage(
                    UPDATE_MJPEG_SERVER_MESSAGE))
            }
        }

        override fun onSurfaceTextureDestroyed(surface: SurfaceTexture?): Boolean {
            return false
        }

        override fun onSurfaceTextureAvailable(surface: SurfaceTexture?, width: Int, height: Int) {
            val cameraManager: CameraManager = mTextureView.context.getSystemService(Context.CAMERA_SERVICE) as CameraManager

            val streamConfigurationMap: StreamConfigurationMap? =
                cameraManager.getCameraCharacteristics(cameraManager.cameraIdList[CAMERA_ID]).get(
                    CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP) ?: return
            mCameraSupportedPreviewSize.clear()
            for (outputSize in streamConfigurationMap!!.getOutputSizes(ImageFormat.JPEG)) {
                Log.i("CF", "preview size: $outputSize")
                mCameraSupportedPreviewSize.add(outputSize)
            }
            mCameraSupportedPreviewSize.sortBy { it.width * it.height }
            if (mImageReader == null) {
                val previewSize = geLargestPreviewSize(Size(CAMERA_PREVIEW_SIZE_WIDTH, CAMERA_PREVIEW_SIZE_HEIGHT))
                mImageReader = ImageReader.newInstance(
                    previewSize.width, previewSize.height, ImageFormat.JPEG, 2)
            }
            openCamera()
            mSurfacePreviewSize = geLargestPreviewSize(Size(CAMERA_PREVIEW_SIZE_WIDTH, CAMERA_PREVIEW_SIZE_HEIGHT))
            Log.i("CF", "Surface Preview Size: (${mSurfacePreviewSize.width}, ${mSurfacePreviewSize.height})")
        }
    }

    private fun geLargestPreviewSize(requestSize: Size): Size {
        var size: Size = Size(-1, -1)
        mCameraSupportedPreviewSize.forEach {
            if (it.width <= requestSize.width && it.height <= requestSize.height) {
                if (size.width < it.width && size.height < it.height) {
                    size = it
                }
            }
        }
        return size
    }

    private val textureViewOnLongClickListener = object: View.OnLongClickListener {
        override fun onLongClick(p0: View?): Boolean {
            if (!mDoubleClickMode) {
                Snackbar.make(p0!!, "TextureView On Long Click !", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()

                if ((mTouchDownPositionX >= 0) && (mTouchDownPositionY >= 0)) {
                    val alertDialog = androidx.appcompat.app.AlertDialog.Builder(mTextureView.context).create()
                    alertDialog.setTitle("object on click pos($mTouchDownPositionX, $mTouchDownPositionY)")
                    alertDialog.setMessage("do something ?")

                    alertDialog.setButton(
                        androidx.appcompat.app.AlertDialog.BUTTON_POSITIVE, "Modify",
                        object : DialogInterface.OnClickListener {
                            override fun onClick(dialog: DialogInterface?, which: Int) {
                                Snackbar.make(p0!!, "Need to call MODIFY function", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show()
                                mNeedToCheckObjectPositionX = mTouchDownPositionX
                                mNeedToCheckObjectPositionY = mTouchDownPositionY

//                            stopBackgroundThread()
//
//                                mIntentIntegrator.setCameraId(1)
//                                mIntentIntegrator.captureActivity = BarcodeScannerActivity::class.java
//                                mIntentIntegrator.setDesiredBarcodeFormats(IntentIntegrator.EAN_13)
//                                mIntentIntegrator.setOrientationLocked(false)
//                                mIntentIntegrator.setBeepEnabled(true)
//                                mIntentIntegrator.initiateScan()

                            }
                        }
                    )

                    alertDialog.setButton(
                        androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE, "Remove",
                        object : DialogInterface.OnClickListener {
                            override fun onClick(dialog: DialogInterface?, which: Int) {
                                removeItem(mTouchDownPositionX, mTouchDownPositionY)
                                Snackbar.make(p0!!, "Item has been removed", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show()
                            }
                        }
                    )

                    alertDialog.setButton(
                        androidx.appcompat.app.AlertDialog.BUTTON_NEUTRAL, "Cancel",
                        object : DialogInterface.OnClickListener {
                            override fun onClick(dialog: DialogInterface?, which: Int) {
                                Snackbar.make(p0!!, "Operation has been cancelled", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show()
                            }
                        }
                    )

                    alertDialog.show()
                }
            }

            return true
        }
    }

    private val textureViewOnTouchListener = object: View.OnTouchListener {
        override fun onTouch(v: View?, event: MotionEvent?): Boolean {

            if (mDoubleClickMode) {
                val data : ClipData = ClipData.newPlainText("POS", "(${mDragEndPosition.x}, ${mDragEndPosition.y})")
                v!!.startDragAndDrop(data, View.DragShadowBuilder(v), v, 0)
            }

            // save the X,Y coordinates
            if (event!!.actionMasked == MotionEvent.ACTION_DOWN) {
                if (mDoubleClickMode) {

                } else {
                    mTouchDownPositionX = event.x
                    mTouchDownPositionY = event.y
                    Snackbar.make(
                        v!!,
                        "On Touch Event pos @($mTouchDownPositionX, $mTouchDownPositionY)",
                        Snackbar.LENGTH_LONG
                    ).setAction("Action", null).show()
                }
            }


            // let the touch event pass on to whoever needs it
            return false

        }
    }

    private val textureViewOnClickListener = object: View.OnClickListener {
        override fun onClick(v: View?) {
            val pressTime = System.currentTimeMillis()
            if (pressTime - mLastPressTime <= DOUBLE_PRESS_INTERVAL) {
                mDoubleClickMode = !mDoubleClickMode

//                mDoubleClickChecker = 0

//                if (mDoubleClickChecker == 1) {
//
//                }

//                if (mDoubleClickChecker == 2) {
//                    // double clicked
//                    mDoubleClickMode = !mDoubleClickMode
//                }

                if (mDoubleClickMode) {
                    Snackbar.make(
                        v!!,
                        "Into Double Click Mode",
                        Snackbar.LENGTH_LONG
                    ).setAction("Action", null).show()
                } else {
                    Snackbar.make(
                        v!!,
                        "Exit Double Click Mode",
                        Snackbar.LENGTH_LONG
                    ).setAction("Action", null).show()
                }
            } else {
//                if (!mDoubleClickMode) {
//                    mDoubleClickChecker = 0
//                }
            }
            mLastPressTime = pressTime


//                Snackbar.make(view, "TextureView On Click !", Snackbar.LENGTH_LONG)
//                    .setAction("Action", null).show()
        }
    }

    private val textureViewOnDragListener = object: View.OnDragListener {
        override fun onDrag(v: View?, event: DragEvent?): Boolean {
            when (event?.action) {
                DragEvent.ACTION_DRAG_STARTED -> {
                    Log.d("CF", "Drag Started: pos@(${event.x.toInt()}, ${event.y.toInt()})")
                    if (mDoubleClickMode) {
                        mDragStartPosition.x = event.x.toInt()
                        mDragStartPosition.y = event.y.toInt()
                    }
                }
                DragEvent.ACTION_DRAG_ENTERED -> {}
                DragEvent.ACTION_DRAG_EXITED -> {}
                DragEvent.ACTION_DRAG_LOCATION, DragEvent.ACTION_DROP -> {
                    Log.d("CF", "Drag Location or ACTION_DROP: pos@(${event.x.toInt()}, ${event.y.toInt()})")
                    if (mDoubleClickMode) {
                        mDragEndPosition.x = event.x.toInt()
                        mDragEndPosition.y = event.y.toInt()

                        val canvas = mSurfaceView.holder.lockCanvas()
                        if (canvas == null) {
//                            Log.e(Constraints.TAG, "Cannot draw onto the canvas as it's null")
                        } else {
                            if (mDoubleClickMode) {
                                canvas.drawColor(0, PorterDuff.Mode.CLEAR)

                                val myPaint = Paint()
                                myPaint.color = Color.RED
                                myPaint.strokeWidth = 5.0f
                                myPaint.style = Paint.Style.STROKE
                                if (mDragStartPosition.x >= 0 && mDragStartPosition.y >= 0) {
                                    canvas.drawRect(
                                        mDragStartPosition.x.toFloat(),
                                        mDragStartPosition.y.toFloat(),
                                        mDragEndPosition.x.toFloat(),
                                        mDragEndPosition.y.toFloat(),
                                        myPaint)
                                }
                            }
                        }
                        mSurfaceView.holder.unlockCanvasAndPost(canvas)
                    }
                }
                DragEvent.ACTION_DRAG_ENDED -> {
                    Log.d("CF", "Drag End: pos@(${event.x.toInt()}, ${event.y.toInt()})")
                    if (mDoubleClickMode) {
                        // show confirm message
                        val alertDialog = androidx.appcompat.app.AlertDialog.Builder(mTextureView.context).create()
                        alertDialog.setTitle("Selected Range(${mDragStartPosition.x}, ${mDragStartPosition.y})->(${mDragEndPosition.x}, ${mDragEndPosition.y})")
                        alertDialog.setMessage("(*´-`*).｡oO(これでいいの？)")

                        alertDialog.setButton(
                            androidx.appcompat.app.AlertDialog.BUTTON_POSITIVE, "いいよー！",
                            object : DialogInterface.OnClickListener {
                                override fun onClick(dialog: DialogInterface?, which: Int) {
                                    mDoubleClickMode = !mDoubleClickMode
                                    Snackbar.make(v!!, "選択した範囲がOKでした。", Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show()

                                    var temp: Int
                                    if(mDragStartPosition.x > mDragEndPosition.x) {
                                        temp = mDragStartPosition.x
                                        mDragStartPosition.x = mDragEndPosition.x
                                        mDragEndPosition.x = temp
                                    }
                                    if(mDragStartPosition.y > mDragEndPosition.y) {
                                        temp = mDragStartPosition.y
                                        mDragStartPosition.y = mDragEndPosition.y
                                        mDragEndPosition.y = temp
                                    }

                                    mDragStartPosition_resize.x = mDragStartPosition.x * mSurfacePreviewSize.width / mSurfaceView.width
                                    mDragStartPosition_resize.y = mDragStartPosition.y * mSurfacePreviewSize.height / mSurfaceView.height
                                    mDragEndPosition_resize.x = mDragEndPosition.x * mSurfacePreviewSize.width / mSurfaceView.width
                                    mDragEndPosition_resize.y = mDragEndPosition.y * mSurfacePreviewSize.height / mSurfaceView.height
                                }
                            }
                        )

                        alertDialog.setButton(
                            androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE, "もう一回やって～！",
                            object : DialogInterface.OnClickListener {
                                override fun onClick(dialog: DialogInterface?, which: Int) {
                                    mDragStartPosition = Point(-1, -1)
                                    mDragEndPosition = Point(-1, -1)
                                    Snackbar.make(v!!, "Do it again!!", Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show()
                                }
                            }
                        )

                        alertDialog.setButton(
                            androidx.appcompat.app.AlertDialog.BUTTON_NEUTRAL, "Cancel",
                            object : DialogInterface.OnClickListener {
                                override fun onClick(dialog: DialogInterface?, which: Int) {
                                    Snackbar.make(v!!, "Operation has been cancelled", Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show()
                                }
                            }
                        )

                        alertDialog.show()

                    }

                }
            }
            return true
        }
    }

    private val surfaceHolderCallback = object: SurfaceHolder.Callback {
        override fun surfaceCreated(holder: SurfaceHolder?) {
        }

        override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {
        }

        override fun surfaceDestroyed(holder: SurfaceHolder?) {
        }
    }

    private var stateCallback = object: CameraDevice.StateCallback() {
        override fun onOpened(camera: CameraDevice) {
            mCameraOpenCloseLock.release()
            mCameraDevice = camera
            createCameraPreviewSession()
        }

        override fun onDisconnected(camera: CameraDevice) {
            mCameraOpenCloseLock.release()
            camera.close()
            mCameraDevice = null
        }

        override fun onError(camera: CameraDevice, error: Int) {
            onDisconnected(camera)
            this@CameraFragment.activity?.finish()
            // TODO: do something while camera get error
        }
    }

    private fun openCamera() {
        val manager: CameraManager = activity?.getSystemService(Context.CAMERA_SERVICE) as CameraManager
        try {
            val cameraID: String = manager.cameraIdList[CAMERA_ID]
            val permission = ContextCompat.checkSelfPermission(activity!!.applicationContext, Manifest.permission.CAMERA)

            if (permission != PackageManager.PERMISSION_GRANTED) {
                requestCameraPermission()
            } else {
                if (!mCameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                    throw RuntimeException("Time out waiting to lock camera opening.")
                }
                manager.openCamera(cameraID, stateCallback, null)
            }
        } catch (e: CameraAccessException) {
            Log.e("CF", e.toString())
            e.printStackTrace()
        } catch (e: InterruptedException) {
            Log.e("CF", e.toString())
            e.printStackTrace()
        } catch (e: Exception) {
            Log.e("CF", e.toString())
            e.printStackTrace()
        }
    }

    private fun closeCamera() {
        try {
            mCaptureSession?.let {
                mCaptureSession!!.close()
                mCaptureSession = null
            }

            mCameraDevice?.let {
                mCameraDevice!!.close()
                mCameraDevice = null
            }

            mImageReader?.let {
                mImageReader!!.close()
                mImageReader = null
            }
        } catch (e: InterruptedException) {
            throw java.lang.RuntimeException("Interrupted while trying to lock camera closing.", e)
        } finally {
            mCameraOpenCloseLock.release()
        }



    }

    private fun createCameraPreviewSession() {
        try {
            val texture = mTextureView.surfaceTexture
            texture.setDefaultBufferSize(mSurfacePreviewSize.width, mSurfacePreviewSize.height)
            val surface = Surface(texture)
            // TEMPLATE_PREVIEW
            // TEMPLATE_RECORD
            // TEMPLATE_VIDEO_SNAPSHOT
            // TEMPLATE_ZERO_SHUTTER_LAG
            // TEMPLATE_MANUAL
            mPreviewRequestBuilder = mCameraDevice!!.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW)
            mPreviewRequestBuilder.addTarget(surface)
            mCameraDevice?.createCaptureSession(
                mutableListOf(surface, mImageReader?.surface),
                @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                object : CameraCaptureSession.StateCallback() {
                    override fun onConfigured(cameraCaptureSession: CameraCaptureSession) {
                        if (mCameraDevice == null) return

                        val cameraManager: CameraManager = mTextureView.context.getSystemService(Context.CAMERA_SERVICE) as CameraManager

                        // get ISO range
                        val sensorSensitivityRange = cameraManager.getCameraCharacteristics(cameraManager.cameraIdList[CAMERA_ID]).get(CameraCharacteristics.SENSOR_INFO_SENSITIVITY_RANGE)
                        if (sensorSensitivityRange != null) {
                            Log.i("CF", "Sensor Sensitivity Range from: ${sensorSensitivityRange.lower} to ${sensorSensitivityRange.upper}")
                        } else {
                            Log.i("CF", "NOT SUPPORT Sensor Info Sensitivity Range")
                        }


                        // get exposure time range
                        val sensorExposureTimeRange = cameraManager.getCameraCharacteristics(cameraManager.cameraIdList[CAMERA_ID]).get(CameraCharacteristics.SENSOR_INFO_EXPOSURE_TIME_RANGE)
                        if (sensorExposureTimeRange != null) {
                            Log.i("CF", "Sensor Exposure Time Range from: ${sensorExposureTimeRange.lower} to ${sensorExposureTimeRange.upper} [ns]")
                        } else {
                            Log.i("CF", "NOT SUPPORT Sensor Info Exposure Time Range")
                        }

                        mCaptureSession = cameraCaptureSession

                        try {
                            // disable auto focus
                            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE)

                            // disable auto exposure
                            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_OFF)

                            mPreviewRequestBuilder.set(CaptureRequest.SENSOR_EXPOSURE_TIME, CAMERA_SENSOR_EXPOSE_TIME)
                            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_EXPOSURE_COMPENSATION, CAMERA_CONTROL_AE_EXPOSURE_COMPENSATION)
                            mPreviewRequestBuilder.set(CaptureRequest.SENSOR_SENSITIVITY, CAMERA_SENSOR_SENSITIVITY.toInt())
//                            mPreviewRequestBuilder.set(CaptureRequest.LENS_FOCUS_DISTANCE, 200.0f)

                            mPreviewRequest = mPreviewRequestBuilder.build()
                            mCaptureSession?.setRepeatingRequest(mPreviewRequest, null, Handler(mBackgroundHandlerThread?.looper))
                        } catch (e: CameraAccessException) {
                            Log.e("erfs", e.toString())
                        } catch (e: NullPointerException) {
                            Log.e("erfs", e.toString())
                        } catch (e: Exception) {
                            Log.e("erfs", e.toString())
                        }
                    }

                    override fun onConfigureFailed(session: CameraCaptureSession) {
                        Toast.makeText(mContext, "Camera Capture Session Configure Failed", Toast.LENGTH_SHORT).show()
                    }

                }, null)
        } catch (e: CameraAccessException) {
            Log.e("erf", e.toString())
            e.printStackTrace()
        } catch (e: NullPointerException) {
            Log.e("erfs", e.toString())
            e.printStackTrace()
        } catch (e: Exception) {
            Log.e("erfs", e.toString())
            e.printStackTrace()
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun requestCameraPermission() {
        if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
            AlertDialog.Builder(activity!!.applicationContext)
                .setMessage("Permission Here")
                .setPositiveButton(android.R.string.ok) { _, _ ->
                    requestPermissions(arrayOf(Manifest.permission.CAMERA), REQUEST_CAMERA_PERMISSION)
                }
                .setNegativeButton(android.R.string.cancel) { _, _ ->
                    // TODO: can not get camera permission, need to do something
                }
                .create()
        } else {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), REQUEST_CAMERA_PERMISSION)
        }
    }


    private fun requestExternalStoragePermission() {
        if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            androidx.appcompat.app.AlertDialog.Builder(activity!!.applicationContext)
                .setMessage("Permission Here")
                .setPositiveButton(android.R.string.ok) { _, _ ->
                    requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION)
                }
                .setNegativeButton(android.R.string.cancel) { _, _ ->
                    // finish()
                }
                .create()
        } else {
            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION)
        }
    }

    private fun startBackgroundThread() {
        if (mBackgroundHandlerThread == null) {
            mBackgroundHandlerThread = HandlerThread("CameraBackground").also { it.start() }
        }

        if (mBackgroundHandler == null) {
            mBackgroundHandler = Handler(mBackgroundHandlerThread?.looper)
        }
    }

    private fun stopBackgroundThread() {
        mBackgroundHandlerThread?.quitSafely()
        try {
            mBackgroundHandlerThread?.join()
            mBackgroundHandlerThread = null
            mBackgroundHandler?.removeCallbacks(null)
            mBackgroundHandler = null
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }


    private fun saveImageToStorage(bitmap: Bitmap) {
        mSaveFileSemaphore.acquire()
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmssSSS", Locale.JAPAN).format(Date())
        val imageFileName = "c4viewer_$timeStamp.jpg"
//        val storageDir = File(context!!.filesDir, "capture_images")
        val storageDir = File(Environment.getExternalStorageDirectory(), "c4viewer")
        if (!storageDir.exists()) {
            if (!storageDir.mkdir()) {
                Log.e("CF", "can not create directory: $storageDir")
            }
        }
//        MediaStore.Images.Media.insertImage(context!!.contentResolver, bitmap, imageFileName, "")
        val saveFile : File? = File(storageDir, imageFileName)
        try {
            val fos = FileOutputStream(saveFile)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
            fos.flush()
            fos.close()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }

        if (saveFile != null) {
            Log.d("CF", "Image Saved On: $saveFile")
//            Toast.makeText(mContext, "Saved: $saveFile", Toast.LENGTH_SHORT).show()
        }

        MediaStore.Images.Media.insertImage(context!!.contentResolver, saveFile!!.absolutePath, imageFileName, null)

        val intent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://$saveFile"))
        context!!.sendBroadcast(intent)
        Thread.sleep(1)
        mSaveFileSemaphore.release()
//        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE).apply {
//            addCategory(Intent.CATEGORY_DEFAULT)
//            putExtra(MediaStore.EXTRA_OUTPUT, createSaveFileUri())
//        }
//
//        startActivityForResult(intent, CAMERA_REQUEST_CODE)
    }

    private fun removeItem(x: Float, y: Float) {
//        mSemaphore.acquire()
//        val rObjects = c4SharedViewModel.getRObjectList().value
//        for (index in 0 until rObjects!!.size) {
//            val rect = c4SharedViewModel.getRObjectList().value!![index].iObject.coordinate
//            if ((x >= rect.left) and  (x<= rect.right) and (y >= rect.top) and (y <= rect.bottom)) {
//                c4SharedViewModel.rObjectsList.value!!.removeAt(index)
//                break
//            }
//        }
//        mSemaphore.release()
//        getSharedRObjects()
//        onSharedRObjectsChangedListener!!.cameraOnSharedRObjectsChangedListener(0)
    }

    private fun modifyItem(x: Float, y: Float, ean13: String) {
//        mSemaphore.acquire()
//        val rObjects = c4SharedViewModel.getRObjectList().value
//        val itemList = resources.getStringArray(R.array.itemList).toMutableList()
//
//        for (index in 0 until rObjects!!.size) {
//            val rect = c4SharedViewModel.getRObjectList().value!![index].iObject.coordinate
//            if ((x >= rect.left) and  (x<= rect.right) and (y >= rect.top) and (y <= rect.bottom)) {
//                c4SharedViewModel.rObjectsList.value!![index].iObject.ean13 = ean13
//                c4SharedViewModel.rObjectsList.value!![index].iObject.tagId = c4SharedViewModel.item.getIdByEanCode(ean13)
//                c4SharedViewModel.rObjectsList.value!![index].iObject.tag = itemList[c4SharedViewModel.rObjectsList.value!![index].iObject.tagId!! - 1]
//                break
//            }
//        }
//        mSemaphore.release()
//        getSharedRObjects()
//        onSharedRObjectsChangedListener!!.cameraOnSharedRObjectsChangedListener(0)
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    fun setCaptureEventListener(listener : CaptureEventListener) {
        mCaptureEventListener = listener
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    interface CameraOnSharedRObjectsChangedListener : EventListener{
        fun cameraOnSharedRObjectsChangedListener(index: Int)

    }

    interface CaptureEventListener: EventListener {
        fun onFrame(frame: ByteArray)
    }

    companion object {
        const val REQUEST_CAMERA_PERMISSION: Int = 1
        const val REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION : Int = 2
        const val CAMERA_REQUEST_CODE : Int = 3
        const val CAMERA_PREVIEW_SIZE_WIDTH: Int = 1920//1280
        const val CAMERA_PREVIEW_SIZE_HEIGHT: Int = 1080//720
        const val SEMAPHORE_PERMITS_COUNT: Int = 1
        const val CAMERA_ID: Int = 0

        const val CAMERA_SENSOR_EXPOSE_TIME: Long = 10000
        const val CAMERA_SENSOR_SENSITIVITY: Long = 200

        const val DOUBLE_PRESS_INTERVAL = 250 // in millis

        private const val UPDATE_MJPEG_SERVER_MESSAGE: Int = 1

        const val CAMERA_CONTROL_AE_EXPOSURE_COMPENSATION: Int = 0

        @JvmStatic
        fun newInstance(mc: Context) =
            // インスタンス？　activitiesで生成時に呼ばれている関数
            CameraFragment().apply {
                val tmpDetailFragment = CameraFragment()
                tmpDetailFragment.mContext = mc
                return tmpDetailFragment
            }
    }
}
