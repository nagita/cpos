package com.yut3.cpos.ui.pos


import android.content.Context
import android.graphics.Rect
import android.util.Log
import com.yut3.cpos.R
import com.yut3.cpos.data.IObject
import com.yut3.cpos.data.InferenceResult
import com.yut3.cpos.data.RObject
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.lang.Exception

class InferenceWrapper(context: Context) {
    private val mContext: Context = context
    private var isInitDone: Boolean = false
    private var isInitDone2: Boolean = false
    private var isInferenceDone: Array<Boolean> = Array(1 + 10) {true}

    private var mObjectInferenceInputHeight: Int = -1
    private var mObjectInferenceInputWidth: Int = -1
    private var mClassInferenceInputHeight: Int = -1
    private var mClassInferenceInputWidth: Int = -1

    // import function
    private external fun init(modelNumber: Int, paramPath: String, originalImageWidth: Int, originalImageHeight: Int): Int
    private external fun queryIoNum(modelNumber: Int, ioNum: IntArray): Int
    private external fun queryOutputAttribute(modelNumber: Int, outputCount: Int, outputNumber: Int): Int
    private external fun inference(modelNumber: Int, inputIndex: Int, imageData: ByteArray, width: Int, height: Int, channels: Int) : Int
    private external fun getInferenceResult(modelNumber: Int, objectsClassId: IntArray, objectsConfidence: FloatArray, objectsLeft: IntArray, objectsTop: IntArray, objectsRight: IntArray, objectsBottom: IntArray): Int
    private external fun setYoloThreshold(nms_threshold: IntArray, object_threshold: IntArray, class_probability_threshold: IntArray, object_score_threshold: IntArray): Int
    private external fun setYoloOriginalImageSize(originalImageWidth: Int, originalImageHeight: Int): Int
    private external fun getModelInputSize(yolov3InputHeight: IntArray, yolov3InputWidth: IntArray, mobilenetv1InputHeight: IntArray, mobilenetv1InputWidth: IntArray): Int

    private fun createFileToCache(dir: String, filename: String, id: Int) {
        // mContext.cacheDir.absolutePath + "/" + filename
        val filePath = "$dir/$filename"
        try {
            val folder = File(dir)
            if (!folder.exists()) folder.mkdir()

            val file = File(filePath)
            if (!file.exists()) {
                val ins: InputStream = mContext.resources.openRawResource(id)
                val fos = FileOutputStream(file)
                val buffer = ByteArray(8192)
                while (true) {
                    val length = ins.read(buffer)
                    if (length <= 0)
                        break
                    fos.write(buffer, 0, length)
                }
                fos.flush()

                fos.close()
                ins.close()
                Log.d("InferenceWrapper", "Create: $filePath")
            }
        }catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun create(modelNumber: Int, paramPath: String, filename: String) : Int {
//        TODO("R.raw.yolov3tiny need to modify")
        if (modelNumber == 0) {
            createFileToCache(paramPath, filename, R.raw.yolov3tiny_stage1)
        } else {
            createFileToCache(paramPath, filename, R.raw.mobilenet_stage2)
        }
//        TODO("set original image size for inference model")
        val ret =  init(modelNumber, "$paramPath/$filename", 640, 480)
        if (ret == 0) {
            if (modelNumber == 0) {
                isInitDone = true
            } else {
                isInitDone2 = true
            }
        }

        val yolov3InputHeight = IntArray(1)
        val yolov3InputWidth = IntArray(1)
        val mobilenetv1InputHeight = IntArray(1)
        val mobilenetv1InputWidth = IntArray(1)

        getModelInputSize(yolov3InputHeight, yolov3InputWidth, mobilenetv1InputHeight, mobilenetv1InputWidth)

        mObjectInferenceInputHeight = yolov3InputHeight[0]
        mObjectInferenceInputWidth = yolov3InputWidth[0]
        mClassInferenceInputHeight = mobilenetv1InputHeight[0]
        mClassInferenceInputWidth = mobilenetv1InputWidth[0]

        return ret
    }

    fun queryIoCount(modelNumber: Int, ioNum: IntArray): Int {
        return queryIoNum(modelNumber, ioNum)
    }

    fun queryOutputsAttribute(modelNumber: Int, outputCount: Int, outputNumber: Int): Int {
        return queryOutputAttribute(modelNumber, outputCount, outputNumber)
    }

    fun predict(modelNumber: Int, inputIndex: Int, imageData: ByteArray, width: Int, height: Int, channels: Int): Int {
        isInferenceDone[modelNumber] = false
        val startTime = System.currentTimeMillis()
        inference(modelNumber, inputIndex, imageData, width, height, channels)
        val elapsedTime = System.currentTimeMillis() - startTime
        Log.i("IW", "time: $elapsedTime ms")
        isInferenceDone[modelNumber] = true
        return 0
    }

    fun result(modelNumber: Int): InferenceResult {
        val inferenceResult = InferenceResult()
        val objectsClassId = IntArray(MAX_INFERENCE_RESULT)
        val objectsConfidence = FloatArray(MAX_INFERENCE_RESULT)
        val objectsLeft = IntArray(MAX_INFERENCE_RESULT)
        val objectsTop = IntArray(MAX_INFERENCE_RESULT)
        val objectsRight = IntArray(MAX_INFERENCE_RESULT)
        val objectsBottom = IntArray(MAX_INFERENCE_RESULT)
        val objectCount: Int
        val startTime = System.currentTimeMillis()
        objectCount = getInferenceResult(modelNumber, objectsClassId, objectsConfidence, objectsLeft, objectsTop, objectsRight, objectsBottom)
        val elapsedTime = System.currentTimeMillis() - startTime
        Log.i("IW", "modelNumber:[$modelNumber], result: object count $objectCount, time: $elapsedTime ms")
        if (modelNumber == 0) {
            inferenceResult.objectCount = objectCount
            inferenceResult.outputObjects.clear()
            for (i in 0 until objectCount) {
//                inferenceResult.outputObjects.add(RObject(i, IObject(Rect(objectsLeft[i], objectsTop[i], objectsRight[i], objectsBottom[i]), mContext.resources.getStringArray(R.array.itemList).toMutableList()[objectsClassId[i]], objectsConfidence[i], null, null, null, null), null))
//            inferenceResult.outputObjects.add(Quadruple(i, objectsClassId[i],objectsConfidence[i], Rect(objectsLeft[i], objectsTop[i], objectsRight[i], objectsBottom[i])))
                inferenceResult.outputObjects.add(RObject(i, IObject(Rect(objectsLeft[i], objectsTop[i], objectsRight[i], objectsBottom[i]), "1", objectsConfidence[i], null, null, null, null), null))
            }
        } else {
            for (i in 0 until objectCount) {
                inferenceResult.dataArray.add(objectsConfidence[i])
            }
        }

        return inferenceResult
    }

    fun setYoloPostProcessThresholds(nms_threshold: Int, object_threshold: Int, class_probability_threshold: Int, object_score_threshold: Int) : Int {
        val nmsThresholdBuf = IntArray(1)
        val objectThresholdBuf = IntArray(1)
        val classProbabilityThresholdBuf = IntArray(1)
        val objectScoreThresholdBuf = IntArray(1)

        nmsThresholdBuf[0] = nms_threshold
        objectThresholdBuf[0] = object_threshold
        classProbabilityThresholdBuf[0] = class_probability_threshold
        objectScoreThresholdBuf[0] = object_score_threshold

        return setYoloThreshold(nmsThresholdBuf, objectThresholdBuf, classProbabilityThresholdBuf, objectScoreThresholdBuf)
    }

    fun setOriginalImageSize(originalImageWidth: Int, originalImageHeight: Int): Int {
        return setYoloOriginalImageSize(originalImageWidth, originalImageHeight)
    }

    fun isInitSuccess() : Boolean {
        return isInitDone && isInitDone2
    }

    fun isPredictDone(modelNumber: Int): Boolean {
        return isInferenceDone[modelNumber]
    }

    fun getModelInputHeight(modelNumber: Int): Int {
        if (modelNumber == 0) {
            return mObjectInferenceInputHeight
        }
        else {
            return mClassInferenceInputHeight
        }
    }

    fun getModelInputWidth(modelNumber: Int): Int {
        if (modelNumber == 0) {
            return mObjectInferenceInputWidth
        }
        else {
            return mClassInferenceInputWidth
        }
    }

    companion object {
        const val MAX_INFERENCE_RESULT: Int = 2600
        init {
            System.loadLibrary("rk4j")
        }
    }
}