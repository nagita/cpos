package com.yut3.cpos

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.ItemTouchHelper
import com.yut3.cpos.data.model.OrderItem

import com.yut3.cpos.dummy.DummyContent
import com.yut3.cpos.dummy.DummyContent.DummyItem
import com.yut3.cpos.ui.SwipeToDeleteCallback

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [OrderFragment.OnListFragmentInteractionListener] interface.
 */
class OrderFragment : Fragment() {

    // TODO: Customize parameters
    private var columnCount = 1

    private var listener: OnListFragmentInteractionListener? = null
    private var mUpdateOrderListListener: OnUpdateOrderListListener? = null
    //private var mLongClickOrderListListener: OnLongClickOrderListListener? = null
    private var mView: RecyclerView? = null
    private var mContext: Context? = null

    private lateinit var mAdapter: OrderRecyclerViewAdapter
    private lateinit var mCPosSharedViewModel: CPosSharedViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
        mCPosSharedViewModel = ViewModelProviders.of(mContext as FragmentActivity, ViewModelProvider.AndroidViewModelFactory(activity!!.application)).get(CPosSharedViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_order_list, container, false)

        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                val orderItemList : MutableList<OrderItem> = arrayListOf()
                adapter = OrderRecyclerViewAdapter(this@OrderFragment.context!!, activity!!.application, orderItemList, listener)
                val swipeHandler = object : SwipeToDeleteCallback(this@OrderFragment.context!!) {
                    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                        val adapter = view.adapter as OrderRecyclerViewAdapter
                        val index = viewHolder.adapterPosition
                        adapter.removeAt(index)
                        mUpdateOrderListListener!!.onUpdateOrderList()
                    }
                }

                val itemTouchHelper = ItemTouchHelper(swipeHandler)
                itemTouchHelper.attachToRecyclerView(view)
                mAdapter = adapter as OrderRecyclerViewAdapter
            }
        }
        mView = view as RecyclerView
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
            mUpdateOrderListListener = context as OnUpdateOrderListListener
            mContext = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson
     * [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onListFragmentInteraction(item: OrderItem?)
    }

    //interface OnLongClickOrderListListener {
    //    fun onLongClickOrderListListener(item: OrderItem?)
    //}

    interface OnUpdateOrderListListener {
        fun onUpdateOrderList()
    }

    fun updateOrderList() {
        mAdapter.updateDataSet()
        mView!!.scrollToPosition(mCPosSharedViewModel.getChangedOrderListItemIndex())
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
            OrderFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
}
