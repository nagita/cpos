package com.yut3.cpos

import android.animation.ObjectAnimator
import android.app.Application
import android.media.AudioManager
import android.media.ToneGenerator
import android.widget.ImageView
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.yut3.cpos.data.RObject
import com.yut3.cpos.data.model.OrderItem
import com.yut3.cpos.data.model.Product
import com.yut3.cpos.data.model.RuntimeOperatingMode

class CPosSharedViewModel(application: Application) : AndroidViewModel(application) {
    private var runtimeOperatingMode: RuntimeOperatingMode = RuntimeOperatingMode.TYPE_END_USER
    val orderProductList = MutableLiveData<MutableList<Product>>()
    val orderItemList = MutableLiveData<MutableList<OrderItem>>()
    val tempOrderProductList = MutableLiveData<MutableList<Product>>()
    val tempOrderItemList = MutableLiveData<MutableList<OrderItem>>()
    val rObjectsList = MutableLiveData<MutableList<RObject>>()
    val advertisePictureList = MutableLiveData<MutableList<Int>>()

    private var changedOrderListItemIndex = -1


    fun addOrderProduct(product: Product) {
        orderProductList.value!!.add(Product(product.uuid, product.uid, product.id, product.categoryLevel, product.barcode, product.price, product.specialOffer))
        updateOrderItemListByOrderProductList()
    }

    fun removeOrderProductById(id: Int) {
        val index = orderProductList.value!!.indexOfLast { it.id == id }
        orderProductList.value!!.removeAt(index)
        updateOrderItemListByOrderProductList()
    }

    fun clearOrderProduct() {
        orderProductList.value!!.clear()
        updateOrderItemListByOrderProductList()
    }

    fun addTempOrderProduct(product: Product) {
        tempOrderProductList.value!!.add(Product(product.uuid, product.uid, product.id, product.categoryLevel, product.barcode, product.price, product.specialOffer))
        updateTempOrderItemListByTempOrderProductList()
    }

    fun addTempToOrderProduct() {
        orderProductList.value!!.addAll(tempOrderProductList.value!!)
        updateOrderItemListByOrderProductList()
    }

    fun clearTempOrderProduct() {
        tempOrderProductList.value!!.clear()
        updateTempOrderItemListByTempOrderProductList()
    }



    fun setOrderProductList(list: MutableList<Product>) {
        orderProductList.postValue(list)
        updateOrderItemListByOrderProductList()
    }

    fun getOrderProductList(): MutableList<Product>? {
        return orderProductList.value
    }

    fun setOrderItemList(list: MutableList<OrderItem>) {
        orderItemList.postValue(list)
    }

    fun getOrderItemList(): MutableList<OrderItem>? {
        return orderItemList.value
    }

    fun getTempOrderItemList(): MutableList<OrderItem>? {
        return tempOrderItemList.value
    }

    fun updateOrderItemListByOrderProductList() {
        orderItemList.value!!.clear()
        for (productCount in 0 until orderProductList.value!!.size) {
            val orderItem = orderItemList.value!!.filter { it.name == orderProductList.value!![productCount].uid }
            if (orderItem.isEmpty()) {
//                TODO("Need to get price or some product info here to display")
                orderItemList.value!!.add(OrderItem(orderProductList.value!![productCount].id, orderProductList.value!![productCount].uid, orderProductList.value!![productCount].barcode.code, 1, orderProductList.value!![productCount].price, orderProductList.value!![productCount].specialOffer))
                changedOrderListItemIndex = orderItemList.value!!.size - 1
            } else {
                val index = orderItemList.value!!.indexOfFirst { it.name == orderProductList.value!![productCount].uid }
                orderItemList.value!![index].count += 1
                changedOrderListItemIndex = index
            }
        }
    }

    fun updateTempOrderItemListByTempOrderProductList() {
        tempOrderItemList.value!!.clear()
        for (productCount in 0 until tempOrderProductList.value!!.size) {
            val orderItem = tempOrderItemList.value!!.filter { it.name == tempOrderProductList.value!![productCount].uid }
            if (orderItem.isEmpty()) {
                tempOrderItemList.value!!.add(OrderItem(tempOrderProductList.value!![productCount].id, tempOrderProductList.value!![productCount].uid, tempOrderProductList.value!![productCount].barcode.code, 1, tempOrderProductList.value!![productCount].price, tempOrderProductList.value!![productCount].specialOffer))
                changedOrderListItemIndex = orderItemList.value!!.size - 1
            } else {
                val index = tempOrderItemList.value!!.indexOfFirst { it.name == tempOrderProductList.value!![productCount].uid }
                tempOrderItemList.value!![index].count += 1
                changedOrderListItemIndex = index
            }
        }
    }

    fun getRuntimeOperatingMode() : RuntimeOperatingMode {
        return runtimeOperatingMode
    }

    fun setRuntimeOperatingMode(mode: RuntimeOperatingMode) {
        runtimeOperatingMode = mode
    }

    fun setRObjectList(list: MutableList<RObject>) {
        rObjectsList.postValue(list)
    }

    fun getRObjectList() :MutableLiveData<MutableList<RObject>> {
        return rObjectsList
    }

    fun setAdvertisePictureList() {
        advertisePictureList.value = arrayListOf()
        advertisePictureList.value!!.add(R.raw.liteon_smart_factory)
        advertisePictureList.value!!.add(R.raw.liteon_smart_city_video)
        advertisePictureList.value!!.add(R.raw.liteon_smart_solution)
    }

    fun getAdvertisePicture(index: Int) :Int {
        return advertisePictureList.value!![index]
    }


    fun setBeepSound() {
        val toneGenerator = ToneGenerator(AudioManager.STREAM_SYSTEM, ToneGenerator.MAX_VOLUME)
        toneGenerator.startTone(ToneGenerator.TONE_PROP_BEEP)
    }

    fun getChangedOrderListItemIndex(): Int {
        return changedOrderListItemIndex
    }

}