package com.yut3.cpos

import android.animation.ArgbEvaluator
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.app.Application
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Point
import android.graphics.Typeface
import android.util.Log
import android.util.TypedValue
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.animation.Animation
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders


import com.yut3.cpos.OrderFragment.OnListFragmentInteractionListener
//import com.yut3.cpos.OrderFragment.OnLongClickOrderListListener
import com.yut3.cpos.data.model.OrderItem
import com.yut3.cpos.data.model.Product
import com.yut3.cpos.dummy.DummyContent.DummyItem
import com.yut3.cpos.ui.pos.dummy.ForSaleProduct

import kotlinx.android.synthetic.main.fragment_order.view.*
import kotlinx.android.synthetic.main.fragment_order.view.productImageView
import kotlinx.android.synthetic.main.fragment_order.view.productName
import kotlinx.android.synthetic.main.fragment_product.view.*

/**
 * [RecyclerView.Adapter] that can display a [DummyItem] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 * TODO: Replace the implementation with code for your data type.
 */
class OrderRecyclerViewAdapter(
    private val mContext: Context,
    private val mApplication: Application,
    private val mValues: List<OrderItem>,
    private val mListener: OnListFragmentInteractionListener?
//    private val mOrderListListener: OnLongClickOrderListListener?
) : RecyclerView.Adapter<OrderRecyclerViewAdapter.ViewHolder>() {
//    private var mProductList: MutableList<DummyItem> = mValues.toMutableList()
    private var mOrderList: MutableList<OrderItem> = mValues.toMutableList()

    private lateinit var mCPosSharedViewModel: CPosSharedViewModel

    private var mChangedOrderItemListIndex: Int = -1
    private var mIsOperatingRemove: Boolean = false

    private val mOnClickListener: View.OnClickListener
//    private val mOnLongClickListener: View.OnLongClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as OrderItem
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            mListener?.onListFragmentInteraction(item)
        }

        //mOnLongClickListener = View.OnLongClickListener { v ->
        //    val item = v.tag as OrderItem
//            mOrderListListener?.onLongClickOrderListListener(item)
//        }

        mCPosSharedViewModel = ViewModelProviders.of(mContext as FragmentActivity, ViewModelProvider.AndroidViewModelFactory(mApplication)).get(CPosSharedViewModel::class.java)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_order, parent, false)
        val display = (mContext.getSystemService(Context.WINDOW_SERVICE)!! as WindowManager).defaultDisplay

        val point = Point()
        display.getSize(point)
        // 640: product fragment width
        // 8 + 8 item left and right margin
        // 16 + 16 RecyclerView left and right margin
        view.layoutParams.width = (point.x - TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (640 + 8 + 8 + 16 + 16).toFloat(), mContext.resources.displayMetrics)).toInt()
        val itemWidth = view.layoutParams.width
        val pxWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (15 + 60 + 10 + 8 + 8).toFloat(), mContext.resources.displayMetrics)
        val productTextView = view.findViewById<LinearLayout>(R.id.productTextContent)
        productTextView.layoutParams.width = (itemWidth - pxWidth).toInt()

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val cardView = holder.mView as CardView
        cardView.radius = 20.0F

        val item = mOrderList[position]
        holder.mProductName.text = "${item.name}"
        holder.mProductCount.text = "x${item.count}"
        holder.mProductPrice.text = "$ ${item.price}"
        holder.mProductSpecialComment.text = "$ ${item.specialOffer}"
        holder.mProductTotalPrice.text = "$ ${item.count * item.specialOffer}"

        //holder.mProductImageView.setImageResource(R.mipmap.ic_launcher)
        holder.mProductImageView.setImageResource(ForSaleProduct.getProductPicture(item.id))

        holder.mProductPrice.paintFlags = holder.mProductPrice.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        holder.mProductPrice.setTextColor(Color.parseColor("#AAAAAA"))

        holder.mProductSpecialComment.setTextColor(Color.parseColor("#FF9090"))
        holder.mProductSpecialComment.typeface = Typeface.DEFAULT_BOLD

        holder.mProductTotalPrice.setTextColor(Color.parseColor("#00D8FF"))
//        holder.mProductTotalPrice.setBackgroundColor(Color.parseColor("#00D8FF"))
        holder.mProductTotalPrice.typeface = Typeface.DEFAULT_BOLD

        holder.mProductColorbarTextView.setBackgroundColor(Color.GREEN)

//        TODO("bug: while remove item, this will display in all item!!")
        if ("酒" in item.name) {
            holder.mProductAdditionalMessage.text = "年齢確認"
            holder.mProductAdditionalMessage.setBackgroundColor(Color.CYAN)
        }
        else {
            holder.mProductAdditionalMessage.text = ""
        }


        Log.i("ORVA", "mChangedOrderItemListIndex: [${mChangedOrderItemListIndex}], position: [${position}]")
        if (mChangedOrderItemListIndex == position) {
            Log.i("ORVA", "create animation on position ${position}")
            holder.mObjectAnimator.duration = 250
            holder.mObjectAnimator.setEvaluator(ArgbEvaluator())
            holder.mObjectAnimator.repeatMode = ValueAnimator.REVERSE
            holder.mObjectAnimator.repeatCount = 1
            holder.mObjectAnimator.start()
            mChangedOrderItemListIndex = -1
        }

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mOrderList.size

    fun removeAt(position: Int) {
        mCPosSharedViewModel.removeOrderProductById(mOrderList[position].id)
        mIsOperatingRemove = true
        notifyItemRemoved(position)
        updateDataSet()
    }

    fun updateDataSet() {
        mOrderList = mCPosSharedViewModel.getOrderItemList()!!
        mChangedOrderItemListIndex = if (mIsOperatingRemove) {
            mIsOperatingRemove = false
            -1
        } else {
            mCPosSharedViewModel.getChangedOrderListItemIndex()
        }
        notifyDataSetChanged()
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mProductColorbarTextView: TextView = mView.productColorbarTextView
        val mProductColorbarTextView2: TextView = mView.productColorbarTextView2
        val mProductImageView: ImageView = mView.productImageView
        val mProductName: TextView = mView.productName
        val mProductCount: TextView = mView.productCount
        val mProductPrice: TextView = mView.productPrice
        val mProductSpecialComment: TextView = mView.productSpecialComment
        val mProductTotalPrice: TextView = mView.productTotalPrice
        val mProductAdditionalMessage: TextView = mView.productAdditionalMessage

        val mObjectAnimator: ObjectAnimator = ObjectAnimator.ofInt(mProductColorbarTextView2, "backgroundColor", Color.WHITE, Color.CYAN)

        override fun toString(): String {
            return super.toString() + " '" + mProductName.text + "'"
        }
    }
}
